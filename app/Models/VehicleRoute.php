<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleRoute extends Model
{
	//use SoftDeletes;
    use HasFactory;
    protected $guarded = ['id'];

    protected $casts = [
        'stoppages' => 'array'
    ];

    public function startFrom(){
        return $this->belongsTo(Counter::class, 'start_from', 'id');
        //->withTrashed();
    }


    public function endTo(){
        return $this->belongsTo(Counter::class, 'end_to', 'id');
        //->withTrashed();
    }

    //scope
    public function scopeActive(){
      return  $this->where('status', 1);
    }
}
