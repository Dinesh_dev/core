<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class BoardingPoint extends Model
{
	//use SoftDeletes;
	
    protected $guarded = ['id'];
	
	
	public function counter(){
        return $this->belongsTo(Counter::class);
    }
	
	public function trip(){
        return $this->hasOne(Trip::class);
    }
	
    //scope
    public function scopeActive(){
        return $this->where('status', 1);
    }

    // public function scopeRouteStoppages($query, $array)
    // {
        // return $query->whereIn('id', $array)
        // ->orderByRaw("field(id,".implode(',',$array).")")->get();
    // }
}
