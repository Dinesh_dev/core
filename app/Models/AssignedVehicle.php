<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class AssignedVehicle extends Model
{
	//use SoftDeletes;
    use HasFactory;
    protected $guarded = ['id'];

    public function trip(){
        return $this->belongsTo(Trip::class);
        //->withTrashed();
    }

    public function vehicle(){
        return $this->belongsTo(Vehicle::class);
        //->withTrashed();
    }
}
