<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TransportHistory;

class TransportHistoryController extends Controller
{
    function index(){
       
        
        $pageTitle = 'Logistic History';
        $items=TransportHistory::paginate(getPaginate());
        $emptyMessage = 'No Data found.';
        return view('admin.transport.history', compact('items', 'pageTitle','emptyMessage'));
    }
    public function store(Request $request)
    {
        # code...
        $request->validate([
            'sendername' => 'required',
            'senderphoneno' => 'required',
            'senderaddress'=>'required',
            'receivername'=>'required',
            'receiveraddress'=>'required',
            'receiverphoneno'=>'required',
            'packagetype'=>'required',
            'amountcharged'=>'required'
        ]);

        TransportHistory::insert([
            'transport_id'=>getTrx(),
            'sendername' => $request->sendername,
            'senderphoneno' => $request->senderphoneno,
            'senderaddress'=>$request->senderaddress,
            'receivername'=>$request->receivername,
            'receiveraddress'=>$request->receiveraddress,
            'receiverphoneno'=>$request->receiverphoneno,
            'packagetype'=>$request->packagetype,
            'packageweight'=>$request->packageweight,
            'amountcharged'=>$request->amountcharged,
            
        ]);


        $notify[] = ['success', 'Logistics histroy save successfully.'];
        
        return back()->withNotify($notify);
    }

    public function update(Request $request,$id)
    {
        # code...
        $request->validate([
            'sendername' => 'required',
            'senderphoneno' => 'required',
            'senderaddress'=>'required',
            'receivername'=>'required',
            'receiveraddress'=>'required',
            'receiverphoneno'=>'required',
            'packagetype'=>'required',
            'amountcharged'=>'required',
            'status'=>'required'
        ]);
        
        TransportHistory::where('id',$id)->update([
            'sendername' => $request->sendername,
            'senderphoneno' => $request->senderphoneno,
            'senderaddress'=>$request->senderaddress,
            'receivername'=>$request->receivername,
            'receiveraddress'=>$request->receiveraddress,
            'receiverphoneno'=>$request->receiverphoneno,
            'packagetype'=>$request->packagetype,
            'packageweight'=>$request->packageweight,
            'amountcharged'=>$request->amountcharged,
            'status'=>$request->status
        ]);

        $notify[] = ['success', 'Logistics histroy updated successfully.'];
        
        return back()->withNotify($notify);
    }

    public function distroy(Request $request)
    {
        # code...
        $request->validate(['id' => 'required|integer']);
        TransportHistory::find($request->id)->delete();
        $notify[] = ['success', 'Logistics history deleted successfully.'];
        return back()->withNotify($notify);
    }
}
