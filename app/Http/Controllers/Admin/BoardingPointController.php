<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Counter;
use App\Models\BoardingPoint;

class BoardingPointController extends Controller
{
    public function boardingPoints(){
        $pageTitle = 'All Boarding Points';
        $emptyMessage = 'No boarding point found';
        $boarding_points = BoardingPoint::paginate(getPaginate());
		$counters = Counter::active()->get();
        return view('admin.boarding-point.list', compact('pageTitle','emptyMessage','boarding_points','counters'));
    }

    public function boardingPointStore (Request $request){
        $request->validate([
            'name' => 'required|unique:boarding_points',
            'counter_id' => 'required',
        ]);

        $boarding_point = new BoardingPoint();
        $boarding_point->name      =  $request->name;
        $boarding_point->counter_id      =  $request->counter_id;
        $boarding_point->save();

        $notify[] = ['success', 'Boarding Point save successfully.'];
        return back()->withNotify($notify);
    }

    public function boardingPointUpdate(Request $request, $id){
        $request->validate([
            'name' => 'required|unique:boarding_points,name,'.$id,
            'counter_id' => 'required',
        ]);

        $boarding_point = BoardingPoint::find($id);
        $boarding_point->name      =  $request->name;
        $boarding_point->counter_id      =  $request->counter_id;
        $boarding_point->save();

        $notify[] = ['success', 'Boarding Point update successfully.'];
        return back()->withNotify($notify);
    }

    public function boardingPointActiveDisabled(Request $request){
        $request->validate(['id' => 'required|integer']);

        $boarding_point = BoardingPoint::find($request->id);
        $boarding_point->status = $boarding_point->status == 1 ? 0 : 1;
        $boarding_point->save();
        
        if($boarding_point->status == 1){
            $notify[] = ['success', 'Boarding Point active successfully.'];
        }else{
            $notify[] = ['success', 'Boarding Point disabled successfully.'];
        }

        return back()->withNotify($notify);
    }
	
	public function delete(Request $request){
        $request->validate(['id' => 'required|integer']);
        BoardingPoint::find($request->id)->delete();
        $notify[] = ['success', 'Boarding Point deleted successfully.'];
        return back()->withNotify($notify);
    }	
	
}
