<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Review;

class ReviewController extends Controller
{
    public function reviews(){
        $pageTitle = 'All Reviews';
        $emptyMessage = 'No review found';
        $reviews = Review::paginate(getPaginate());
        return view('admin.review.list', compact('pageTitle','emptyMessage','reviews'));
    }
    public function reviewActiveDisabled(Request $request){
        $request->validate(['id' => 'required|integer']);

        $review = Review::find($request->id);
        $review->status = $review->status == 1 ? 0 : 1;
        $review->save();
        
        if($review->status == 1){
            $notify[] = ['success', 'Review active successfully.'];
        }else{
            $notify[] = ['success', 'Review disabled successfully.'];
        }

        return back()->withNotify($notify);
    }
	
	public function delete(Request $request){
        $request->validate(['id' => 'required|integer']);
        Review::find($request->id)->delete();
        $notify[] = ['success', 'Review deleted successfully.'];
        return back()->withNotify($notify);
    }	
	
}
