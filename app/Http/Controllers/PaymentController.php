<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Paystack;
use App\Models\BookedTicket;
use App\Models\GeneralSetting;
use App\Models\Deposit;
use App\Models\AdminNotification;
use Auth;

class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway(Request $request)
    {
		
		try {
			//code...
			$data = array(
				"amount" => $request->amount * 100,
				"reference" => $request->reference,
				"email" => $request->email,
				"currency" => $request->currency,
				"orderID" => $request->orderID,
			);
	
			return Paystack::getAuthorizationUrl($data)->redirectNow();  
		} catch (\Throwable $th) {
			//throw $th;

			return redirect()->route('paystackerror');
		}

		   
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
		$general = GeneralSetting::first();
        $paymentDetails = Paystack::getPaymentData();
		
		// if($paymentDetails['data']['status'] == 'success')
		// {
			$bookedTicket = BookedTicket::where('pnr_number', $paymentDetails['data']['reference'])->first();
			$bookedTicket->status = 1;
            $bookedTicket->save();
			
			$user = $bookedTicket->user;
			$charge = 0 + ($bookedTicket->sub_total * 3.9 / 100); //fixed_charge set to 0 and percentage charge set to 3.9
			$payable = $bookedTicket->sub_total + $charge;
			$final_amo = $payable * 1; //rate default set to 1
			
			$data = new Deposit();
			$data->user_id = Auth::guest() ? 0 : $user->id;
			$data->customer_email = $bookedTicket->customer_email;
			$data->booked_ticket_id = $bookedTicket->id;
			$data->method_code = 107; //paystack method code
			$data->method_currency = "NGN";
			$data->amount = $bookedTicket->sub_total;
			$data->charge = $charge;
			$data->rate = 1; //Default set to 1
			$data->final_amo = $final_amo;
			$data->btc_amo = 0;
			$data->btc_wallet = "";
			$data->trx = getTrx();
			$data->try = 0;
			$data->detail = $paymentDetails;
			$data->status = $paymentDetails['data']['status'] == 'success' ? 1 : 3;
			$data->save();
			// session()->put('Track', $data->trx);
			// return redirect()->route('user.deposit.preview');
			
			$adminNotification = new AdminNotification();
            $adminNotification->user_id = $bookedTicket->customer_email ? 0 : $user->id;
			
			if($paymentDetails['data']['status'] == 'success')
			{
				$adminNotification->title = 'Payment successful via Paystack';
				$notify = 'PAYMENT_COMPLETE';
			}
			else
			{
				$adminNotification->title = 'Payment failed via Paystack';
				$notify = 'PAYMENT_FAILED';
			}
            $adminNotification->click_url = urlPath('admin.vehicle.ticket.booked');
            $adminNotification->save();

            // notify($user, $notify, [
                // 'method_name' => 'Paystack',
                // 'method_currency' => $data->method_currency,
                // 'method_amount' => showAmount($data->final_amo),
                // 'amount' => showAmount($data->amount),
                // 'charge' => showAmount($data->charge),
                // 'currency' => $general->cur_text,
                // 'rate' => showAmount($data->rate),
                // 'trx' => $data->trx,
                // 'journey_date' => showDateTime($bookedTicket->date_of_journey , 'd m, Y'),
                // 'seats' => implode(',',$bookedTicket->seats),
                // 'total_seats' => sizeof($bookedTicket->seats),
                // 'source' => $bookedTicket->pickup->name,
                // 'destination' => $bookedTicket->drop->name
            // ]);
			
		// }
		if($paymentDetails['data']['status'] == 'success')
		{
			$notify = ['success', 'Payment Successful.'];
			
			if($bookedTicket->customer_email)
			{
				return redirect()->route('ticket.customer.print', $bookedTicket->id);
			}
            return redirect()->route('user.home');
		}
		
		$notify[] = ['error', 'Payment Failed.'];
        return redirect()->route('user.home');
        
    }
}