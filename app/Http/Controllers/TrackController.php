<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\GeneralSetting;
use App\Models\TransportHistory;    
class TrackController extends Controller
{
    //
    public function __construct()
    {
        $this->activeTemplate = activeTemplate();
    }
    public function trackpackage()
    {
        # code... 
        $pageTitle = "Track your package";
      
        return view($this->activeTemplate . 'user.track',compact('pageTitle'));
    }

    public function trackpackagedetails(Request $request)
    {
        # code...
        $pageTitle = "Track your Package";
    
        //Search package by package id.
        $data= TransportHistory::where('transport_id',$request->trackingno)->first();

        if(!$data){
            $notify[] = ['error', 'Tracking no does not exists.Please enter correct tracking no'];
            return redirect()->route('user.track')->withNotify($notify);
        }
      
        return view($this->activeTemplate . 'user.track',compact('pageTitle','data'));
    }
}
