@extends('admin.layouts.app')

@section('panel')
<div class="row">
        <div class="col-lg-12">
            <div class="card b-radius--10 ">
                <div class="card-body p-0">
                    <div class="table-responsive--md  table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                                <tr>
                                    <th>@lang('Tracking No')</th>
                                    <th>@lang('Sender Name')</th>
                                    <th>@lang('Sender Phone')</th>
                                    <th>@lang('Sender Address')</th>
                                    <th>@lang('Package Type')</th>
                                    <th>@lang('Package Weight ')</th>
                                    <th>@lang('Amount Charged ')</th>
                                    <th>@lang('Package Status')</th>
                                    <th>@lang('Receiver Name ')</th>
                                    <th>@lang('Receiver Phone')</th>
                                    <th>@lang('Receiver Address ')</th>
                                    <th>@lang('Action')</th>

                                </tr>
                            </thead>
                            <tbody>

                                 @forelse($items as $item)
                                    <tr>
                                    <td data-label="@lang('Package Id')">
                                    <span class="badge badge--success font-weight-bold"> {{$item->transport_id}} </span>
                                    </td>
                                    <td data-label="@lang('Sender Name')">
                                        {{ $item->sendername }}
                                    </td>
                                    <td data-label="@lang('Sender Phone')">
                                        {{ $item->senderphoneno }}
                                    </td>
                                    <td data-label="@lang('Sender Address')">
                                        {{ $item->senderaddress }}
                                    </td>
                                    <td data-label="@lang('Package Type')">
                                        @if($item->packagetype == '1')
                                        <span class="badge badge--success font-weight-bold"> Fragile </span>
                                        @endif
                                        @if($item->packagetype == '2')
                                        <span class="badge badge--success font-weight-bold"> Non Fragile </span>
                                        @endif
                                        
                                    </td>
                                    <td data-label="@lang('Package Weight')">
                                        {{$item->packageweight}}
                                    </td>
                                    <td data-label="@lang('Amount Charged')">
                                        {{$item->amountcharged}}
                                    </td>
                                    <td data-label="@lang('Status')">
                                       
                                        @if($item->status == '1')
                                        <span class="badge badge--success font-weight-bold" >In Transit </span>
                                        @endif
                                        @if($item->status == '2')
                                        <span class="badge badge--success font-weight-bold"> Processing </span>
                                        @endif
                                        @if($item->status == '3')
                                        <span class="badge badge--success font-weight-bold"> Delivered </span>
                                        @endif
                                        @if($item->status == '4')
                                        <span class="badge badge--success font-weight-bold"> Arrived </span>
                                        @endif
                                      
                                    </td>
                                    <td data-label="@lang('Receiver Name')">
                                        {{$item->receivername}}    
                                    </td>
                                    <td data-label="@lang('Receiver Phone')">
                                        {{$item->receiverphoneno}}
                                    </td>
                                    <td data-label="@lang('Receiver address')">
                                         {{$item->receiveraddress}}       
                                    </td>
                                    <td>
                                        <button type="button" class="icon-btn ml-1 editBtn"
                                                data-toggle="modal" data-target="#editModal"
                                                data-history_type = "{{ $item }}"
                                                data-original-title="@lang('Update')">
                                            <i class="la la-pen"></i>
                                        </button>
                                        <button type="button"
											class="icon-btn btn--danger ml-1 removeBtn"
											data-toggle="modal" data-target="#removeModal"
											data-id=""
											data-original-title="@lang('Delete')">
											<i class="las la-trash"></i>
										</button>
                                    </td>
                                </tr>
                                 @empty
                                    <tr>
                                        <td class="text-muted text-center" colspan="100%">{{ __($emptyMessage) }}</td>
                                    </tr>
                                 @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ paginateLinks($items) }}
                </div>
            </div>
        </div>
</div> 

{{-- remove METHOD MODAL --}}
<div id="removeModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('Delete Logistics History')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('admin.transport.history.delete')}}" method="POST">
                    @csrf
                    <input type="text" name="id" hidden="true">
                    <div class="modal-body">
                        <strong>@lang('Are you sure, you want to delete this?')</strong>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn--danger">@lang('Delete')</button>
                    </div>
            </form>
        </div>
    </div>
</div>
{{-- Add METHOD MODAL --}}
    <div id="addModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> @lang('Add Logistics History')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.transport.history.store')}}" method="POST">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Sender Name')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Sender Name')" name="sendername" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Sender Phone')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Sender Phone no')" name="senderphoneno" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Sender Address')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Sender Address')" name="senderaddress" required>
                        </div>
                      
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Pakage Type')</label>
                            <select name="packagetype" class="form-control" required>
                                <option value="">@lang('Select packagetype')</option>
                                <option value="1">Fragile</option>
                                <option value="2">Non Fragile</option>

                            </select>
                           
                            
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Packet weight ')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Packet Weight')" name="packageweight" >
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Amount charged')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Amount Charged')" name="amountcharged" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Receiver Name')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Receiver Name')" name="receivername" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Receiver Phoneno')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Receiver Phone no')" name="receiverphoneno" required>
                        </div>
                        <div class="form-group">
                            <label class="form-control-label font-weight-bold"> @lang('Receiver Address')</label>
                            <input type="text" class="form-control" placeholder="@lang('Enter Receiver Address')" name="receiveraddress" required>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn--primary">@lang('Save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

{{-- Update METHOD MODAL --}}
    <div id="editModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"> @lang('Update Logistics History')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST">
                    @csrf
                    <div class="modal-body">
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Sender Name')</label>
                                <input type="text" class="form-control" placeholder="@lang('Enter Sender Name')" name="sendername" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Sender Phone')</label>
                                <input type="text" class="form-control" placeholder="@lang('Enter Sender Phone no')" name="senderphoneno" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Sender Address')</label>
                                <input type="text" class="form-control" placeholder="@lang('Enter Sender Address')" name="senderaddress" required>
                            </div>
                        
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Pakage Type')</label>
                                <select name="packagetype" class="form-control" required>
                                    <option value="">@lang('Select packagetype')</option>
                                    <option value="1">Fragile</option>
                                    <option value="2">Non Fragile</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Packet weight ')</label>
                                <input type="text" class="form-control" placeholder="@lang('Enter Packet Weight')" name="packageweight" >
                            </div>
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Amount charged')</label>
                                <input type="text" class="form-control" placeholder="@lang('Enter Amount Charged')" name="amountcharged" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Receiver Name')</label>
                                <input type="text" class="form-control" placeholder="@lang('Enter Receiver Name')" name="receivername" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Receiver Phoneno')</label>
                                <input type="text" class="form-control" placeholder="@lang('Enter Receiver Phone no')" name="receiverphoneno" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Receiver Address')</label>
                                <input type="text" class="form-control" placeholder="@lang('Enter Receiver Address')" name="receiveraddress" required>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label font-weight-bold"> @lang('Pakage Status')</label>
                                <select name="status" class="form-control" required>
                                    <option value="">@lang('Select status')</option>
                                    <option value="1">In Transit</option>
                                    <option value="2">Processing</option>
                                    <option value="3">Delivered</option>
                                    <option value="4">Arrived</option>

                                </select>
                            </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
                        <button type="submit" class="btn btn--primary">@lang('Update')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection



@push('breadcrumb-plugins')
    <a href="javascript:void(0)" class="btn btn-sm btn--primary box--shadow1 text--small addBtn"><i class="fa fa-fw fa-plus"></i>@lang('Add New')</a>
@endpush

@push('script')
<script>

(function ($) {
"use strict";

    $('.addBtn').on('click', function () {
        var modal = $('#addModal');
        modal.modal('show');
    });

    $('.removeBtn').on('click', function () {
        var modal = $('#removeModal');
        modal.find('input[name=id]').val($(this).data('id'));        
        modal.modal('show');
    });

    $(document).on('click', '.editBtn', function () {
        var modal   = $('#editModal');
        var data    = $(this).data('history_type');
        var link    = `{{ route('admin.transport.history.update', '') }}/${data.id}`;
        modal.find('input[name=sendername]').val(data.sendername);
        modal.find('input[name=senderphoneno]').val(data.senderphoneno);
        modal.find('input[name=senderaddress]').val(data.senderaddress);
        modal.find('select[name=packagetype]').val(data.packagetype);
        modal.find('input[name=packageweight]').val(data.packageweight);
        modal.find('input[name=amountcharged]').val(data.amountcharged);
        modal.find('input[name=receivername]').val(data.receivername);
        modal.find('input[name=receiverphoneno]').val(data.receiverphoneno);
        modal.find('input[name=receiveraddress]').val(data.receiveraddress);
        modal.find('select[name=status]').val(data.status);
        modal.find('form').attr('action', link);
        modal.modal('show');

                
    });


})(jQuery);
</script>
@endpush