@extends('admin.layouts.app')

@section('panel')
    <div class="row">
        <div class="col-md-12">
            <div class="card b-radius--10 ">
                <div class="card-body">
                    <div class="table-responsive--sm table-responsive">
                        <table class="table table--light style--two">
                            <thead>
                                <tr>
                                    <th>@lang('User')</th>
                                    <th>@lang('Kin')</th>
                                    <th>@lang('PNR Number')</th>
                                    <th>@lang('Departure Date')</th>
									<th>@lang('Return Date')</th>
                                    <th>@lang('Trip')</th>
                                    <th>@lang('Pickup Point')</th>
                                    <th>@lang('Dropping Point')</th>
                                    <th>@lang('Booked Seats')</th>
                                    <th>@lang('Status')</th>
                                    <th>@lang('Ticket Count')</th>
                                    <th>@lang('Action')</th>
									{{--
                                    <th>@lang('Fare')</th>
									--}}
                                </tr>
                            </thead>
                            <tbody>
                            @forelse($tickets as $item)
                                <tr>
									@if($item->customer_name)
									<td data-label="@lang('User')">
                                        <span class="font-weight-bold">{{ __(@$item->customer_name) }}</span>
                                    </td>	
									@else
                                    <td data-label="@lang('User')">
                                        <span class="font-weight-bold">{{ __(@$item->user->fullname) }}</span>
                                    <br>
                                    <span class="small"> <a href="{{ route('admin.users.detail', $item->user_id) }}"><span>@</span>{{ __(@$item->user->username) }}</a> </span>

                                    </td>
									@endif
									<td data-label="@lang('Kin')">
										@if($item->kin_name)
                                        <span class="font-weight-bold">Name: {{ __(@$item->kin_name) }}</span>
										<br>
										<span class="font-weight-bold">Email: {{ __(@$item->kin_email) }}</span>
										<br>
										<span class="font-weight-bold">Contact No: {{ __(@$item->kin_contact_no) }}</span>
										<br>
										@endif
                                    </td>
                                    <td data-label="@lang('PNR Number')">
                                        <span class="text-muted">{{ __($item->pnr_number) }}</span>
                                    </td>
                                    <td data-label="@lang('Journey Date')">
                                        {{ __(showDateTime($item->date_of_journey, 'd M, Y')) }}
                                    </td>
									<td data-label="Return Date">
										{{ __($item->date_of_return ? showDateTime($item->date_of_return , 'd M, Y') : 'N/A' ) }}
									</td>
                                    <td data-label="@lang('Trip')">
                                        <span class="font-weight-bold">{{ __($item->trip->fleetType->name) }}</span>
                                        <br>
                                        <span class="font-weight-bold"> {{ __($item->trip->startFrom->name ) }} - {{ __($item->trip->endTo->name ) }}</span>
                                    </td>
                                    <td data-label="@lang('Pickup Point')">
                                        {{ __($item->pickup->name) }}
                                    </td>
                                    <td data-label="@lang('Dropping Point')">
                                        {{ __($item->drop->name) }}
                                    </td>
									<td data-label="@lang('Booked Seats')">
                                        {{ __(implode(",",$item->seats)) }}
                                    </td>
                                    <td data-label="@lang('Status')">
                                        @if ($item->status == 1)
                                            <span class="badge badge--success font-weight-normal text--samll">@lang('Booked')</span>
                                        @elseif($item->status == 2)
                                            <span class="badge badge--warning font-weight-normal text--samll">@lang('Pending')</span>
										@elseif($item->status == 3)
                                            <span class="badge badge--danger font-weight-normal text--samll">@lang('Canceled')</span>
                                        @else
                                            <span class="badge badge--dark font-weight-normal text--samll">@lang('Rejected')</span>
                                        @endif
                                    </td>
                                    <td data-label="@lang('Ticket Count')">
                                        {{ __(sizeof($item->seats)) }}
                                    </td>
									<td data-label="@lang('Action')">
                                        @if ($item->status == 1)
											<button type="button"
												class="icon-btn btn--danger ml-1 removeBtn"
												data-toggle="modal" data-target="#removeModal_{{ $item->id }}"
												data-id="{{ $item->id }}"
												data-original-title="@lang('Delete')">
												<i class="las la-trash"></i>
											</button>
                                        @endif
                                    </td>
									{{--
                                    <td data-label="@lang('Fare')">
                                        {{ __(showAmount($item->sub_total)) }} {{ __($general->cur_text) }}
                                    </td>
									--}}
                                </tr>
								{{-- remove METHOD MODAL --}}
								<div id="removeModal_{{ $item->id }}" class="modal fade" tabindex="-1" role="dialog">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title"> @lang('Cancel Booking')</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<form action="{{ route('admin.vehicle.ticket.cancelTicket',$item->id)}}" method="POST">
												@csrf
												<input type="text" name="id" hidden="true">
												<div class="modal-body">
													<strong>@lang('Are you sure, you want to cancel this?')</strong>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn--dark" data-dismiss="modal">@lang('Close')</button>
													<button type="submit" class="btn btn--danger">@lang('Delete')</button>
												</div>
											</form>
										</div>
									</div>
								</div>
                            @empty
                                <tr>
                                    <td class="text-muted text-center" colspan="100%">{{ __($emptyMessage) }}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer py-4">
                    {{ paginateLinks($tickets) }}
                </div>
            </div>
        </div>
    </div>
	
@endsection
@push('breadcrumb-plugins')
<form action="{{route('admin.vehicle.ticket.search', $scope ?? str_replace('admin.vehicle.ticket.', '', request()->route()->getName()))}}" method="GET" class="form-inline float-sm-right bg--white mb-2 ml-0 ml-xl-2 ml-lg-0">
    <div class="input-group has_append">
        <input type="text" name="search" class="form-control" placeholder="@lang('Search PNR Number')" value="{{ $search ?? '' }}">
        <div class="input-group-append">
            <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>

<form action="{{ Request::url() }}" method="GET" target ="_blank" class="form-inline float-sm-right bg--white">
	<div class="input-group has_append ">
		<input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" class="datepicker-here form-control" data-position='bottom right' placeholder="@lang('Min date - Max date')" autocomplete="off" value="{{ @$dateSearch }}">
		<input type="hidden" name="method" value="{{ @$methodAlias }}">
		<input type="hidden" name="report" value="pdf">
		<div class="input-group-append">
			<button class="btn btn--primary" type="submit"><i class="fa fa-file-pdf"></i></button>
		</div>
	</div>
</form>
@endpush

@push('script-lib')
  <script src="{{ asset('assets/global/js/datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/global/js/datepicker.en.js') }}"></script>
@endpush
@push('script')
  <script>
    (function($){
        "use strict";
        if(!$('.datepicker-here').val()){
            $('.datepicker-here').datepicker();
        }
    })(jQuery)
  </script>
@endpush
