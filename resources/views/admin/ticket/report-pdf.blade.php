<!DOCTYPE html>
<html>
<head>
<style>
table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  text-align: center;
}
</style>
</head>
<body style="font-size:10px;">
    <h1>{{ $data['title'] }}</h1>
    <p>{{ $data['start_date'] }} - {{ $data['end_date'] }}</p>
    <table width="100%">
		<thead>
			<tr>
				<th>@lang('User')</th>
				<th>@lang('Kin')</th>
				<th>@lang('PNR Number')</th>
				<th>@lang('Departure Date')</th>
				<th>@lang('Return Date')</th>
				<th>@lang('Trip')</th>
				<th>@lang('Pickup Point')</th>
				<th>@lang('Dropping Point')</th>
				<th>@lang('Booked Seats')</th>
				<th>@lang('Status')</th>
				<th>@lang('Ticket Count')</th>
				{{--<th>@lang('Fare')</th>--}}
			</tr>
		</thead>
		<tbody>
		@foreach($tickets as $item)
			<tr>
				@if($item->customer_name)
				<td data-label="@lang('User')">
					<span class="font-weight-bold">{{ __(@$item->customer_name) }}</span>

				</td>	
				@else
				<td data-label="@lang('User')">
					<span class="font-weight-bold">{{ __(@$item->user->fullname) }}</span>
				<br>
				<span class="small"> <span>@</span>{{ __(@$item->user->username) }} </span>

				</td>
				@endif
				<td data-label="@lang('Kin')">
				@if($item->kin_name)
					<span class="font-weight-bold">Name: {{ __(@$item->kin_name) }}</span>
					<br>
					<span class="font-weight-bold">Email: {{ __(@$item->kin_email) }}</span>
					<br>
					<span class="font-weight-bold">Contact No: {{ __(@$item->kin_contact_no) }}</span>
					<br>
				@endif
				</td>
				<td data-label="@lang('PNR Number')">
					<span class="text-muted">{{ __($item->pnr_number) }}</span>
				</td>
				<td data-label="@lang('Journey Date')">
					{{ __(showDateTime($item->date_of_journey, 'd M, Y')) }}
				</td>
				<td data-label="Return Date">
					{{ __($item->date_of_return ? showDateTime($item->date_of_return , 'd M, Y') : 'N/A' ) }}
				</td>
				<td data-label="@lang('Trip')">
					<span class="font-weight-bold">{{ __($item->trip->fleetType->name) }}</span>
					<br>
					<span class="font-weight-bold"> {{ __($item->trip->startFrom->name ) }} - {{ __($item->trip->endTo->name ) }}</span>
				</td>
				<td data-label="@lang('Pickup Point')">
					{{ __($item->pickup->name) }}
				</td>
				<td data-label="@lang('Dropping Point')">
					{{ __($item->drop->name) }}
				</td>
				<td data-label="@lang('Booked Seats')">
					{{ __(implode(",",$item->seats)) }}
				</td>
				<td data-label="@lang('Status')">
					@if ($item->status == 1)
						<span class="badge badge--success font-weight-normal text--samll">@lang('Booked')</span>
					@elseif($item->status == 2)
						<span class="badge badge--warning font-weight-normal text--samll">@lang('Pending')</span>
					@else
						<span class="badge badge--danger font-weight-normal text--samll">@lang('Rejected')</span>
					@endif
				</td>
				<td data-label="@lang('Ticket Count')">
					{{ __(sizeof($item->seats)) }}
				</td>
				{{--
				<td data-label="@lang('Fare')">
					{{ __(showAmount($item->sub_total)) }} {{ __($general->cur_text) }}
				</td>
				--}}
			</tr>
		@endforeach
		</tbody>
	</table>
</body>
</html>