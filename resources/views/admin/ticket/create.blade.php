@extends('admin.layouts.app')

@section('panel')
    <div class="row">
        <div class="col-md-12">
            <div class="card b-radius--10 ">
               <iframe src="{{ route('adminTicket') }}" width="100%" height="1500"></iframe>
            </div>
        </div>
    </div>
	
@endsection
@push('breadcrumb-plugins')
<form action="{{route('admin.vehicle.ticket.search', $scope ?? str_replace('admin.vehicle.ticket.', '', request()->route()->getName()))}}" method="GET" class="form-inline float-sm-right bg--white mb-2 ml-0 ml-xl-2 ml-lg-0">
    <div class="input-group has_append">
        <input type="text" name="search" class="form-control" placeholder="@lang('Search PNR Number')" value="{{ $search ?? '' }}">
        <div class="input-group-append">
            <button class="btn btn--primary" type="submit"><i class="fa fa-search"></i></button>
        </div>
    </div>
</form>

<form action="{{ Request::url() }}" method="GET" target ="_blank" class="form-inline float-sm-right bg--white">
	<div class="input-group has_append ">
		<input name="date" type="text" data-range="true" data-multiple-dates-separator=" - " data-language="en" class="datepicker-here form-control" data-position='bottom right' placeholder="@lang('Min date - Max date')" autocomplete="off" value="{{ @$dateSearch }}">
		<input type="hidden" name="method" value="{{ @$methodAlias }}">
		<input type="hidden" name="report" value="pdf">
		<div class="input-group-append">
			<button class="btn btn--primary" type="submit"><i class="fa fa-file-pdf"></i></button>
		</div>
	</div>
</form>
@endpush

@push('script-lib')
  <script src="{{ asset('assets/global/js/datepicker.min.js') }}"></script>
  <script src="{{ asset('assets/global/js/datepicker.en.js') }}"></script>
@endpush
@push('script')
  <script>
    (function($){
        "use strict";
        if(!$('.datepicker-here').val()){
            $('.datepicker-here').datepicker();
        }
    })(jQuery)
  </script>
@endpush
