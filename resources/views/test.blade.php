<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
  
<div class="container">
            <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                                                            

			<input type="hidden" name="metadata" value="123" >
																		

			<input type="hidden" name="email" value="arthuralvin88@gmail.com"> {{-- required --}}
											 
			<input type="hidden" name="orderID" value="345">
										 
			 
			<input type="hidden" name="amount" value="10.00"> {{-- required in kobo --}}
																		
			<input type="hidden" name="currency" value="NGN">
																		
			<input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> 
																		{{ csrf_field() }} 
																		
																				   
			 <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">
								
			<i class="fa fa-plus-circle fa-lg"></i> Pay Now!</button>        
			</form>
        </div>
</body>
</html>