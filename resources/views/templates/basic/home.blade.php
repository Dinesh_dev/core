@extends($activeTemplate.'layouts.newfrontend')
@section('content')
    @if($sections->secs != null)
   
        @foreach(json_decode($sections->secs) as $sec)         
            @include($activeTemplate.'sections.'.$sec)
            
        @endforeach
    @endif
    <section class="section-57 section-bg-animation context-dark">
        <div class="shell">
          <div class="range range-xs-center range-xs-middle">
            <div class="cell-md-10 cell-lg-8 text-center text-md-right">
              <h2 class="text-bold">Take Part in the Discount Campaign</h2>
            </div>
            <div class="cell-lg-3 offset-top-30 offset-lg-top-0 text-lg-left"><a href="{{route('ticket')}}" class="btn btn-ripe-lemon">Get Started</a></div>
          </div>
        </div>
      </section>
@endsection
