@extends($activeTemplate.'layouts.newfrontend')
@section('content')
@php
$counters = App\Models\Counter::get();
@endphp
<!-- Ticket Search Starts -->
<section class="blog-section section-90 section-md-111">
    <div class="container" >
        <div class="row">
            <div class="col-md-3">
               
                <form action="{{ route('search') }}" id="filterForm">
                    @if(request()->has('trip'))
                        <input type="text" name="trip" id="" value="{{request()->get('trip')}}" hidden>
                    @endif
                    @if(request()->has('pickup'))
                    <input type="text" name="pickup" id="" value="{{request()->get('pickup')}}" hidden>
                    @endif
                    @if(request()->has('destination'))
                    <input type="text" name="destination" id="" value="{{request()->get('destination')}}" hidden>
                    @endif
                    @if(request()->has('date_of_journey'))
                    <input type="text" name="date_of_journey" autocomplete="off" id="date_of_journey_filter" value="{{request()->get('date_of_journey')}}" hidden>
                    @endif
                    <div class="ticket-filter">
                        <div class="filter-header filter-item">
                            <h4 class="title mb-0" style="margin-bottom: 0px">@lang('Filter')</h4>
                            <button type="reset" class="reset-button-new btn-xs btn-tag btn btn-ripe-lemon">@lang('Reset All')</button>
                        </div>
                        @if($fleetType)
                        <div class="filter-item">
                            <h5 class="title">@lang('Vehicle Type')</h5>
                            <ul class="bus-type">
                                @foreach ($fleetType as $fleet)
                                <li class="custom--checkbox">
                                    <input name="fleetType[]" class="search" value="{{ $fleet->id }}" id="{{ $fleet->name }}" type="checkbox" @if (request()->fleetType)
                                    @foreach (request()->fleetType as $item)
                                    @if ($item == $fleet->id)
                                    checked
                                    @endif
                                    @endforeach
                                    @endif >
                                    <label for="{{ $fleet->name }}"><span><i class="fa fa-bus "></i>{{ __($fleet->name) }}</span></label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @if ($routes)
                        <div class="filter-item">
                            <h5 class="title">@lang('Routes')</h5>
                            <ul class="bus-type">
                                @foreach ($routes as $route)
                                <li class="custom--checkbox">
                                    <input name="routes[]" class="search" value="{{ $route->id }}" id="route.{{ $route->id }}" type="checkbox" @if (request()->routes)
                                    @foreach (request()->routes as $item)
                                    @if ($item == $route->id)
                                    checked
                                    @endif
                                    @endforeach
                                    @endif >
                                    <label for="route.{{ $route->id }}"><span><span><i class="fa  fa-road"></i> {{ __($route->name) }} </span></label>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif

                        @if ($schedules)
                        <div class="filter-item">
                           
                            @if(request()->has('date_of_journey') && !is_null(request()->get('date_of_journey')))
                               
                          
                            <h5 class="title">@lang('Choose Date')</h5>
                            <input type="text" id="date_of_journey_two" autocomplete="off" name="" value="{{request()->get('date_of_journey')}}" class="date_of_journey form-control" placeholder="dd-mm-yy" style="cursor: pointer"/>
                              <h5 class="title">@lang('Schedule')</h5>
                            <ul class="bus-type">

                                @foreach ($schedules as $schedule)
                                <li class="custom--checkbox">
                                    <input name="schedules[]" class="search" value="{{ $schedule->id }}" id="schedule.{{ $schedule->id }}" type="checkbox" @if (request()->schedules)
                                    @foreach (request()->schedules as $item)
                                    @if ($item == $schedule->id)
                                    checked
                                    @endif
                                    @endforeach
                                    @endif>
                                    <label for="schedule.{{ $schedule->id }}"><span><span><i class="fa  fa-clock-o"></i> {{ showDateTime($schedule->start_from, 'h:i a').' - '. showDateTime($schedule->end_at, 'h:i a') }} </span></label>
                                </li>
                                @endforeach
                            </ul>
                           
                            @else 
                            <h5 class="title">@lang('Choose Date')</h5>
                            <div class="input-group date">
                                <input type="text"  name="date_of_journey" id="date_of_journey_two" autocomplete="off" class="date_of_journey form-control" placeholder="dd-mm-yy" style="cursor: pointer"/>
                            </div>
                            @endif
                            
                        </div>
                        @endif
                    </div>
                </form>
               

            </div>
            <div class="col-md-9">
                <div class="ticket-wrapper">
                    @forelse ($trips as $trip)
                        @php
                        $start = Carbon\Carbon::parse($trip->schedule->start_from);
                        $end = Carbon\Carbon::parse($trip->schedule->end_at);
                        $diff = $start->diff($end);
                        $ticket = App\Models\TicketPrice::where('fleet_type_id', $trip->fleetType->id)->where('vehicle_route_id', $trip->route->id)->first();
                        @endphp
                         <div class="ticket-item">
                            <div class="ticket-item-inner">
                                <span class="ratting"><i class="fa fa-bus"></i>{{ __($trip->fleetType->name) }}</span>

                                <h5 class="bus-name" style="margin-bottom: 0px">{{ __($trip->title) }}</h5>
                                <span class="bus-info">@lang('Seat Layout - ') {{ __($trip->fleetType->seat_layout) }}</span>
                                <div style="display:flex;align-items:center">
                                    @isset($ticket->price)
                                   
                                        <button class="btn-xs btn-tag btn btn-ripe-lemon" >{{ showAmount($ticket->price) }} {{ __($general->cur_sym) }}</button>
                                    @endisset
                                    <p class="place" style="margin-left:20px"> <i class="fa fa-calendar "></i> {{$trip->scheduledate}}</p>
                                </div>
                            </div>
                            <div class="ticket-item-inner travel-time">
                                <div class="bus-time">
                                    <p class="time">{{ showDateTime($trip->schedule->start_from, 'h:i A') }}</p>
                                    <p class="place">{{ __($trip->startFrom->name) }}</p>
                                </div>
                                <div class=" bus-time">
                                    <i class="fa fa-arrow-right"></i>
                                    <p>{{ $diff->format('%H:%I min') }}</p>
                                </div>
                                <div class=" bus-time">
                                    <p class="time">{{ showDateTime($trip->schedule->end_at, 'h:i A') }}</p>
                                    <p class="place">{{ __($trip->endTo->name) }}</p>
                                </div>
                            </div>
                            <div class="ticket-item-inner book-ticket" style="color:#173573;display: flex;
                            gap: 10px;
                            align-items: center;justify-content:flex-end" >
                                
                                <a class="btn btn-default" href="{{ route('ticket.seats', [$trip->id, slug($trip->title)]) }}">@lang('Select Seat')</a>
                               
                            </div>
                         </div>
                    @empty
                        <div class="ticket-item">
                            <h5>{{ __($emptyMessage) }}</h5>
                        </div>
                    @endforelse

                </div> 
                <div class="offset-md-top-30">
                    @if ($trips->hasPages())
                    {{ paginateLinksnew($trips) }}
                    @endif   

                </div>
            </div>
        
        </div>
    </div>
</section>

<!-- Ticket Section Starts Here -->

@endsection
@push('script')
<script>
    (function($) {
        "use strict";
        $('.search').on('change', function() {
            $('#filterForm').submit();
        });

        $('.reset-button-new').on('click', function() {
            // $('.search').attr('checked', false);
            // $('#filterForm').submit();
            // location.reload();
            location.href="{{route('ticket')}}";
        })
    })(jQuery)


	
	$('#trip').on('change', function() {
		if($('#trip').val() == 'round_trip')
		{
			$('#date_of_return_div').removeClass('d-none');
		}
		else
		{
			$('#date_of_return_div').addClass('d-none');
		}
	})
	
	@if(request()->date_of_return)
		$('#date_of_return_div').removeClass('d-none');
	@endif

    $('#date_of_journey_two').on('change',function(){
        $('#date_of_journey_filter').val($('#date_of_journey_two').val());
        if($('#date_of_journey_two').val()){
            $('#filterForm').submit();
        }
       
    });
    $('#date_of_journey_two').on('change',function(){
        $('#date_of_journey_filter').val($('#date_of_journey_two').val());
        if($('#date_of_journey_two').val()){
            $('#filterForm').submit();
        }
    });

    
</script>
@endpush