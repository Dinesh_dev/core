@extends($activeTemplate.'layouts.newfrontend')
@section('content')
 <!-- Privacy Policy Section Starts Here -->
<section class="section-90 section-md-111">
    <div class="shell">
        <div class="range range-xs-center range-lg-right">
            <div class="cell-sm-10 cell-md-12">
                <div class="privacy-policy-content" style="text-align: left">
                    <p>
                        @php
                            echo stripStyle(@$policy->data_values->details);
                        @endphp
                    </p>
                </div>
            </div>
        </div>
    </div>
    
</section>
<!-- Privacy Policy Section Ends Here -->
@endsection
