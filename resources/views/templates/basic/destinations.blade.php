@extends($activeTemplate.'layouts.newfrontend')

@section('content')
@php 
$counters = App\Models\Counter::get();
@endphp
<section class="section-90 section-md-111 text-left bg-zircon">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-md-8">
          <h2 class="text-bold text-md-left">Where Will You Go?</h2>
          <hr class="divider hr-md-left-0 bg-chathams-blue">
          <div class="offset-top-30 offset-md-top-60">
            <p>If you’re planning to travel to America, there are so many places to visit. From the historic landmarks of Boston and Washington to the rich, vibrant culture of San Francisco and the famous Staute of Liberty in New York, so when it comes to planning your American trip it’s essential to book your bus tickets well in advance.</p>
          </div>
          <div class="range  offset-top-30 offset-md-top-60">
            @foreach($top_trips as $item)
            <div class="cell-xs-10 cell-sm-6">  
              <div class="offset-top-30 box-member box-member-type-2 box-member-modern box-member-caption-offset">
                <div class="box-member-img-wrap">
                    <img src="{{ env('APP_URL')}}/assets/vehicles/images/display/{{ $item->image }}" alt="" class="img-responsive" width="480" height="550">
                    
                    {{-- <img src="images/pages/destinations-01-370x370.jpg" width="480" height="550" alt="" class="img-responsive"/> --}}
                </div>
                <div class="box-member-wrap">
                  <div class="box-member-caption">
                    <div class="box-member-caption-inner">
                      <div class="box-member-caption-wrap">
                        <div class="box-member-title">
                          <div class="h4 text-bold text-white">
                            <a href="{{ route('ticket.seats', [$item->id, slug($item->title)]) }}"> {{$item->start_from}} - {{$item->end_to}}
                               
                            </a>
                          </div>
                        </div>
                        <div class="box-member-description offset-top-6">
                          <p></p>
                        </div>
                        <div class="offset-top-20"><a href="{{ route('ticket.seats', [$item->id, slug($item->title)]) }}" class="btn btn-ripe-lemon">Book Ticket</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
           
          </div>
          <div class="text-center text-md-left offset-top-60">
            @if ($top_trips->hasPages())
            {{ paginateLinksNew($top_trips) }}
            @endif
            <!-- Classic Pagination-->
            {{-- <nav>
              <ul class="pagination-classic">
                <li><a href="#" class="icon icon-xxs icon-primary fa fa-chevron-left"></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#" class="icon icon-xxs icon-primary fa fa-chevron-right"></a></li>
              </ul>
            </nav> --}}
          </div>
        </div>
        <div class="cell-md-4 offset-top-60 offset-md-top-0">
          <div class="inset-md-left-30">
            <!-- Panel-->
            <div class="panel panel-xl panel-vertical panel-sm-resize panel-md-resize section-bottom-40 bg-chathams-blue context-dark text-lg-left">
              <h5 class="text-bold"><span class="big"><span class="big"><span class="big">Find Bus Tickets</span></span></span></h5>
              <form action="{{ route('search') }}" class="offset-top-25">
                <div class="group group-top">
                  <div class="group-item element-fullwidth">
                    <div class="form-group text-left">
                        <label style="color: white;">Ticket Type</label>
                        <div class="form--group">
                            <i class="las la-bus"></i>
                            <select name="trip" id="trip" class="form--control select2">
                              @foreach (['one_way' => 'One-way', 'round_trip' => 'Round Trip'] as $trip_key => $trip_value)
                              <option value="{{ $trip_key }}" @if(request()->trip ==$trip_key) selected @endif>{{ __($trip_value) }}</option>
                              @endforeach
                            </select>
                        </div>
                    </div>
                  </div>
                  <div class="group-item element-fullwidth offset-top-6 offset-xs-top-0 offset-lg-top-6">
                    <div class="form-group text-left">
                        <label style="color: white;">Pickup Point</label>
                        <div class="form--group">
                            <i class="las la-location-arrow"></i>
                            <select class="form--control select2" name="pickup">
                                <option value="">@lang('Pickup Point')</option>
                                @foreach ($counters as $counter)
                                <option value="{{ $counter->id }}" @if(request()->pickup == $counter->id) selected @endif>{{ __($counter->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                  </div>
                  <div class="group-item element-fullwidth offset-top-6 offset-md-top-0 offset-lg-top-6">
                    <div class="form-group text-left">
                        <label style="color: white;">Dropping Point</label>
                        <div class="form--group">
                            <i class="las la-map-marker"></i>
                            <select name="destination" class="form--control select2">
                                <option value="">@lang('Dropping Point')</option>
                                @foreach ($counters as $counter)
                                <option value="{{ $counter->id }}" @if(request()->destination == $counter->id) selected @endif>{{ __($counter->name) }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                  </div>


                  <div class="group-item element-fullwidth offset-top-6 offset-md-top-0 offset-lg-top-6">
                    <div class="form-group text-left">
                        <label style="color: white;">Departure Date</label>
                        <div class="form--group">
                            <i class="las la-calendar-check"></i>
                            <input style="width: 100%;
                            height: 46px;
                            color: black !important;" type="date" autocomplete="off" name="date_of_journey" class="form--control datepicker" placeholder="@lang('Departure Date')" autocomplete="off">
                        </div>
                    </div>
                  </div>



                  <div class="group-item reveal-block reveal-md-inline-block text-center text-md-left offset-top-15">
                    <button type="button" class="btn btn-ripe-lemon shadow-drop-md">Search</button>
                  </div>
                </div>
              </form>
            </div>
            <!-- Testimonials-->
            {{-- <div class="offset-top-60">
              <h5 class="text-bold">Testimonials</h5>
            </div>
            <div class="offset-top-6">
              <div class="text-subline"></div>
            </div>
           
            <div data-items="1" data-loop="false" data-stage-padding="5" data-margin="10" data-nav="false" data-dots="true" class="owl-carousel owl-carousel-classic owl-dots-25 text-center text-md-left">
              <div class="owl-item">
              
                <blockquote class="quote quote-classic offset-top-25">
                
                  <div class="media">
                    <div class="media-left"><span class="icon icon-xs fa fa-quote-left text-primary"></span></div>
                    <div class="media-body">
                      <p class="h6 text-italic">
                        <q>I haven't ever booked any ticket before BusExpress, but the site looks great as if we are booking an air ticket. Great work! I will definitely book tickets from your company from now on!</q>
                      </p>
                      <div class="offset-top-10">
                        <div class="unit unit-middle unit-horizontal unit-spacing-xs">
                          <div class="unit-left"><img src="images/users/user-tyheresa-barrett-40x40.jpg" width="40" height="40" alt="" class="img-circle img-responsive center-block"></div>
                          <div class="unit-body">
                            <p class="quote-author"><a href="team-member-profile.html" class="text-gray">Theresa Barrett</a></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </blockquote>
              </div>
              <div class="owl-item">
                <!-- Classic Blockquote-->
                <blockquote class="quote quote-classic offset-top-25">
                  <!-- Media-->
                  <div class="media">
                    <div class="media-left"><span class="icon icon-xs fa fa-quote-left text-primary"></span></div>
                    <div class="media-body">
                      <p class="h6 text-italic">
                        <q>It’s a perfect service for booking bus tickets online! I got the number from a helpline at 2.00 pm & by 3.30 I got my tickets booked at 9.00pm, when others had no seats.</q>
                      </p>
                      <div class="offset-top-10">
                        <div class="unit unit-middle unit-horizontal unit-spacing-xs">
                          <div class="unit-left"><img src="images/users/user-roger-washington-40x40.jpg" width="40" height="40" alt="" class="img-circle img-responsive center-block"></div>
                          <div class="unit-body">
                            <p class="quote-author"><a href="team-member-profile.html" class="text-gray">Roger Washington</a></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </blockquote>
              </div>
              <div class="owl-item">
                <!-- Classic Blockquote-->
                <blockquote class="quote quote-classic offset-top-25">
                  <!-- Media-->
                  <div class="media">
                    <div class="media-left"><span class="icon icon-xs fa fa-quote-left text-primary"></span></div>
                    <div class="media-body">
                      <p class="h6 text-italic">
                        <q>Thank you very much for support. Keep going like this and I wish you people to be the No.1 Online Bus Ticket provider in our country. You are really doing a good job.</q>
                      </p>
                      <div class="offset-top-10">
                        <div class="unit unit-middle unit-horizontal unit-spacing-xs">
                          <div class="unit-left"><img src="images/users/user-jean-kennedy-40x40.jpg" width="40" height="40" alt="" class="img-circle img-responsive center-block"></div>
                          <div class="unit-body">
                            <p class="quote-author"><a href="team-member-profile.html" class="text-gray">Jean Kennedy</a></p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </blockquote>
              </div>
            </div> --}}
            <div class="offset-top-42 text-center text-md-left">
              <!-- Box-->
              {{-- <div style="background: #191919 url(images/pages/destinations-07-370x370.jpg) center/cover no-repeat" class="box">
                <h3 class="text-bold text-white">Take Part <br class="veil reveal-lg-inline-block"> in the Discount Campaign</h3>
                <div class="offset-top-15 offset-md-top-30"><a href="single-tour.html" class="btn btn-ripe-lemon">Get Started</a></div>
              </div> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection