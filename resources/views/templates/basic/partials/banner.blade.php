@php
$contents = getContent('banner.content',true);
$counters = App\Models\Counter::get();
@endphp

@php
$contents = getContent('banner.content',true);
$counters = App\Models\Counter::get();
@endphp
<!-- Banner Section Starts Here -->
<div data-height="600px" data-min-height="200px" data-slide-effect="fade" data-simulate-touch="false" class="swiper-container swiper-slider context-dark text-lg-left">
          <div class="swiper-wrapper">
            <div 
            data-slide-bg='{{getImage("assets/images/frontend/banner/background-02-1920x950.jpg")}}'  class="swiper-slide"
            style='background-image: url({{getImage("assets/images/frontend/banner/background-02-1920x950.jpg")}});background-size: cover;width: 1332px;transform: translate3d(0px, 0px, 0px);transition-duration: 0ms;opacity: 1;'
            
            >
              <div class="swiper-slide-caption-wrap">
                <div class="swiper-slide-caption">
                  <div class="shell">
                    <div class="range range-xs-center range-lg-left section-sm-50 section-md-0">
                      <div class="cell-xs-10 cell-lg-7 cell-xl-6">
                        <h1 data-caption-animate="fadeInDown" data-caption-delay="200" class="text-bold" style="color:#fff">
                            <!-- {{ __($contents->data_values->heading) }} -->
                        Get Your Ticket Online,<br> Easy and Safely

                        </h1>
                        <!-- <div data-caption-animate="fadeInUp" data-caption-delay="600" class="offset-top-20 offset-lg-top-49">
                          <p class="h6 text-mercury">BusExpress is the leading go-to website for booking inter-city bus <br class="veil reveal-lg-inline-block"> online.</p>
                        </div> -->
                        <div data-caption-animate="fadeInUp" data-caption-delay="800" class="offset-top-20">
                            <a href="{{ __(@$contents->data_values->link) }}" class="btnbanner btn-default btn-skew" style="border:1px solid #dddd">{{ __(@$contents->data_values->link_title) }}</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div data-slide-bg="url({{ getImage('assets/images/frontend/banner/'.$contents->data_values->background_image, "1500x88") }}) repeat-x bottom;" class="swiper-slide">
              <div class="swiper-slide-caption-wrap">
                <div class="swiper-slide-caption">
                  <div class="shell">
                    <div class="range range-xs-center range-lg-left">
                      <div class="cell-xs-10 cell-lg-9 cell-xl-7">
                        <h1 data-caption-animate="fadeInDown" data-caption-delay="200" class="text-bold">Providing quality service at unbeatable rates.</h1>
                        <div data-caption-animate="fadeInUp" data-caption-delay="600" class="offset-top-20 offset-lg-top-49">
                          <p class="h6 text-mercury">We provide affordable and reliable bus ticket booking services online <br class="veil reveal-lg-inline-block"> all over the world.</p>
                        </div>
                        <div data-caption-animate="fadeInUp" data-caption-delay="800" class="offset-top-20"><a href="deals.html" class="btn btn-default btn-skew">Learn More</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> -->
          </div>
          <div class="swiper-center-caption">
            <div class="shell bannershell">
              <div class="range range-xs-center">
                <div class="cell-sm-10 cell-md-12">
                  <!-- Panel-->
                  <div class="panel panel-lg bg-overlay-chathams-blue text-lg-left" >
                    <h3><span class="small text-bold text-white" >Find Bus Tickets</span></h3>
                    <form action="{{ route('search') }}" class="ticket-form ticket-form-new row g-3 justify-content-center m-0">
                            <div class="col-md-2">
                                <label style="color: white;">Ticket Type</label>
                                <div class="form--group">
                                    <i class="las la-bus"></i>
                                    <select name="trip" id="trip" class="form--control select2">
                                      @foreach (['one_way' => 'One-way', 'round_trip' => 'Round Trip'] as $trip_key => $trip_value)
                                      <option value="{{ $trip_key }}" @if(request()->trip ==$trip_key) selected @endif>{{ __($trip_value) }}</option>
                                      @endforeach
                                    </select>
                                </div>
                            </div>
							              <div class="col-md-3">
                                <label style="color: white;">Pickup Point</label>
                                <div class="form--group">
                                    <i class="las la-location-arrow"></i>
                                    <select class="form--control select2" name="pickup">
                                        <option value="">@lang('Pickup Point')</option>
                                        @foreach ($counters as $counter)
                                        <option value="{{ $counter->id }}" @if(request()->pickup == $counter->id) selected @endif>{{ __($counter->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label style="color: white;">Dropping Point</label>
                                <div class="form--group">
                                    <i class="las la-map-marker"></i>
                                    <select name="destination" class="form--control select2">
                                        <option value="">@lang('Dropping Point')</option>
                                        @foreach ($counters as $counter)
                                        <option value="{{ $counter->id }}" @if(request()->destination == $counter->id) selected @endif>{{ __($counter->name) }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label style="color: white;">Departure Date</label>
                                <div class="form--group">
                                    <i class="las la-calendar-check"></i>
                                    <input type="text" name="date_of_journey" class="form--control datepicker" placeholder="@lang('Departure Date')" autocomplete="off">
                                </div>
                            </div>
                            <div id="date_of_return_div" class="col-md-2 d-none">
                              <div class="form--group">
                                <i class="las la-calendar-check"></i>
                                <input type="text" name="date_of_return" id="date_of_return" class="form--control datepicker" placeholder="@lang('Return Date')" autocomplete="off" value="{{ request()->date_of_return }}">
                              </div>
                            </div>
                            <div class="col-md-2" >
                                <div class="form--group" style="margin-top: 35px;">
                                    <button class="btn-ripe-lemon">@lang('Find Tickets')</button>
                                </div>
                            </div>
                        </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- Swiper Pagination-->
          <div class="swiper-pagination"></div>
          <!-- Swiper Navigation-->
          <div class="swiper-button-prev"><span class="icon icon-xs fa fa-chevron-left"></span></div>
          <div class="swiper-button-next"><span class="icon icon-xs fa fa-chevron-right"></span></div>
    </div>




@push('script')
<script src="{{asset('assets/templates/basic/js/core.min.js')}}"></script>
<script src="{{asset('assets/templates/basic/js/script.js')}}"></script>

<script>
$('#trip').on('change', function() {
    if($('#trip').val() == 'round_trip')
    {
        $('#date_of_return_div').removeClass('d-none');
    }
    else
    {
        $('#date_of_return_div').addClass('d-none');
    }
})

@if(request()->date_of_return)
    $('#date_of_return_div').removeClass('d-none');
@endif
</script>
@endpush
