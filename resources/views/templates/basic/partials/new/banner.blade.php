<!-- Swiper-->
<div data-autoplay = "true" data-height="" data-min-height="200px" data-slide-effect="fade" data-simulate-touch="false" class="swiper-container swiper-slider context-dark text-lg-left">
    <div class="swiper-wrapper">
      <div data-slide-bg='{{getImage("assets/images/frontend/banner/background-02-1920x950.jpg")}}' class="swiper-slide">
        <div class="swiper-slide-caption-wrap">
          <div class="swiper-slide-caption">
            <div class="shell">
              <div class="range range-xs-center range-lg-left section-sm-50 section-md-0">
                <div class="cell-xs-10 cell-lg-7 cell-xl-6">
                  <h1 data-caption-animate="fadeInDown" data-caption-delay="200" class="text-bold">Comfort & Style <br class="veil reveal-lg-inline-block"> Over Every Mile</h1>
                  <div data-caption-animate="fadeInUp" data-caption-delay="600" class="offset-top-20 offset-lg-top-49">
                    <p class="h6 text-mercury"> The leading go-to website for booking  bus <br class="veil reveal-lg-inline-block"> online.</p>
                  </div>
                  <div data-caption-animate="fadeInUp" data-caption-delay="800" class="offset-top-20"><a href="{{route('ticket')}}" class="btn btn-default btn-skew">Book Now</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div  data-slide-bg='{{getImage("assets/images/frontend/banner/background-03-1920x950.jpg")}}' class="swiper-slide">
        <div class="swiper-slide-caption-wrap">
          <div class="swiper-slide-caption">
            <div class="shell">
              <div class="range range-xs-center range-lg-left">
                <div class="cell-xs-10 cell-lg-9 cell-xl-7">
                  <h1 data-caption-animate="fadeInDown" data-caption-delay="200" class="text-bold">Providing quality service at unbeatable rates.</h1>
                  <div data-caption-animate="fadeInUp" data-caption-delay="600" class="offset-top-20 offset-lg-top-49">
                    <p class="h6 text-mercury">We provide affordable and reliable bus ticket booking services online <br class="veil reveal-lg-inline-block"> all over the world.</p>
                  </div>
                  <div data-caption-animate="fadeInUp" data-caption-delay="800" class="offset-top-20"><a href="{{route('ticket')}}" class="btn btn-default btn-skew">Learn More</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-center-caption">
      <div class="shell">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-md-12">
            <!-- Panel-->
            <div class="panel panel-lg bg-overlay-chathams-blue text-lg-left">
              <h3><span class="small text-bold text-white">Find Bus Tickets</span></h3>
              @include($activeTemplate.'partials.new.ticketsearchform')
              
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Swiper Pagination-->
    <div class="swiper-pagination"></div>
    <!-- Swiper Navigation-->
    <div class="swiper-button-prev"><span class="icon icon-xs fa fa-chevron-left"></span></div>
    <div class="swiper-button-next"><span class="icon icon-xs fa fa-chevron-right"></span></div>
  </div>
  
  