@php
$content = getContent('contact.content', true);
@endphp
@php
$contents = getContent('banner.content',true);
$counters = App\Models\Counter::get();
@endphp

@php
$contents = getContent('banner.content',true);
$counters = App\Models\Counter::get();
@endphp

<header class="page-head slider-menu-position">
        <!-- RD Navbar Transparent-->
        <div class="rd-navbar-wrap">
          <nav data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-md-stick-up-offset="90px" data-lg-stick-up-offset="75px" data-auto-height="false" class="rd-navbar rd-navbar-top-panel rd-navbar-default rd-navbar-white rd-navbar-static-fullwidth-transparent" data-lg-auto-height="true" data-md-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
            <div class="rd-navbar-inner">
              <!-- RD Navbar Top Panel-->
              <div class="rd-navbar-top-panel">
                <div class="left-side">
                  <!-- Contact Info-->
                  <address class="contact-info text-left">
                    <div class="reveal-inline-block"><a href="callto:#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle "><i class="fa fa-phone"></i></span></span><span class="unit-body"><span class="text-gray-darker">+234 803 771 7320</span></span></a></div>
                    <div class="reveal-inline-block"><a href="mailto:#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-email-outline"></span></span><span class="unit-body"><span class="text-gray-darker">uniquemasstransport@gmail.com</span></span></a></div>
                    <div class="reveal-inline-block">
                    <ul class="list-inline list-inline-2">
                    <li><a href="https://www.facebook.com/profile.php?id=100087385396788" class="icon icon-xxs icon-silver-filled icon-circle fa fa-facebook"></a></li>
                    <li><a href="https://twitter.com/uniquemas1" class="icon icon-xxs icon-silver-filled icon-circle fa fa-twitter"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-google-plus"></a></li>
                    <li><a href="https://www.instagram.com/uniquemasstransport/?igshid=YmMyMTA2M2Y=" class="icon icon-xxs icon-silver-filled icon-circle fa fa-instagram"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-rss"></a></li>
                  </ul>
                    </div>
                  </address>
                </div>
                <div class="right-side">
                  <!-- <div class="goaccount">
                    <a href="#"><i class="fas fa-sign-in-alt"></i> <span>Sign In</span> </a>
                    <span>/</span>
                    <a href="#"><i class="fas fa-user-plus"></i> <span>Sign Up</span></a>
                  </div> -->
                  @guest
                  <ul class="list-inline list-inline-2" style="padding:5px !important;border:1px solid #ddd;">
                    <li><a href="{{ route('user.login') }}" class="" style="font-size:14px;color:#3256a4"><i class="fa fa-sign-in"></i> Sign In</a></li>
                    <li><a href="#" style="color:#3256a4">/</a></li>
                    <li><a href="{{ route('user.register') }}" class="" style="font-size:14px;"><i class="fa fas fa-user-plus" style="color:#3256a4"></i> Sign up</a></li>
                    
                  </ul>
                  @endguest
                  @auth
                  <ul class="list-inline list-inline-2" style="padding:5px !important;border:1px solid #ddd;">
                    <li><a href="{{ route('user.home') }}" class="" style="font-size:14px;color:#3256a4"><i class="fa fa-sign-in"></i> Dashboard</a></li>
                    
                  </ul>
                  @endauth
                </div>
              </div>
              <!-- RD Navbar Panel-->
              <div class="rd-navbar-panel">
                <!-- RD Navbar Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                <!-- RD Navbar Top Panel Toggle-->
                <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-top-panel" class="rd-navbar-top-panel-toggle"><span></span></button>
                <!-- Navbar Brand-->
                <div class="rd-navbar-brand"><a href="{{route('home')}}">
                  <img width='97' height='57' src="{{ getImage(imagePath()['logoIcon']['path'].'/logo_2.jpeg') }}" alt="@lang('Logo')"/></a></div>
              </div>
              <div class="rd-navbar-menu-wrap">
                <div class="rd-navbar-nav-wrap">
                  <div class="rd-navbar-mobile-scroll">
                    <!-- Navbar Brand Mobile-->
                    <div class="rd-navbar-mobile-brand"><a href="{{route('home')}}">
                      <img width='97' height='57' src="{{ getImage(imagePath()['logoIcon']['path'].'/logo_2.jpeg') }}" alt=''/></a></div>
                    <div class="form-search-wrap">
                      <!-- RD Search Form-->
                      <form action="search-results.html" method="GET" class="form-search rd-search">
                        <div class="form-group">
                          <label for="rd-navbar-form-search-widget" class="form-label form-search-label form-label-sm rd-input-label">Search</label>
                          <input id="rd-navbar-form-search-widget" type="text" name="s" autocomplete="off" class="form-search-input input-sm form-control form-control-gray-lightest input-sm"/>
                        </div>
                        <button type="submit" class="form-search-submit"><span class="fa fa-search"></span></button>
                      </form>
                    </div>
                    <!-- RD Navbar Nav-->
                    @if(Auth::guest())
                      <ul class="rd-navbar-nav">
                        <li class="{{request()->routeIs('home')?'active':''}}"><a href="{{route('home')}}">Home</a></li>
                        @foreach($pages as $k => $data)
                        <li class="{{request()->url() == route('pages',[$data->slug])?'active':''}}" >
                            <a href="{{route('pages',[$data->slug])}}">{{__($data->name)}}</a>
                        </li>
                        @endforeach
                        <li class="{{request()->routeIs('blog')?'active':''}}">
                            <a href="{{ route('blog') }}">@lang('Blog')</a>
                        </li>
                        <li class="{{request()->routeIs('track')?'active':''}}">
                            <a href="{{ route('track') }}">@lang('Logistics Tracker')</a>
                        </li>
                        <li class="{{request()->routeIs('contact')?'active':''}}">
                            <a href="{{ route('contact') }}">@lang('Contact')</a>
                        </li>
                        @if(!request()->routeIs('ticket'))
                        <li>
                          <a href="{{ route('ticket') }}" class="cmn--btn btn--sm top-btn" style="margin-top:-5px;background:linear-gradient(45deg, #18377a 0%, #3256a4 100%);text-transform:inherit">@lang('Buy Tickets')</a>
                        </li>
                        @endif
                      </ul>
                    @else
                      <ul class="rd-navbar-nav">
                        <li>
                          <a href="{{ route('user.home') }}">@lang('Dashboard')</a>
                      </li>
                      <li>
                          <a href="{{ route('user.track') }}">@lang('Logistics Tracker')</a>
                      </li>
                      <li class="rd-navbar--has-dropdown rd-navbar-submenu">
                        <a href="javascript::void()">@lang('Booking')</a>
                        <!-- RD Navbar Dropdown-->
                        <ul class="rd-navbar-dropdown">
                          <li><a href="{{ route('ticket') }}">@lang('Buy Ticket')</a></li>
                          <li><a href="{{ route('user.ticket.history') }}">@lang('Booking History')</a></li>
                          
                        </ul>
                      </li>
                     
                        <li class="rd-navbar--has-dropdown rd-navbar-submenu">
                            <a href="javascript::void()">@lang('Support Ticket')</a>
                            <ul class="rd-navbar-dropdown">
                                <li>
                                    <a href="{{route('ticket.open')}}">@lang('Create New')</a>
                                </li>
                                <li>
                                    <a href="{{route('support_ticket')}}">@lang('Tickets')</a>
                                </li>
                            </ul>
                        </li>

                        <li class="rd-navbar--has-dropdown rd-navbar-submenu">
                            <a href="#0">@lang('Profile')</a>
                            <ul class="rd-navbar-dropdown">
                                <li>
                                    <a href="{{ route('user.profile.setting') }}">@lang('Profile')</a>
                                </li>
                                <li>
                                    <a href="{{ route('user.change.password') }}">@lang('Change Password')</a>
                                </li>
                                <li>
                                    <a href="{{ route('user.logout') }}">@lang('Logout')</a>
                                </li>
                            </ul>
                        </li>
                      </ul>
                    
                   
                    @endif
                  </div>
                </div>
                <!-- RD Navbar Search-->
                <!-- <div class="rd-navbar-search rd-navbar-search-top-panel"><a data-rd-navbar-toggle=".rd-navbar-inner,.rd-navbar-search" href="#" class="rd-navbar-search-toggle mdi"><span></span></a>
                  <form action="search-results.html" data-search-live="rd-search-results-live" method="GET" class="rd-navbar-search-form search-form-icon-right rd-search">
                    <div class="form-group">
                      <label for="rd-navbar-search-form-input" class="form-label rd-input-label">Type and hit enter...</label>
                      <input id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off" class="rd-navbar-search-form-input form-control form-control-gray-lightest"/>
                    </div>
                    <div id="rd-search-results-live" class="rd-search-results-live"></div>
                  </form>
                </div> -->
              </div>
            </div>
          </nav>
        </div>
        @if($banner)
          @include($activeTemplate .'partials.new.banner')
        @else
          @include($activeTemplate.'partials.new.breadcrumb') 
        @endif
</header>