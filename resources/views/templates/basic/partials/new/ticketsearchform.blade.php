<form action="{{ route('search') }}" id="ticketSearchForm" class="ticket-form ticket-form-new row g-3 justify-content-center m-0">
    <div class="col-md-2">
        <label style="color: white;">Ticket Type</label>
        <div class="form--group">
            <i class="las la-bus"></i>
            <select name="trip" id="trip" class="form--control select2">
              @foreach (['one_way' => 'One-way', 'round_trip' => 'Round Trip'] as $trip_key => $trip_value)
              <option value="{{ $trip_key }}" @if(request()->trip ==$trip_key) selected @endif>{{ __($trip_value) }}</option>
              @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3">
    <label style="color: white;">Pickup Point</label>
        <div class="form--group">
            <i class="las la-location-arrow"></i>
            <select class="form--control select2" name="pickup">
                <option value="">@lang('Pickup Point')</option>
                @foreach ($counters as $counter)
                <option value="{{ $counter->id }}" @if(request()->pickup == $counter->id) selected @endif>{{ __($counter->name) }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <label style="color: white;">Dropping Point</label>
        <div class="form--group">
            <i class="las la-map-marker"></i>
            <select name="destination" class="form--control select2">
                <option value="">@lang('Dropping Point')</option>
                @foreach ($counters as $counter)
                <option value="{{ $counter->id }}" @if(request()->destination == $counter->id) selected @endif>{{ __($counter->name) }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="col-md-2">
        <label style="color: white;">Departure Date</label>
        <div class="form--group">
            <div class="input-group date">
            <input type="text" class="date_of_journey form-control" autocomplete="off"  id="date_of_journey" autocomplete="off" name="date_of_journey" placeholder="DD-MM-YY"  style="cursor: pointer;color:black;"/>
            </div>   
        </div>
    </div>
   
    <div class="col-md-2 cell-md-2 cell-lg-2" >
      <div class="reveal-block reveal-md-inline-block">
        <button type="submit" style="max-width: 147px; min-width: 147px; min-height: 50px;margin-top:28px;" class="shadow-drop-md btn btn-ripe-lemon element-fullwidth">search</button>
      </div>
       
    </div>
</form>

