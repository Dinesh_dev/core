@php
    $content = getContent('breadcrumb.content', true);
@endphp
<!-- Modern Breadcrumbs-->
<section class="section-height-800 breadcrumb-modern rd-parallax context-dark bg-gray-darkest text-lg-left">
    <div data-speed="0.2" data-type="media" data-url="{{getImage("assets/images/frontend/breadcrumb/".@$content->data_values->background_image, "1920x1288")}}" class="rd-parallax-layer"></div>
    <div data-speed="0" data-type="html" class="rd-parallax-layer">
      <div class="bg-primary-chathams-blue-reverse">
        <div class="shell section-top-57 section-bottom-30 section-md-top-210 section-lg-top-260">
          {{-- {{?'hideme':'text-bold'}} {{request()->routeIs('search')?'hideme':'text-bold'}} --}}
          @if(!request()->routeIs('ticket') || request()->routeIs('search'))
          <div class="veil reveal-md-block" >
           
            <h1 class="">{{ __(@$pageTitle) }}</h1>
            
          </div>
          <ul class="list-inline list-inline-icon list-inline-icon-type-1 list-inline-icon-extra-small list-inline-icon-white p offset-top-30 offset-md-top-40 offset-lg-top-125">
            <li class="#"><a href="{{route('home')}}" class="text-white">Home</a></li>
            <li class="#">{{ __(@$pageTitle) }}
            </li>
          </ul>
          @else 
          <div class="panel panel-lg bg-overlay-chathams-blue text-lg-left " >
            <h3><span class="small text-bold text-white" >Find Bus Tickets</span></h3>
            @include($activeTemplate.'partials.new.ticketsearchform')
            
        </div> 
          @endif
        </div>
      </div>
    </div>
</section>
