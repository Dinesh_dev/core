@php
$content = getContent('footer.content', true);
$socialLinks = getContent('social_links.element',false,null,true);
$policies = getContent('policies.element',false,null,true);
@endphp

 <footer class="page-footer bg-chathams-blue">
        <section class="section-60">
          <div class="shell">
            <div class="range range-xs-center text-md-left">
              <div class="cell-xs-10 cell-sm-7 cell-md-4">
                <!-- Footer brand-->
                <div class="footer-brand"><a href="{{route('home')}}">
                    <img width='97' height='57' src='{{ getImage(imagePath()['logoIcon']['path'].'/logo_2.jpeg') }}' alt=''/>
                  </a>
                </div>
                <div class="offset-top-30 inset-sm-right-20">
                  <p class="text-silver">
                    UMT is a tech powered company in Nigeria that provide seamless mobility services to commuters accross the nation.

                  </p>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-7 cell-md-3 offset-top-60 offset-md-top-0">
                <div>
                  <h5 class="text-bold text-white">Contacts</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline bg-ripe-lemon"></div>
                </div>
                <div class="offset-top-20">
                  <!-- Contact Info-->
                  <address class="contact-info text-left">
                    <div>
                      <div class="reveal-inline-block">
                        <a href="callto:#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-phone"></span></span><span class="unit-body"><span class="text-silver">+234 803 771 7320</span></span></a></div>
                    </div>
                    <div class="offset-top-10">
                      <div class="reveal-inline-block">
                        <a href="mailto:#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-email-outline"></span></span><span class="unit-body"><span class="text-silver">uniquemasstransport@gmail.com</span></span></a></div>
                    </div>
                    <div class="offset-top-10">
                      <div class="reveal-inline-block">
                        <a href="#" class="unit unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-map-marker"></span></span><span class="unit-body"><span class="text-silver">17 Balogun Street Anifowoshe Ikeja Lagos.</span></span></a></div>
                    </div>
                  </address>
                </div>
                <div class="offset-top-20">
                  <!-- List Inline-->
                  <ul class="list-inline list-inline-2">
                    <li><a href="#" class="icon icon-xxs icon-gray-darker-filled icon-circle fa fa-facebook"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-gray-darker-filled icon-circle fa fa-twitter"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-gray-darker-filled icon-circle fa fa-google-plus"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-gray-darker-filled icon-circle fa fa-instagram"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-gray-darker-filled icon-circle fa fa-rss"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-7 cell-md-2 offset-top-60 offset-md-top-0">
                <div>
                  <h5 class="text-bold text-white">Useful Links</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline bg-ripe-lemon"></div>
                </div>
               
                <div class="offset-top-20">
                  <ul class="footer-link-sec">
                    @foreach($pages as $k => $data)
                      <li class="offset-top-10">
                          <a class="text-silver" href="{{route('pages',[$data->slug])}}">{{__($data->name)}}</a>
                      </li>
                      @endforeach
                </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-7 cell-md-2 offset-top-60 offset-md-top-0">
                <div>
                  <h5 class="text-bold text-white">Policies</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline bg-ripe-lemon"></div>
                </div>
               
                <div class="offset-top-20">
                  <ul class="footer-link-sec">
                    @foreach ($policies as $policy)
                    <li class="offset-top-10">
                        <a class="text-silver " href="{{ route('policy.details', [$policy->id, slug($policy->data_values->title)]) }}">@php
                            echo $policy->data_values->title
                            @endphp</a>
                    </li>
                    @endforeach
              </ul>
                </div>

              </div>
            </div>
          </div>
        </section>
        <section class="section-12 text-md-left">
          <div class="shell">
            <p class="font-accent"><span class="small text-silver-dark">&copy; <span id="copyright-year"></span> All Rights Reserved</span></p>
          </div>
        </section>
      </footer>