@php
$content = getContent('footer.content', true);
$socialLinks = getContent('social_links.element',false,null,true);
$policies = getContent('policies.element',false,null,true);
@endphp
<footer class="page-footer">
        <section class="section-60">
          <div class="shell">
            <div class="range range-xs-center text-md-left">
              <div class="cell-xs-10 cell-sm-7 cell-md-4">
                <!-- Footer brand-->
                <div class="footer-brand"><a href="{{route('home')}}">
                  <img width='97' height='57' src="{{ getImage(imagePath()['logoIcon']['path'].'/logo_2.jpeg') }}" alt=''/></a>
                </div>
                <div class="offset-top-30 inset-sm-right-20">
                  <p class="text-gray">
                     UMT is a tech powered company in Nigeria that provide seamless mobility services to commuters accross the nation.

                  </p>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-7 cell-md-3 offset-top-60 offset-md-top-0">
                <div>
                  <h5 class="text-bold">Contacts</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline"></div>
                </div>
                <div class="offset-top-20">
                  <!-- Contact Info-->
                  <address class="contact-info text-left">
                    <div>
                      <div class="reveal-inline-block"><a href="callto:#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle fa fa-phone"></span></span><span class="unit-body"><span class="text-gray-darker">+234 803 771 7320</span></span></a></div>
                    </div>
                    <div class="offset-top-10">
                      <div class="reveal-inline-block"><a href="mailto:#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle fa fa-envelope"></span></span><span class="unit-body"><span class="text-gray-darker">uniquemasstransport@gmail.com</span></span></a></div>
                    </div>
                    <div class="offset-top-10">
                      <div class="reveal-inline-block"><a href="#" class="unit unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle fa  fa-map-marker "></span></span><span class="unit-body"><span class="text-gray-darker">17 Balogun Street Anifowoshe Ikeja Lagos.</span></span></a></div>
                    </div>
                  </address>
                </div>
                <div class="offset-top-20">
                  <!-- List Inline-->
                  <ul class="list-inline list-inline-2">
                    <li><a href="https://www.facebook.com/profile.php?id=100087385396788" class="icon icon-xxs icon-silver-filled icon-circle fa fa-facebook"></a></li>
                    <li><a href="https://twitter.com/uniquemas1" class="icon icon-xxs icon-silver-filled icon-circle fa fa-twitter"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-google-plus"></a></li>
                    <li><a href="https://www.instagram.com/uniquemasstransport/?igshid=YmMyMTA2M2Y=" class="icon icon-xxs icon-silver-filled icon-circle fa fa-instagram"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-rss"></a></li>
                  </ul>
                </div>
              </div>
              <div class="cell-xs-10 cell-sm-7 cell-md-2 offset-top-60 offset-md-top-0">
                <div>
                  <h5 class="text-bold">Useful Links</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline"></div>
                </div>
                <div class="offset-top-20">
                  <ul class="footer-link-sec">
                      @foreach($pages as $k => $data)
                        <li>
                            <a href="{{route('pages',[$data->slug])}}">{{__($data->name)}}</a>
                        </li>
                        @endforeach
                  </ul>

                </div>
                <!-- <div class="offset-top-20">
                        <form data-form-output="form-subscribe-footer" data-form-type="subscribe" method="post" action="bat/rd-mailform.php" class="rd-mailform rd-mailform-subscribe">
                          <div class="form-group form-group-sm">
                            <div class="input-group">
                              <input placeholder="Your e-mail..." type="email" name="email" data-constraints="@Email @Required" class="form-control"><span class="input-group-btn">
                                <button type="submit" class="btn btn-xs btn-ripe-lemon">Subscribe</button></span>
                            </div>
                          </div>
                          <div id="form-subscribe-footer" class="form-output"></div>
                        </form>
                </div> -->
              </div>
              <div class="cell-xs-10 cell-sm-7 cell-md-2 offset-top-60 offset-md-top-0">
                <div>
                  <h5 class="text-bold">Policies</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline"></div>
                </div>
                <div class="offset-top-20">
                  <ul class="footer-link-sec">
                        @foreach ($policies as $policy)
                        <li>
                            <a href="{{ route('policy.details', [$policy->id, slug($policy->data_values->title)]) }}">@php
                                echo $policy->data_values->title
                                @endphp</a>
                        </li>
                        @endforeach
                  </ul>
                </div>
                  
              </div>
            </div>
          </div>
        </section>
        <section class="section-12 bg-gray-darker text-md-left">
          <div class="shell">
            <p class="font-accent"><span class="small text-silver-dark">&copy; <span id="copyright-year"></span> All Rights Reserved </span>
            </p>
          </div>
        </section>
      </footer>