@php
$content = getContent('contact.content', true);
@endphp
@php
    $bedcontent = getContent('breadcrumb.content', true);
@endphp
 <!-- RD Navbar Transparent-->
 <div class="rd-navbar-wrap">
    <nav data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-static" data-md-stick-up-offset="90px" data-lg-stick-up-offset="75px" data-auto-height="false" class="rd-navbar rd-navbar-top-panel rd-navbar-default rd-navbar-white rd-navbar-static-fullwidth-transparent" data-lg-auto-height="true" data-md-layout="rd-navbar-fullwidth" data-lg-layout="rd-navbar-static" data-lg-stick-up="true">
    <div class="rd-navbar-inner">
        <!-- RD Navbar Top Panel-->
        <div class="rd-navbar-top-panel">
        <div class="left-side">
            <!-- Contact Info-->
            <!-- <address class="contact-info text-left"> -->
            <div class="reveal-inline-block" style="padding-right:50px"><a href="callto:#"  class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left" ><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle " style="text-align:center;border-radius:50%"><i class="fa fa-phone"></i></span></span><span class="unit-body" style="padding-left:3px !important;color:#000000c9 !important !important"><span class="text-gray-darker">+234 803 771 7320</span></span></a></div>
            <div class="reveal-inline-block" style="padding-right:15px"><a href="mailto:#"  class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left" ><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle  mdi mdi-email-outline" style="text-align:center;border-radius:50%"><i class="fa fa-envelope"></i></span></span><span class="unit-body" style="padding-left:3px !important;color:#000000c9 !important !important"><span class="text-gray-darker">uniquemasstransport@gmail.com</span></span></a></div>
            <div class="reveal-inline-block">
                    <ul class="list-inline list-inline-2" style="display:flex;gap:4px;margin-left:10px">
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-facebook icon-hover"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-twitter icon-hover"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-google-plus icon-hover"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-instagram icon-hover"></a></li>
                    <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-rss icon-hover"></a></li>
                  </ul>
                    </div>
            <!-- <div class="reveal-inline-block"><a href="#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-map-marker"></span></span><span class="unit-body"><span class="text-gray-darker">2130 Fulton Street San Diego, CA 94117-1080 USA</span></span></a></div> -->
            <!-- </address> -->
        </div>
        <div class="right-side">
            <!-- <div class="goaccount">
            <a href="#"><i class="fas fa-sign-in-alt"></i> <span>Sign In</span> </a>
            <span>/</span>
            <a href="#"><i class="fas fa-user-plus"></i> <span>Sign Up</span></a>
            </div> -->
            @guest
            <ul class="list-inline list-inline-2 list-inline-3" style="margin-top:0.3px;display:flex;justify-content:space-around;width:200px;border:1px solid #ddd;padding:5px;">
            <li><a href="{{ route('user.login') }}" class="" style="font-size:14px;color:#3256a4"><i class="fa fa-sign-in"></i> Sign In</a></li>
            <li><a href="#" style="color:#3256a4">/</a></li>
            <li><a href="{{ route('user.register') }}" class="" style="font-size:14px;"><i class="fa fa-user" style="color:#3256a4"></i> Sign up</a></li>
            
            </ul>
            @endguest
            @auth
            <ul class="list-inline list-inline-2 list-inline-3" style="margin-top:2px;display:flex;justify-content:space-around;width:200px;border:1px solid #ddd;">
            <li><a href="{{ route('user.home') }}" class="" style="font-size:14px;color:#3256a4"><i class="fa fa-sign-in"></i> Dashboard</a></li>
            
            </ul>
            @endauth
        </div>
        </div>
        <!-- RD Navbar Panel-->
        <div class="rd-navbar-panel">
        <!-- RD Navbar Toggle-->
        <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
        <!-- RD Navbar Top Panel Toggle-->
        <button data-rd-navbar-toggle=".rd-navbar, .rd-navbar-top-panel" class="rd-navbar-top-panel-toggle"><span></span></button>
        <!-- Navbar Brand-->
        <div class="rd-navbar-brand"><a href="{{route('home')}}">
            <img width='97' height='57' src="{{ getImage(imagePath()['logoIcon']['path'].'/logo_2.jpeg') }}" alt="@lang('Logo')"/></a></div>
        </div>
        <div class="rd-navbar-menu-wrap">
        <div class="rd-navbar-nav-wrap">
            <div class="rd-navbar-mobile-scroll">
            <!-- Navbar Brand Mobile-->
            <div class="rd-navbar-mobile-brand"><a href="{{route('home')}}">
                <img width='97' height='57' src="{{ getImage(imagePath()['logoIcon']['path'].'/logo_2.jpeg') }}" alt=''/></a></div>
            <div class="form-search-wrap">
                <!-- RD Search Form-->
                <form action="search-results.html" method="GET" class="form-search rd-search">
                <div class="form-group">
                    <label for="rd-navbar-form-search-widget" class="form-label form-search-label form-label-sm rd-input-label">Search</label>
                    <input id="rd-navbar-form-search-widget" type="text" name="s" autocomplete="off" class="form-search-input input-sm form-control form-control-gray-lightest input-sm"/>
                </div>
                <button type="submit" class="form-search-submit"><span class="fa fa-search"></span></button>
                </form>
            </div>
            <!-- RD Navbar Nav-->
            <ul class="rd-navbar-nav">
                <li><a href="{{route('home')}}" class="{{request()->routeIs('home')?'activetop':''}}">Home</a></li>
                @foreach($pages as $k => $data)
                <li>
                    <a href="{{route('pages',[$data->slug])}}" class="{{request()->url() == route('pages',[$data->slug])?'activetop':''}}" >{{__($data->name)}}</a>
                </li>
                @endforeach
                
                <li>
                    <a href="{{ route('blog') }}" class="{{request()->routeIs('blog')?'activetop':''}}" >@lang('Blog')</a>
                </li>
                <li>
                    <a href="{{ route('track') }}" class="{{request()->routeIs('track')?'activetop':''}}" >@lang('Logistics Tracker')</a>
                </li>
                <li>
                    <a href="{{ route('contact') }}" class="{{request()->routeIs('contact')?'activetop':''}}">@lang('Contact')</a>
                </li>
                @if(!request()->routeIs('ticket'))
                <li>
                <a href="{{ route('ticket') }}" class="cmn--btn btn--sm top-btn" style="margin-top:-5px;background:#0e6d9ee6;">@lang('Buy Tickets')</a>
                </li>
                @endif
                
                <!-- <li class="rd-navbar--has-dropdown rd-navbar-submenu"><a href="destinations.html">Destinations</a>
                
                <ul class="rd-navbar-dropdown">
                    <li><a href="destinations.html">All Destinations</a></li>
                    <li><a href="single-tour.html">Single Tour</a></li>
                    <li><a href="ticket-catalog.html">Ticket Catalog</a></li>
                    <li><a href="single-ticket.html">Single Ticket</a></li>
                </ul>
                </li> -->
                <!-- <li><a href="deals.html">Deals</a></li> -->
                <!-- <li class="rd-navbar--has-dropdown rd-navbar-submenu"><a href="blog-classic.html">News</a>
                
                <ul class="rd-navbar-dropdown">
                    <li><a href="blog-classic.html">Classic Blog</a></li>
                    <li><a href="blog-grid.html">Grid Blog</a></li>
                    <li><a href="blog-masonry.html">Masonry Blog</a></li>
                    <li><a href="blog-modern.html">Modern Blog</a></li>
                    <li><a href="single-post.html">Post Page</a></li>
                </ul>
                </li> -->
                <!-- <li><a href="tracking.html">Tracking</a></li> -->
                <!-- <li class="rd-navbar--has-megamenu rd-navbar-submenu"><a href="#">Pages</a>
                <div class="rd-navbar-megamenu">
                    <div class="row">
                    <div class="col-md-3">
                        <div class="veil reveal-md-block">
                        <h6 class="text-bold">Gallery</h6>
                        </div>
                        <ul class="offset-md-top-20">
                        <li><a href="grid-gallery.html">Grid Gallery</a></li>
                        <li><a href="grid-without-padding-gallery.html">Grid Without Padding Gallery</a></li>
                        <li><a href="masonry-gallery.html">Masonry Gallery</a></li>
                        <li><a href="cobbles-gallery.html">Cobbles Gallery</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="veil reveal-md-block">
                        <h6 class="text-bold">Elements</h6>
                        </div>
                        <ul class="offset-md-top-20">
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="tabs-accordions.html">Tabs &amp; Accordions</a></li>
                        <li><a href="progress-bars.html">Progress Bars</a></li>
                        <li><a href="tables.html">Tables</a></li>
                        <li><a href="forms.html">Forms</a></li>
                        <li><a href="buttons.html">Buttons</a></li>
                        <li><a href="icons.html">Icons</a></li>
                        <li><a href="grid.html">Grid</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="veil reveal-md-block">
                        <h6 class="text-bold">Pages</h6>
                        </div>
                        <ul class="offset-md-top-20">
                        <li><a href="team-member-profile.html">Team Member Profile</a></li>
                        <li><a href="services.html">Services</a></li>
                        <li><a href="timetable.html">Timetable</a></li>
                        <li><a href="careers.html">Careers</a></li>
                        <li><a href="sitemap.html">Sitemap</a></li>
                        <li><a href="404.html">404 Page</a></li>
                        <li><a href="503.html">503 Page</a></li>
                        <li><a href="under-construction.html">Under Construction</a></li>
                        <li><a href="login-register.html">Login/Register</a></li>
                        <li><a href="search-results.html">Search Results</a></li>
                        <li><a href="privacy.html">Terms of Use</a></li>
                        <li><a href="single-service.html">Single Service</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="veil reveal-md-block">
                        <h6 class="text-bold">Layouts</h6>
                        </div>
                        <ul class="offset-md-top-20">
                        <li><a href="index-variant-2.html">Home Variant 2</a></li>
                        <li><a href="index-variant-3.html">Home Variant 3</a></li>
                        <li><a href="transparent-header.html">Transparent Header</a></li>
                        <li><a href="corporate-header.html">Corporate Header</a></li>
                        <li><a href="minimal-header.html">Minimal Header</a></li>
                        <li><a href="hamburger-menu-header.html">Hamburger Menu Header</a></li>
                        <li><a href="footer-widget-light.html">Footer Widget Light</a></li>
                        <li><a href="footer-widget-dark.html">Footer Widget Dark</a></li>
                        <li><a href="footer-center-light.html">Footer Center Light</a></li>
                        <li><a href="footer-center-dark.html">Footer Center Dark</a></li>
                        <li><a href="footer-minimal-light.html">Footer Minimal Light</a></li>
                        <li><a href="footer-minimal-dark.html">Footer Minimal Dark</a></li>
                        </ul>
                    </div>
                    </div>
                </div>
                </li> -->
                <!-- <li><a href="contacts.html">Contacts</a></li> -->
            </ul>
            </div>
        </div>
        <!-- RD Navbar Search-->
        <!-- <div class="rd-navbar-search rd-navbar-search-top-panel"><a data-rd-navbar-toggle=".rd-navbar-inner,.rd-navbar-search" href="#" class="rd-navbar-search-toggle mdi"><span></span></a>
            <form action="search-results.html" data-search-live="rd-search-results-live" method="GET" class="rd-navbar-search-form search-form-icon-right rd-search">
            <div class="form-group">
                <label for="rd-navbar-search-form-input" class="form-label rd-input-label">Type and hit enter...</label>
                <input id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off" class="rd-navbar-search-form-input form-control form-control-gray-lightest"/>
            </div>
            <div id="rd-search-results-live" class="rd-search-results-live"></div>
            </form>
        </div> -->
        </div>
    </div>
    </nav>
</div>
<section class="section-height-800 breadcrumb-modern rd-parallax context-dark bg-gray-darkest text-lg-left">
<div style='background: url({{ getImage("assets/images/frontend/breadcrumb/".@$bedcontent->data_values->background_image, "1920x1288") }}) center;height:500px;background-size:cover;'>
<div class="container container-flex-col">
    <h1 class="text-bold" style="padding-top:19rem;color:white !important;text-align:left">{{ __(@$pageTitle) }}</h1>
    
   
    <ul class="list-inline list-inline-icon list-inline-icon-type-1 list-inline-icon-extra-small list-inline-icon-white p offset-top-30 offset-md-top-40 offset-lg-top-125 bedcrumlinks" style="display:flex;margin-left:10px">
        <li><a href="{{ route('home') }}" class="text-white">Home</a></li>
        @if(@$pageTitle == 'Blog Details')
        <li>
            <a href="{{route('blog')}}" class="text-white">
                Blog
            </a>    
        </li>
        @endif
        <li>
            <a href="#" class="text-white">
                {{ __(@$pageTitle) }}
            </a>    
        </li>
        
    </ul>
</div>

</div>

<!-- <div data-speed="0.2" data-type="media" data-url='{{ getImage("assets/images/frontend/breadcrumb/".@$content->data_values->background_image, "1920x1288") }}' class="rd-parallax-layer"></div>
<div data-speed="0" data-type="html" class="rd-parallax-layer">
<div class="bg-primary-chathams-blue-reverse">
    <div class="shell section-top-57 section-bottom-30 section-md-top-210 section-lg-top-260">
    <div class="veil reveal-md-block">
        <h1 class="text-bold" style="text-align:center">FAQ Page</h1>
    </div>
    <ul class="list-inline list-inline-icon list-inline-icon-type-1 list-inline-icon-extra-small list-inline-icon-white p offset-top-30 offset-md-top-40 offset-lg-top-125">
        <li><a href="index.html" class="text-white">Home</a></li>
        <li><a href="our-history.html" class="text-white">About</a></li>
        <li>FAQ Page
        </li>
    </ul>
    </div>
</div>
</div> -->
</section>
<!-- Header Section Starts Here -->
<!-- <div class="header">
<div class="header-top">
    <div class="container">
        <div class="header-top-area">
            <ul class="left-content">
                <li>
                    <i class="las la-phone"></i>
                    <a href="tel:{{ __(@$content->data_values->contact_number) }}" style="color:black">
                        {{--{{ __(@$content->data_values->contact_number) }}--}}
						+234 803 771 7320
                    </a>
                </li>
                <li>
                    <i class="las la-envelope-open"></i>
                    <a href="mailto:{{ __(@$content->data_values->email) }}" style="color:black">
                        {{--{{ __(@$content->data_values->email) }}--}}
						uniquemasstransport@gmail.com
                    </a>
                </li>
            </ul>
            <div class="right-content d-flex flex-wrap" style="gap:10px">
				{{--
                <div>
                    <select class="langSel form--control">
                        @foreach($language as $item)
                        <option value="{{$item->code}}" @if(session('lang')==$item->code) selected @endif>{{ __($item->name) }}</option>
                        @endforeach
                    </select>
                </div>
				--}}
                @guest
                <ul class="header-login">
                    <li><a class="sign-in" href="{{ route('user.login') }}"><i class="fas fa-sign-in-alt"></i>@lang('Sign In')</a></li>
                    <li>/</li>
                    <li><a class="sign-up" href="{{ route('user.register') }}"><i class="fas fa-user-plus"></i>@lang('Sign Up')</a></li>
                </ul>
                @endguest
                @auth
                <ul class="header-login">
                    <li>
                        <a href="{{ route('user.home') }}">@lang('Dashboard')</a>
                    </li>
                </ul>
                @endauth
            </div>
        </div>
    </div>
</div>
<div class="header-bottom" id="header-bottom">
    <div class="container">
        <div class="header-bottom-area">
            <div class="logo">
                <a href="{{ route('home') }}">
                    <img src="{{ getImage(imagePath()['logoIcon']['path'].'/logo_2.jpeg') }}" alt="@lang('Logo')">
                </a>
            </div> 
            <div class="header-nav-menu-area">
            <ul class="menu">
                <li>
                    <a href="{{ route('home') }}">@lang('Home')</a>
                </li>
                @foreach($pages as $k => $data)
                <li>
                    <a href="{{route('pages',[$data->slug])}}">{{__($data->name)}}</a>
                </li>
                @endforeach
                <li>
                    <a href="{{ route('blog') }}">@lang('Blog')</a>
                </li>
                <li>
                    <a href="{{ route('track') }}">@lang('Logistics Tracker')</a>
                </li>
                <li>
                    <a href="{{ route('contact') }}">@lang('Contact')</a>
                </li>
            </ul>
            <div class="d-flex flex-wrap algin-items-center">
                <a href="{{ route('ticket') }}" class="cmn--btn btn--sm">@lang('Buy Tickets')</a>
                <div class="header-trigger-wrapper d-flex d-lg-none ms-4">
                    <div class="header-trigger d-block d-lg-none">
                        <span></span>
                    </div>
                    <div class="top-bar-trigger">
                        <i class="las la-ellipsis-v"></i>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
</div> -->


@push('script')
<script>
    $(document).ready(function() {
        "use strict";
        $(".langSel").on("change", function() {
            window.location.href = "{{route('home')}}/change/" + $(this).val();
        });
        var rdNavbar = $(".rd-navbar");
        if (rdNavbar.length) {
            rdNavbar.RDNavbar({
            stickUpClone: (rdNavbar.attr("data-stick-up-clone")) ? rdNavbar.attr("data-stick-up-clone") === 'true' : false,
            stickUpOffset: (rdNavbar.attr("data-stick-up-offset")) ? rdNavbar.attr("data-stick-up-offset") : 1,
            anchorNavOffset: -78
            });
            if (rdNavbar.attr("data-body-class")) {
            document.body.className += ' ' + rdNavbar.attr("data-body-class");
            }
        }
    });
</script>
@endpush