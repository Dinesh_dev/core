@php
$content = getContent('footer.content', true);
$socialLinks = getContent('social_links.element',false,null,true);
$policies = getContent('policies.element',false,null,true);
@endphp

<!-- Footer Section Starts Here -->
<section data-aos="fade-in" data-aos-duration="2000" class="footer-seciton " style="position:relative">
    <div class="footerbg"></div>
    <div class="footer-top footer-section-bgwhite">
        <div class="container">
            <div class="row footer-wrapper gy-sm-5 gy-4">
                <div class="col-md-4 col-sm-6">
                    <div class="footer-widget" >
                        <div class="logo">
                            <img src="{{ getImage(imagePath()['logoIcon']['path'].'/logo_2.jpeg') }}" alt="@lang('Logo')">
                        </div>
                        {{--<p>{{ __(@$content->data_values->short_description) }}</p>--}}
                        <p>
                        <!-- BusExpress is the leading go-to website for booking inter-city bus ticket online. Our booking system allows travelers to search and book bus tickets for over a hundred bus companies throughout the United States, Mexico, Canada and Europe. -->
                          UMT is a tech powered company in Nigeria that provide seamless mobility services to commuters accross the nation.
                        </p>
                       
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">@lang('Contacts')</h4>
                        @php
                        $contacts = getContent('contact.content', true);
                        @endphp
                        <div class="offset-top-6">
                            <div class="text-subline bg-ripe-lemon"></div>
                        </div>
                        <div class="offset-top-20">
                        <address class="contact-info text-left">
                    <div>
                      <div class="reveal-inline-block"><a href="callto:#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left unit-lef-flex"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle las la-phone-volume"></span></span><span class="unit-body"><span class="text-silver">+234 803 771 7320</span></span></a></div>
                    </div>
                    <div class="offset-top-10">
                      <div class="reveal-inline-block"><a href="mailto:#" class="unit unit-middle unit-horizontal unit-spacing-xs"><span class="unit-left unit-lef-flex"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle las la-envelope"></span></span><span class="unit-body"><span class="text-silver">uniquemasstransport@gmail.com</span></span></a></div>
                    </div>
                    <div class="offset-top-10">
                      <div class="reveal-inline-block"><a href="#" class="unit unit-horizontal unit-spacing-xs"><span class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle fas fa-map-marker"></span></span><span class="unit-body"><span class="text-silver">17 Balogun Street Anifowoshe Ikeja Lagos</span></span></a></div>
                    </div>
                  </address>
                            <!-- <ul class="footer-contacts">
                            
                            <li>
                                <i class="las la-phone-volume icon-primary-filled"></i> 
                                <a href="tel:+234 803 771 7320"> +234 803 771 7320</a>
                            </li>
                            <li>
                                <i class="las la-envelope icon-primary-filled"></i> 
                                <a href="mailto:{{ __($contacts->data_values->email) }}" style="margin-right:-10px"> uniquemasstransport@gmail.com</a>
                            </li>
                            
                            <li>
                                <i class="fas fa-map-marker icon-primary-filled" style="width:70px"></i> 
                                <a href="#">   Head Office - 17 Balogun Street Anifowoshe Ikeja Lagos.</a>
                            </li>
                        </ul> -->
                        </div>
                        
                         <ul class="social-icons">
                            @foreach ($socialLinks as $item)
                                <li>
                                    <a href="{{ $item->data_values->url }}">@php echo $item->data_values->icon @endphp</a>
                                </li>
                            @endforeach
                        </ul>
                     
                        
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="footer-widget">
                         <h4 class="widget-title">@lang('Useful Links')</h4>
                         <div class="offset-top-6">
                            <div class="text-subline bg-ripe-lemon"></div>
                        </div>
                        <div class="offset-top-20">
                                <ul class="footer-links">
                                    @foreach($pages as $k => $data)
                                    <li>
                                        <a href="{{route('pages',[$data->slug])}}">{{__($data->name)}}</a>
                                    </li>
                                    @endforeach
                                    {{--
                                    <li>
                                        <a href="{{ route('blog') }}">@lang('Blog')</a>
                                    </li>
                                    --}}
                                    <li>
                                        <a href="{{ route('contact') }}">@lang('Contact')</a>
                                    </li>
                                </ul>
                        </div>
                       
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">@lang('Policies')</h4>
                        <div class="offset-top-6">
                            <div class="text-subline bg-ripe-lemon"></div>
                        </div>
                        <div class="offset-top-20">
                        <ul class="footer-links">
                            @foreach ($policies as $policy)
                            <li>
                                <a href="{{ route('policy.details', [$policy->id, slug($policy->data_values->title)]) }}">@php
                                    echo $policy->data_values->title
                                    @endphp</a>
                            </li>
                            @endforeach

                        </ul>
                                    </div>       
                    </div>              
                </div>
                
            </div>
        </div>
    </div>
    <div class="footer-bottom bg-gray-darker">
        <div class="container">
        <p class="font-accent"><span class="small text-silver-dark">© <span id="copyright-year">2023</span> All Rights Reserved </span>
            </p>
        </div>
    </div>
</section>
<!-- Footer Section Ends Here -->

@php
$cookie = App\Models\Frontend::where('data_keys','cookie.data')->first();
@endphp

<!-- cookies default start -->
<!-- <div id="cookiePolicy" class="cookies-card bg--default radius--10px text-center">
    <div class="cookies-card__icon">
        <i class="fas fa-cookie-bite"></i>
    </div>
    <p class="mt-4 cookies-card__content">
        @php
        echo @$cookie->data_values->description
        @endphp
        <a href="{{ route('cookie.details') }}" target="_blank">@lang('learn more')</a>
    </p>
    <div class="cookies-card__btn mt-4">
        <a href="#" name="cookieAccept" class="cookies-btn">@lang('Allow')</a>
    </div>
</div> -->
<!-- cookies default end -->
@push('script')
<!-- <script>
    (function($) {
        "use strict";

        $('#cookiePolicy').hide();
        @if(@$cookie-> data_values-> status && !session('cookie_accepted'))
        $('#cookiePolicy').show();
        @endif

        $('a[name="cookieAccept"]').click(function(event) {
            event.preventDefault();
            var actionUrl = "{{ route('cookie.accept') }}";
            $.ajax({
                type: "GET",
                url: actionUrl,
                success: function(data) {
                    console.log(data);
                    $('#cookiePolicy').hide();
                    if (data.success) {
                        notify('success', data.success);
                        $('#cookiePolicy').hide();
                    }
                }
            });
        });
        $('.search').on('change', function() {
            $('#filterForm').submit();
        });
    })(jQuery);
</script> -->
@endpush
