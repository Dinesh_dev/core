<!doctype html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> {{ $general->sitename(__($pageTitle)) }}</title>
    @include('partials.seo')
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,400italic,700%7CLato:400">
   
    <link rel="stylesheet" href="{{ asset($activeTemplateTrue.'css/newcss/style.css')}}">
    <style>
       #date_of_journey::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: black;
  opacity: 0.8; /* Firefox */
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: black;
}

::-ms-input-placeholder { /* Microsoft Edge */
  color: black;
}
        .border-primary {
  border-color: #0d6efd !important;
}
        .replyname{
            display:flex;
            text-align: right;
            align-items: center;
        }
        .d-flex{
            display:flex;
        }
        .flex-wrap{
            flex-wrap: wrap;
        }
        .justify-content-between{
            justify-content: space-between;
        }
        .align-items-center{
            align-items: center;
        }
        .badge {
  display: inline-block;
  padding: .35em .65em;
  font-size: .75em;
  font-weight: 700;
  line-height: 1;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  border-radius: .25rem;
}
        .modal-header-flex{
            display:flex;
            justify-content: space-between;
        }
        .modal-title-full{
            width:100%;
            text-align: left;
        }
        .btn--danger {
  color: #fff;
  background: #dc3545 !important;
  border-color: #dc3545 !important;
}
        .dashboard-widget {
  background: #fff;
  border-radius: 10px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  padding: 25px;
  border-left: 4px solid #0e9e4d;
    border-left-color: rgb(14, 158, 77);
    box-shadow: 1px 1px #88888840;
}
div[class*="col"]:nth-child(3n+1) .dashboard-widget {
  border-color: #43c525;
}
div[class*="col"]:nth-child(3n + 1) .dashboard-widget .dashboard-widget__icon {
    background-color: #43c525;
}

div[class*="col"]:nth-child(3n + 2) .dashboard-widget {
    border-color: #dc3545;
}
div[class*="col"]:nth-child(3n + 2) .dashboard-widget .dashboard-widget__icon {
    background-color: #dc3545;
}

div[class*="col"]:nth-child(3n + 3) .dashboard-widget {
    border-color: #ffc107;
}
div[class*="col"]:nth-child(3n + 3) .dashboard-widget .dashboard-widget__icon {
    background-color: #ffc107;
}
.dashboard-widget__icon {
	font-size: 32px;
	background-color: #0e9e4d;
	border-radius: 7px;
	color: #fff;
	width: 60px;
	height: 60px;
	display: flex;
	align-items: center;
	justify-content: center;
}
        .offset-md-bottom-60{
            margin-bottom: 60px !important;
        }
        .cmn--card {
    box-shadow: 0 0 15px rgba(31, 31, 35, 0.18);
    border: 0;
}
.cmn--card .card-header {
    background: #2b4d98;
    border: 0;
    padding: 15px 25px;
    border-radius: 5px 5px 0 0;
    
}
.cmn--card .card-footer {
    border: 0;
    padding: 15px 25px;
    background: #fff;
    border-radius: 0 0 5px 5px;
}
        .seat-plan-wrapper {
    width: 100%;
    margin-top: 40px;
    display: none;
    position: relative;
    z-index: 1;
    background: #fff;
}

.seat-plan-wrapper.selected {
    display: block;
}
.seat-wrapper-first div:nth-of-type(3){
    margin-left: 10px !important;
}
.seat-plan-inner {
    margin-top: 25px;
}

.seat-plan-inner .single {
    position: relative;
   
    min-height: 150px;
    max-width: 100%;
    /* padding: 80px 25px 30px; */
    padding: 30px 0px 30px 14px;
    margin-bottom: 55px;
    border-color: rgb(119, 119, 119) rgb(206, 202, 202) rgb(200, 190, 190);
    border-style: solid;
    border-width: 8px;
}

@media (max-width: 1199px) {
    .seat-plan-inner .single {
        padding-top: 60px;
    }
}

.seat-plan-inner .front {
    position: absolute;
    width: 60px;
    height: 25px;
    top: -13px;
    left: 50%;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    text-transform: uppercase;
    font-size: 10px;
    font-weight: 600;
    z-index: 1;
    color: #9b9b9b;
    background: #f1f1f1;
    letter-spacing: 1px;
}

.seat-plan-inner .rear {
    position: absolute;
    width: 60px;
    height: 25px;
    left: 50%;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
    bottom: -13px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    text-transform: uppercase;
    font-size: 10px;
    font-weight: 600;
    z-index: 23;
    color: #9b9b9b;
    background: #f1f1f1;
    letter-spacing: 1px;
}

.seat-plan-inner .lower {
    width: 50px;
    height: 40px;
    position: absolute;
    /*left: 20px;*/
    top: 20px;
    color: #777;
    font-weight: 600;
    text-transform: uppercase;
}

.seat-plan-inner .driver {
    position: absolute;
    right: 20px;
    top: 15px;
}

.seat-wrapper {
    margin: 0;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    margin-bottom: 15px;
}
.seatlegend{
    z-index: 1000;
    font-weight: 600;
    margin-top: -13px !important;
}
.seat-wrapper .seat svg{
    position: absolute;
    fill: none;
    stroke: black;
    stroke-width:6px;
   
}
.seat-wrapper .seat.selected span{
    color:white !important;
}
.seat-wrapper .seat.selected svg {
   
    fill:rgb(57, 163, 57);
}

.seat-wrapper .reserve-for-ladies .seat {
    color: #777 !important;
}

.seat-wrapper .left-side,
.seat-wrapper .right-side {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
}

.seat-wrapper .seat {
    height: 40px;
    width: 40px;
    color: black;
    /* border: 1px solid #979797; */
    border-top-left-radius: 2px;
    border-bottom-left-radius: 2px;
    margin-right: 10px;
    cursor: pointer;
    position: relative;
    font-weight: 100;
    font-size: 12px;
    display:flex;
    justify-content: center;
    align-items: center;
    background-size: auto; 
    background-repeat: no-repeat;
    text-align: center;
}

.btn-link:hover{
    color: var(--main-color) !important;
}

@media (max-width: 575px) {
    .seat-wrapper .seat {
        height: 45px;
        width: 45px;
        font-size: 14px;
    }
}

/* .seat-wrapper .seat span {
    position: absolute;
    left: 2px;
    right: 2px;
    height: 4px;
    border: 1px solid rgba(27, 39, 61, 0.25);
    border-radius: 2px;
    bottom: 6px;
} */





.seat-for-reserved {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    margin: 0;
    margin-top: 15px;
}

.seat-for-reserved .seat-condition {
    padding: 0;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    width: 50%;
    padding-bottom: 20px;
}

@media (max-width: 575px) {
    .seat-for-reserved .seat-condition {
        width: 100%;
    }
}

.seat-for-reserved .seat {
    width: 50px;
    height: 18px;
    border: 1px solid #979797;
    border-top-left-radius: 2px;
    border-bottom-left-radius: 2px;
    cursor: pointer;
    position: relative;
    margin-right: 10px;
}

.seat-for-reserved .seat span {
    position: absolute;
    top: 2px;
    bottom: 2px;
    width: 4px;
    border: 1px solid rgba(27, 39, 61, 0.25);
    right: 7px;
    border-radius: 2px;
}

.seat-for-reserved p {
    display: inline-block;
    font-size: 14px;
    line-height: 1.25;
}

.seat-for-reserved .selected-by-you .seat {
    border-color: rgb(57, 163, 57);
    background: rgb(57, 163, 57);
}

.seat-for-reserved .selected-by-you .seat span {
    background: #fff;
    border-color: #fff;
}

.seat-for-reserved .selected-by-ladies .seat {
    border-color: #f763c6;
    background: #f763c6;
    color: #fff !important;
}
.seat-for-reserved .selected-by-gents .seat {
    border-color: #554bb9;
    background: #554bb9;
    color: #fff !important;
}

.seat-for-reserved .selected-by-ladies .seat span {
    background: #fff;
    border-color: #fff;
}
.seat-for-reserved .selected-by-others .seat {
    border-color: #707d88;
    background: #707d88;
    color: #fff !important;
}

.seat-for-reserved .selected-by-others .seat span {
    background: #fff;
    border-color: #fff;
}
.seat-for-reserved .selected-by-gents .seat span {
    background: #fff;
    border-color: #fff;
}

.seat-for-reserved .reserve-for-ladies .seat {
    border-color: #f763c6;
    background: #fff;
}

.seat-for-reserved .reserve-for-ladies .seat span {
    background: #f763c6;
    border-color: #f763c6;
    color: #fff;
}

.selected-by-you .seat {
    border-color: var(--main-color);
    background: var(--main-color);
}

.selected-by-you .seat span {
    background: #fff;
    border-color: #fff;
}

.selected-by-ladies .seat span{
    color: #fff;
   
}

.selected-by-ladies .seat svg {
    fill: #f763c6 !important;
   
}
.selected-by-others .seat span{
    color: #fff;
}

.selected-by-others .seat svg {
   
    fill: #707d88;
}
.selected-by-gents .seat span {
    color: #fff !important;
    /* border-color: #554bb9 !important;
    background: #554bb9 !important; */
}
.selected-by-gents .seat svg {
    fill:#554bb9;
}



.reserve-for-ladies .seat {
    border-color: #f763c6 !important;
    background: #fff !important;
}

.reserve-for-ladies .seat span {
    background: #f763c6 !important;
    border-color: #f763c6 !important;
}

.seat-overview-wrapper {
    border: 1px solid rgba(27, 39, 61, 0.1);
    border-radius: 5px;
    padding: 20px 15px;
    position: sticky;
    top: 100px;
}

@media (min-width: 1200px) {
    .seat-overview-wrapper {
        padding: 30px;
    }
}

.seat-overview-wrapper .boarding-point {
    margin-bottom: 15px;
    height: 40px;
}

.seat-overview-wrapper .destination-point {
    margin-top: 15px;
    height: 40px;
}

.seat-overview-wrapper .seat-info {
    margin-top: 15px;
}

.seat-overview-wrapper .seat-info li {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
}

.seat-overview-wrapper .seat-info li .title {
    display: inline-block;
}

.seat-overview-wrapper .seat-info li .value {
    display: inline-block;
}

.seat-overview-wrapper .book-bus-btn {
    height: 40px;
    margin-top: 25px;
}



        .list-group-item:first-child {
  border-top-left-radius: inherit;
  border-top-right-radius: inherit;
}
.bg--base {
  background: #3256a4 !important;
}
.text-white {
  color: #fff !important;
}
.justify-content-between {
  justify-content: space-between !important;
}
.d-flex {
  display: flex !important;
}
     
        .d-none {
  display: none !important;
}
        .list-group-item {
  position: relative;
  display: block;
  padding: .5rem 1rem;
  color: #212529;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid rgba(0,0,0,.125);
}
        .radio-group{
            display:flex;
            gap: 6px;
        }
        .filter-header .reset-button {
	width: auto;
	background: none;
	color: #777;
	padding: 3px 5px;
	font-weight: 500;
	font-size: 14px;
}
        .book-ticket-panel{
            
            position: absolute;
            z-index: 100;
            top:280px;
            width: calc(100% - 9.8%);
        }
        .hideme{
            visibility: hidden;
        }
        @media (min-width: 1200px) {
    .padding-top-sec {
        padding-top: 80px;
    }
    .padding-bottom-sec {
        padding-bottom: 80px;
    }
}

.row {
    --bs-gutter-x: 1.5rem;
    --bs-gutter-y: 0;
    display: flex;
    flex-wrap: wrap;
    margin-top: calc(var(--bs-gutter-y) * -1);
    margin-right: calc(var(--bs-gutter-x)/ -2);
    margin-left: calc(var(--bs-gutter-x)/ -2);
}
.justify-content-center {
    justify-content: center!important;
}
.g-4, .gy-4 {
    --bs-gutter-y: 1.5rem;
}
.g-4, .gx-4 {
    --bs-gutter-x: 1.5rem;
}

.post-item:hover .post-thumb img {
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
}

.post-item:hover .post-meta {
    /* border-top-color: rgba(14, 158, 77, 0.3); */
}
.post-thumb {
    overflow: hidden;
}

.post-thumb img {
    width: 100%;
}

.post-content {
    padding: 15px 20px 5px 20px;
    
    -webkit-box-shadow: 0 0 10px 1px rgba(27, 39, 61, 0.1);
    box-shadow: 0 0 10px 1px rgba(27, 39, 61, 0.1);
    border-radius: 0 0 5px 5px;
    /* Changes */
    position:absolute !important;
    top:0 !important;
}

.post-content .date {
    font-size: 14px;
}

.post-content .date i {
    font-size: 16px;
    color: var(--main-color);
    margin-right: 4px;
}

.post-content .title {
    margin-bottom: 10px;
}

.post-content .title a {
    display: -webkit-box;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 1;
}

.post-content p {
    display: -webkit-box;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 2;
}

.post-content .post-meta {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    
    margin-bottom:5px;
    -webkit-transition: all ease 0.3s;
    transition: all ease 0.3s;
}

.post-content .post-meta li {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}

.post-content .post-meta li div {
    margin-right: 25px;
}

.post-content .post-meta li i {
    margin-right: 5px;
    /* color: var(--main-color); */
    color:white;
}

.post-details-content {
    padding-top: 25px;
}

.post-details-content .meta-post li {
    margin-right: 25px;
}

.post-details-content .meta-post li i {
    margin-right: 5px;
    color: var(--main-color);
}

.post-details-content .title {
    margin-bottom: 25px;
}

.post-details-content .blog-details-pera {
    margin-bottom: 20px;
}

.post-details-content .info li {
    padding-left: 20px;
    position: relative;
}

.post-details-content .info li::before {
    position: absolute;
    content: "";
    width: 100%;
    height: 100%;
    content: "\f14a";
    font-weight: 900;
    font-family: "Line Awesome Free";
    left: 0;
    top: 6px;
    color: #1b273d;
    margin-right: 5px;
}

.post-details-content .quote-wrapper {
    margin-top: 20px;
    margin-bottom: 20px;
    padding: 25px 20px;
    background: rgba(27, 39, 61, 0.03);
}

.post-details-content .quote-wrapper p {
    border-left: 2px solid var(--main-color);
    padding-left: 10px;
}

.post-details-content .quote-wrapper p i {
    font-size: 46px;
    color: var(--main-color);
}

.post-details-content .content-inner {
    border-bottom: 1px solid rgba(27, 39, 61, 0.1);
}

.post-details-content .content-inner .meta-post {
    margin: 5px 0;
    margin-bottom: 10px;
}

.post-details-content .content-inner .meta-post li {
    padding: 2px 0;
}

.post-details-content .meta-content {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    padding: 15px 0;
}

.post-details-content .meta-content .title {
    margin: 0;
    margin-right: 15px;
}

.post-details-content .meta-content li {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}

.post-details-content .meta-content li .la-heart {
    color: #f53b57;
}

.post-details-content .meta-content li i {
    margin-right: 7px;
}

.post-details-content .social-icons {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-top: 0;
}

.post-details-content .social-icons li a {
    background: none;
    margin: 5px;
    border: none;
}

.post-details-content .social-icons li a:hover i {
    color: initial !important;
}

.post-details-content .social-icons li i {
    margin: 0 !important;
}

.post-author-area {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    margin: 40px 0;
    padding: 30px 20px;
    background: rgba(27, 39, 61, 0.03);
}

.post-author-area .author-thumb {
    width: 100px;
    height: 100px;
    border-radius: 50%;
    overflow: hidden;
}

.post-author-area .author-thumb img {
    height: 100%;
    width: 100%;
    -o-object-fit: cover;
    object-fit: cover;
}

.post-author-area .author-content {
    width: calc(100% - 100px);
    padding-left: 25px;
}

.post-author-area .author-content .name {
    margin-bottom: 10px;
}

.post-comment-area .post-comment-title {
    margin-bottom: 25px;
}

.section-header {
    margin-bottom: 40px !important;
}
.text-center {
    text-align: center!important;
}

.section-header .title {
    margin-bottom: 15px;
}
.title-underline {
    position: relative;
}

.title-underline::before{
    position: absolute;
    content: "";
    width: 100%;
    height: 100%;
    width: 75px;
    height: 4px;
    background: #18377a;
    bottom: -9px;
    left: 41%;
    display: flex;
    justify-content: center;
}

.title-underline-bg::before {
    position: absolute;
    content: "";
    width: 100%;
    height: 100%;
    width: 75px;
    height: 4px;
    background: #18377a;
    bottom: -9px;
    left: 45%;
    display: flex;
    justify-content: center;
}

.post-content-two {
    position: absolute;
    bottom: 0;
    color: white !important;
    /* changes  */
    /* background: #161b2ea3; */
    background:#161b2e63;
}

.post-content {
    padding: 15px 20px 5px 20px;
    -webkit-box-shadow: 0 0 10px 1px rgb(27 39 61 / 10%);
    box-shadow: 0 0 10px 1px rgb(27 39 61 / 10%);
    border-radius: 0 0 5px 5px;
}
.post-content .title {
    margin-bottom: 10px;
}

.post-content .post-meta {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
   
    margin-bottom: 5px;
    -webkit-transition: all ease 0.3s;
    transition: all ease 0.3s;
}

.cmn--btn {
    text-transform: uppercase;
    font-size: 14px;
    padding: 12px 25px;
    border-color: rgba(14, 158, 77, 0.6);
    background: var(--main-color);
    color: #fff;
    position: relative;
    border-radius: 5px;
    overflow: hidden;
    font-weight: 600;
}

.cmn--btn:hover {
    color: #fff;
}

@media (max-width: 575px) {
    .cmn--btn {
        padding: 10px 15px;
        font-size: 13px;
    }
}

.cmn--btn.btn--sm,
.btn.btn--sm {
    padding: 6px 15px !important;
    height: auto;
}
.cmn--btn {
    text-transform: uppercase;
    font-size: 14px;
    padding: 12px 25px;
    border-color: rgba(14, 158, 77, 0.6);
    background: var(--main-color);
    color: #fff;
    position: relative;
    border-radius: 5px;
    overflow: hidden;
    font-weight: 600;
}
.bt-blue{
    background:#22438a;
}

.btn-default-new {
    color: #18377a;
    background-color: transparent;
    border-color: rgba(24, 55, 122, 0.3);
}
.btn-new {
    position: relative;
    padding: 9px 35px;
    font-size: 18px;
    line-height: 28px;
    border-radius: 0;
    font-weight: 700;
    text-transform: capitalize;
    white-space: nowrap;
    letter-spacing: 0.02em;
    border-width: 2px;
    transition: .3s;
    backface-visibility: hidden;
}

.btn-default-new:focus, .btn-default-new.focus, .btn-default-new:hover, .btn-default-new:active, .btn-default-new.active,
    .open > .btn-default-new.dropdown-toggle {
      color: #fff;
      background-color: #18377a;
      border-color: #18377a; }
    .btn-default-new:active, .btn-default-new.active,
    .open > .btn-default-new.dropdown-toggle {
      -webkit-box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.15);
      box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.15); }
      .btn-default-new:active:hover, .btn-default-new:active:focus, .btn-default-new:active.focus, .btn-default-new.active:hover, .btn-default-new.active:focus, .btn-default-new.active.focus,
      .open > .btn-default-new.dropdown-toggle:hover,
      .open > .btn-default-new.dropdown-toggle:focus,
      .open > .btn-default-new.dropdown-toggle.focus {
        color: #fff;
        background-color: #18377a;
        border-color: #18377a; }
    .btn-default-new.disabled, .btn-default-new[disabled],
    fieldset[disabled] .btn-default-new {
      pointer-events: none;
      opacity: .5; }
    .btn-default-new .badge {
      color: transparent;
      background-color: #18377a; }
      .title-underline{
        position:relative;
      }
      .goaccount{
        display:flex;
        padding:5px;
        border:1px solid #ddd;
      }
      .rd-navbar-static.rd-navbar-top-panel .right-side{
        margin-top:0px !important;
      }

      .top-btn:hover{
        color:white !important;
      }
      .footer-link-sec{
    padding:0px;
    
}
.footer-link-sec li{
    list-style:none;
}
.blogele{
    height:100%;
    width:100%;
    display:flex;
    flex-direction:column;
    justify-content:space-between;
}
.blogtag{
    background:#f0e319;
    color:black;
    padding:5px;
    
}
    </style>
    <style>
        .filter-header {
            display:flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    width: 100% !important;
}

.filter-header .reset-button {
    width: auto;
    background: none;
    color: #777;
    padding: 3px 5px;
    font-weight: 500;
    font-size: 14px;
}
        .ticket-filter {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    border:1px solid #ddd;
}

.filter-item {
    padding: 15px 15px 25px;
    background: #fff;
    border-bottom: 1px solid rgba(27, 39, 61, 0.1);
    width: 100%;
    text-align: left;
}
.filter-item:first-child {
    border-radius: 5px 5px 0 0;
}
.filter-item:last-child {
    border-bottom: 0;
    border-radius: 0 0 5px 5px;
}
.bus-type{
    padding:0px;
    margin:0px;
}
.bus-type li{
    list-style: none;
    
}

.ticket-item {
    display:flex;
    flex-wrap: wrap;
    padding: 25px;
    background: #fff;
    margin-bottom: 15px;
    border-radius: 5px;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    border:1px solid #ddd;
}

.ticket-item:last-child {
    margin-bottom: 0;
}

@media (max-width: 575px) {
    .ticket-item {
        padding: 20px 15px;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: start;
    }
}

.ticket-item-inner {
    -webkit-box-flex: 1;
    -ms-flex-positive: 1;
    flex-grow: 1;
    text-align: left;
    font-size: 12px;
}

@media (max-width: 767px) {
    .ticket-item-inner {
        width: 100%;
        text-align: left;
    }
}

.ticket-item-inner .bus-info {
    font-size: 13px;
}

.ticket-item-inner .ratting {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    color: #e9ba11;
}

.ticket-item-inner .ratting i {
    margin-right: 4px;
}

.ticket-item-inner .bus-time {
    text-align: center;
}

.ticket-item-inner .bus-time i {
    font-size: 24px;
    color: var(--main-color);
}

.ticket-item-inner .bus-time .time {
    font-size: 14px;
    color: #1f1f1f;
    font-weight: 600;
}

@media (max-width: 767px) {
    .ticket-item-inner .bus-time {
        text-align: left;
    }
}

.ticket-item-inner button {
    height: auto;
    width: auto;
    padding: 10px 15px;
    margin: 0;
    line-height: 1;
    font-weight: 600;
    word-spacing: 3px;
    text-transform: uppercase;
    font-size: 14px;
    margin-top: 10px;
}

.travel-time {
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    display:flex;
    width:220px;
}

@media (max-width: 767px) {
    .travel-time {
        text-align: left;
        margin-left: -15px;
        margin-right: -15px;
        -webkit-box-align: start;
        -ms-flex-align: start;
        align-items: start;
        -webkit-box-pack: start;
        -ms-flex-pack: start;
        justify-content: start;
        margin-top: 25px;
    }
}

.travel-time div {
    padding: 0 15px;
}

@media (max-width: 575px) {
    .travel-time div {
        padding: 0 10px;
    }
}

@media (max-width: 991px) {
    .filter-item {
        padding: 25px 25px 30px;
    }
}

@media (max-width: 575px) {
    .filter-item {
        padding: 15px 15px 25px;
    }
}

.filter-item .title {
    margin-bottom: 15px;
}

.filter-item .bus-type li {
    width: 100%;
    padding: 5px 0px;
}

.filter-item .bus-type li span {
    display: block;
    padding: 3px 8px;
    background: #f6f6f7;
    border-radius: 3px;
    font-size: 14px;
}

.filter-item .bus-type li span i {
    margin-right: 5px;
}

    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
 
    <style>
       td.day{
            background: #2d509b;
            color:white;
        }
        .datepicker table tr td.day:hover, .datepicker table tr td.day.focused{
            background: #2d509b;
            color:white;
        }
    </style>
    @stack('style-lib')

    @stack('style')
   
</head>

<body>
    <!-- <div class="overlay"></div>
    @stack('fbComment') -->
    <div class="page text-center">
    <!-- @include($activeTemplate .'partials.preloader') -->
    @if(request()->routeIs('home'))
        @include($activeTemplate .'partials.new.header',['banner'=>true])
    @else 
        @include($activeTemplate .'partials.new.header',['banner'=>false])
    @endif
    <main class="page-content">
        @yield('content')
    </main>
    @if(request()->routeIs('home'))
        @include($activeTemplate .'partials.new.footer')
    @else
        @include($activeTemplate .'partials.new.footer-blue')
    @endif
    </div>


    <!-- <a href="javascript::void()" class="scrollToTop active"><i class="las la-chevron-up"></i></a> -->
  
   
    <script src="{{asset($activeTemplateTrue.'js/newjs/core.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/newjs/script.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <script>
        $(document).ready(function () {
            var date=new Date();
            var year=date.getFullYear();
            var month=date.getMonth();
            let availableDates=[];
            getAvailableDates().then((availableDates)=>{
                InitDatePicker(availableDates);
    
            });
            
            async function getAvailableDates(){
            
            let res=await fetch("{{route('availabletripdate')}}");
            
            let data= await res.json(); 
            
            return data;
            }
    
            function InitDatePicker(availableDates){
            var date_of_journey=document.querySelectorAll('.date_of_journey');
            
            date_of_journey.forEach(element => {
                
                $(element).datepicker({
                    format: 'dd-mm-yyyy',
                    startDate: new Date(year, month, '01'),
                    endDate: new Date(year+1, month, '31'),
                    beforeShowDay: function(date){
                    
                        dmy = date.getDate() + "-" + (date.getMonth() + 1) + "-" + date.getFullYear();
                    
                        if(availableDates.indexOf(dmy) != -1){
                        
                        return true;
                        }else{
                            return false;
                        }
                    }
                });
            });
            
            }
        
        });
     </script>
    @stack('script-lib')

    @stack('script')

    @include('partials.plugins')

    @include('partials.notify')
</body>

</html>
