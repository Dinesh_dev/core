<!doctype html>
<html lang="en" itemscope itemtype="http://schema.org/WebPage">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> {{ $general->sitename(__($pageTitle)) }}</title>
    @include('partials.seo')
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,400italic,700%7CLato:400">

    <!-- BootStrap Link -->
    <link rel="stylesheet" href="{{ asset($activeTemplateTrue.'css/bootstrap.min.css') }}">
    <!-- Icon Link -->
    <link rel="stylesheet" href="{{ asset('assets/global/css/all.min.css') }}">
    <link rel="stylesheet" href="{{asset('assets/global/css/line-awesome.min.css') }}">
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/flaticon.css') }}">

    <!-- Plugings Link -->
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/slick.css') }}">
    <link rel="stylesheet" href="{{asset('assets/global/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/jquery-ui.css') }}">

    <!-- Cookie Link -->
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/cookie.css') }}">
    <!-- Custom Link -->
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/main.css') }}">
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset($activeTemplateTrue.'css/color.php?color='.$general->base_color)}}">
    <link rel="stylesheet" href="{{ asset($activeTemplateTrue.'css/modify.css')}}">
    <link rel="stylesheet" href="{{ asset($activeTemplateTrue.'css/rdnavbar.css')}}">

    <!-- <link rel="stylesheet" href="{{ asset($activeTemplateTrue.'css/newcss/style.css')}}"> -->

    <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css"
    />
    <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" /> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.1/css/font-awesome.min.css" integrity="sha512-H/zVLBHVS8ZRNSR8wrNZrGFpuHDyN6+p6uaADRefLS4yZYRxfF4049g1GhT+gDExFRB5Kf9jeGr8vueDsyBhhA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  
  


      <style>
        .reveal-inline-block {
    display: inline-block !important;
}
.list-inline-2 {
    margin-left: -10px;
    margin-right: -10px;
    padding-left: 8px;
}
.list-inline {
    margin-left: -5px;
    margin-right: -5px;
}
.list-inline {
    padding-left: 0;
    list-style: none;
    margin-left: -5px;
}
.slider-menu-position {
  position: relative; }
  .slider-menu-position .rd-navbar-wrap {
    position: absolute;
    top: 0;
    left: 0;
    right: 0; }
    .section-height-800{
      height:500px;
    }
    @media (min-width: 1200px) and (max-height: 800px) {
 .section-height-800 [class*="section-"] {
    padding-top: 220px; }
  .section-height-800 ul[class*="offset-top-"] {
    margin-top: 70px; } }
    .bg-gray-darkest {
  background: #212121;
  fill: #212121; }
  .rd-navbar-nav li > a{
    color:white !important;
  }
  .rd-navbar-nav li > a:hover{
    color:black !important;
  }
</style>

<style>
.post-item:hover .post-thumb img {
    -webkit-transform: scale(1.1);
    transform: scale(1.1);
}

.post-item:hover .post-meta {
    /* border-top-color: rgba(14, 158, 77, 0.3); */
}
.post-thumb {
    overflow: hidden;
}

.post-thumb img {
    width: 100%;
}

.post-content {
    padding: 15px 20px 5px 20px;
    
    -webkit-box-shadow: 0 0 10px 1px rgba(27, 39, 61, 0.1);
    box-shadow: 0 0 10px 1px rgba(27, 39, 61, 0.1);
    border-radius: 0 0 5px 5px;
    /* Changes */
    position:absolute !important;
    top:0 !important;
}

.post-content .date {
    font-size: 14px;
}

.post-content .date i {
    font-size: 16px;
    color: var(--main-color);
    margin-right: 4px;
}

.post-content .title {
    margin-bottom: 10px;
}

.post-content .title a {
    display: -webkit-box;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 1;
}

.post-content p {
    display: -webkit-box;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 2;
}

.post-content .post-meta {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    
    margin-bottom:5px;
    -webkit-transition: all ease 0.3s;
    transition: all ease 0.3s;
}

.post-content .post-meta li {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}

.post-content .post-meta li div {
    margin-right: 25px;
}

.post-content .post-meta li i {
    margin-right: 5px;
    /* color: var(--main-color); */
    color:white;
}

.post-details-content {
    padding-top: 25px;
}

.post-details-content .meta-post li {
    margin-right: 25px;
}

.post-details-content .meta-post li i {
    margin-right: 5px;
    color: var(--main-color);
}

.post-details-content .title {
    margin-bottom: 25px;
}

.post-details-content .blog-details-pera {
    margin-bottom: 20px;
}

.post-details-content .info li {
    padding-left: 20px;
    position: relative;
}

.post-details-content .info li::before {
    position: absolute;
    content: "";
    width: 100%;
    height: 100%;
    content: "\f14a";
    font-weight: 900;
    font-family: "Line Awesome Free";
    left: 0;
    top: 6px;
    color: #1b273d;
    margin-right: 5px;
}

.post-details-content .quote-wrapper {
    margin-top: 20px;
    margin-bottom: 20px;
    padding: 25px 20px;
    background: rgba(27, 39, 61, 0.03);
}

.post-details-content .quote-wrapper p {
    border-left: 2px solid var(--main-color);
    padding-left: 10px;
}

.post-details-content .quote-wrapper p i {
    font-size: 46px;
    color: var(--main-color);
}

.post-details-content .content-inner {
    border-bottom: 1px solid rgba(27, 39, 61, 0.1);
}

.post-details-content .content-inner .meta-post {
    margin: 5px 0;
    margin-bottom: 10px;
}

.post-details-content .content-inner .meta-post li {
    padding: 2px 0;
}

.post-details-content .meta-content {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    -webkit-box-pack: justify;
    -ms-flex-pack: justify;
    justify-content: space-between;
    padding: 15px 0;
}

.post-details-content .meta-content .title {
    margin: 0;
    margin-right: 15px;
}

.post-details-content .meta-content li {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
}

.post-details-content .meta-content li .la-heart {
    color: #f53b57;
}

.post-details-content .meta-content li i {
    margin-right: 7px;
}

.post-details-content .social-icons {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-top: 0;
}

.post-details-content .social-icons li a {
    background: none;
    margin: 5px;
    border: none;
}

.post-details-content .social-icons li a:hover i {
    color: initial !important;
}

.post-details-content .social-icons li i {
    margin: 0 !important;
}

.post-author-area {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    margin: 40px 0;
    padding: 30px 20px;
    background: rgba(27, 39, 61, 0.03);
}

.post-author-area .author-thumb {
    width: 100px;
    height: 100px;
    border-radius: 50%;
    overflow: hidden;
}

.post-author-area .author-thumb img {
    height: 100%;
    width: 100%;
    -o-object-fit: cover;
    object-fit: cover;
}

.post-author-area .author-content {
    width: calc(100% - 100px);
    padding-left: 25px;
}

.post-author-area .author-content .name {
    margin-bottom: 10px;
}

.post-comment-area .post-comment-title {
    margin-bottom: 25px;
}
.blogele{
    height:100%;
    width:100%;
    display:flex;
    flex-direction:column;
    justify-content:space-between;
}
.blogtag{
    background:#f0e319;
    color:black;
    padding:5px;
    
}
.post-meta{
  border-bottom:none !important;
}

.post-content .title a{
  color:white !important;
}
</style>
  </style>
    @stack('style-lib')

    @stack('style')
</head>

<body>
    <div class="overlay"></div>
    @stack('fbComment')

    @include($activeTemplate .'partials.preloader')
    <header class="slider-menu-position">
      @include($activeTemplate .'partials.header')
    </header>

    @yield('content')
    @include($activeTemplate .'partials.footer')


    <a href="javascript::void()" class="scrollToTop active"><i class="las la-chevron-up"></i></a>
  
    <script src="{{asset('assets/global/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/bootstrap.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/slick.min.js') }}"></script>
    <script src="{{asset('assets/global/js/select2.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/jquery-ui.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/main.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/rdnavbar.js') }}"></script>

    <!-- <script src="{{asset($activeTemplateTrue.'js/newjs/core.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/newjs/script.js') }}"></script> -->


    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>

    <script>
       AOS.init();

      </script>
    @stack('script-lib')

    @stack('script')

    @include('partials.plugins')

    @include('partials.notify')
</body>

</html>
