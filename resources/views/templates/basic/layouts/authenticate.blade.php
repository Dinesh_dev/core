<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('partials.seo')
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:400,400italic,700%7CLato:400">

    <!-- BootStrap Link -->
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/bootstrap.min.css') }}">

    <!-- Icon Link -->
    <link rel="stylesheet" href="{{ asset('assets/global/css/all.min.css') }}">
    <link rel="stylesheet" href="{{asset('assets/global/css/line-awesome.min.css') }}">
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/flaticon.css') }}">

    <!-- Plugings Link -->
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/slick.css') }}">
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/jquery-ui.css') }}">

    <!-- Custom Link -->
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/main.css') }}">
    <link rel="stylesheet" href="{{asset($activeTemplateTrue.'css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset($activeTemplateTrue.'css/color.php?color='.$general->base_color)}}">

    <title>{{ $general->sitename(__($pageTitle)) }}</title>
    <style>
        .btn-ripe-lemon {
  color: #292a2d;
  background-color: #f0e319;
  border-color: #f0e319; }
  .btn-ripe-lemon:focus, .btn-ripe-lemon.focus, .btn-ripe-lemon:hover, .btn-ripe-lemon:active, .btn-ripe-lemon.active,
  .open > .btn-ripe-lemon.dropdown-toggle {
    color: #292a2d;
    background-color: #c9be0d;
    border-color: #c9be0d; }
  .btn-ripe-lemon:active, .btn-ripe-lemon.active,
  .open > .btn-ripe-lemon.dropdown-toggle {
    -webkit-box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.15);
    box-shadow: 0 1px 10px 0 rgba(0, 0, 0, 0.15); }
    .btn-ripe-lemon:active:hover, .btn-ripe-lemon:active:focus, .btn-ripe-lemon:active.focus, .btn-ripe-lemon.active:hover, .btn-ripe-lemon.active:focus, .btn-ripe-lemon.active.focus,
    .open > .btn-ripe-lemon.dropdown-toggle:hover,
    .open > .btn-ripe-lemon.dropdown-toggle:focus,
    .open > .btn-ripe-lemon.dropdown-toggle.focus {
      color: #292a2d;
      background-color: #c9be0d;
      border-color: #c9be0d; }
  .btn-ripe-lemon.disabled, .btn-ripe-lemon[disabled],
  fieldset[disabled] .btn-ripe-lemon {
    pointer-events: none;
    opacity: .5; }
  .btn-ripe-lemon .badge {
    color: #f0e319;
    background-color: #292a2d; }
    .text-dark{
        color:#8f8f8f !important;
    }

    .form--control:focus {
  border: 1px solid #8f8f8f !important;
}
.text-height-md{
    height:50px;
    border-radius: 0px !important;
}
.account-form-wrapper p a:hover {
  color: #18377a;
  border-color: #18377a;
}
a:hover {
  color:#18377a;
}
    </style>
    @stack('style')
</head>
<body>
    <div class="overlay"></div>
    <!-- Preloader -->
	{{--
    <div class="preloader">
        <div class="loader-wrapper">
            <div class="truck-wrapper">
              <div class="truck">
                <div class="truck-container"></div>
                <div class="glases"></div>
                <div class="bonet"></div>

                <div class="base"></div>

                <div class="base-aux"></div>
                <div class="wheel-back"></div>
                <div class="wheel-front"></div>

                <div class="smoke"></div>
              </div>
            </div>
        </div>
    </div>
	--}}
    <!-- Preloader -->

    @yield('content')

    <script src="{{asset('assets/global/js/jquery-3.6.0.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/bootstrap.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/slick.min.js') }}"></script>
    <script src="{{asset('assets/global/js/select2.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/jquery-ui.min.js') }}"></script>
    <script src="{{asset($activeTemplateTrue.'js/main.js') }}"></script>
    <script src="{{ asset('assets/global/js/secure_password.js') }}"></script>


    @include('partials.plugins')

    @include('partials.notify')
    @stack('script')
</body>
</html>
