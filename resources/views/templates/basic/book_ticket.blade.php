@extends($activeTemplate.'layouts.newfrontend')
@section('content')

<div class="section-90 section-md-111">
    <div class="container">
        <div class="range range-xs-center" style="text-align: left">
            <div class="cell-lg-8 cell-md-8">
                <div class="offset-top-20 offset-md-top-30">
                    <h2 class="text-bold  text-md-left">Fill the details</h2>
                    <hr class="divider hr-sm-left-0 bg-chathams-blue">
					@if(Session::get('ticket'))
					<h6 class="title"> Reuse ticket : {{ Session::get('ticket')->pnr_number }}</h6>
					@endif
                    <form action="{{ route('ticket.book', $trip->id) }}" method="POST" id="bookingForm" class="">
                        @csrf
                        <input type="text" name="price" hidden>
                        <div class="range range-xs-center">
						@if (Auth::guest())
                        
                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside">
                                    <label for="customer_name" class="form-label-outside text-dark">@lang('Customer Name')</label>
                                    <input type="text" id="customer_name" class="form-control" name="customer_name">
                                </div>
                                
                            </div>
                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside">
                                    <label for="customer_email" class="form-label-outside text-dark">@lang('Customer Email')</label>
                                    <input type="email" id="customer_email" class="form-control" name="customer_email">
                                </div>
                            </div>
                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside offset-top-20">
                                    <label for="customer_contact_no" class="form-label-outside text-dark">@lang('Customer Contact No.')</label>
                                    <input type="text" id="customer_contact_no" class="form-control" name="customer_contact_no">
                                </div>
    
                            </div>
                           
                       
						
						
						@endif
                        <div class="cell-sm-6">
                            <div class="form-group form-group-label-outside offset-top-20">
                                <label for="kin_name" class="form-label-outside text-dark">@lang('Kin Name')</label>
                                <input type="text" id="kin_name" class="form-control" name="kin_name">
                            </div>
                        </div>
                       
                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside offset-top-20">
                                    <label for="kin_email" class="form-label-outside text-dark">@lang('Kin Email')</label>
                                    <input type="email" id="kin_email" class="form-control" name="kin_email">
                                </div>
                               
                            </div>
                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside offset-top-20">
                                    <label for="kin_contact_no" class="form-label-outside text-dark">@lang('Kin Contact No.')</label>
                                    <input type="text" id="kin_contact_no" class="form-control" name="kin_contact_no">
                                </div>
                            </div>
                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside offset-top-20">
                                    <label for="date_of_journey" class="form-label-outside text-dark">@lang('Departure Date')</label>
                                    <input type="text" id="date_of_journey" autocomplete="off" class="form-control datepicker" value="{{$trip->scheduledate}}" name="date_of_journey" readonly>
                                   
                                </div>
                               
                               
                           
                            </div>
                            @if( Session::get('date_of_return'))
                                <div class="cell-sm-6">
                                    <div class="form-group form-group-label-outside offset-top-20">
                                        <label for="date_of_return" class="form-label-outside text-dark">@lang('Return Date')</label>
                                        <input type="text" id="date_of_return" class="form-control datepicker" value="{{ Session::get('date_of_return') ? Session::get('date_of_return') : date('m/d/Y') }}" name="date_of_return">
                                    </div>
                                </div>
                            @endif

                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside offset-top-20">
                                    <label for="pickup_point" class="form-label-outside text-dark">@lang('Pickup Point')</label>
                                     @foreach($stoppages as $item)
                                        @if (Session::get('pickup')==$item->id || $item->id == $trip->start_from)
                                            <input type="text" id="pickup_point_name" autocomplete="off" class="form-control " value="{{$item->name}}"  readonly style="background:linear-gradient(90deg, #3256a4 0%, #18377a 100%);color:#fff">
                                        @endif
                                    @endforeach
                                    <div style="display: none">
                                        <select name="pickup_point" id="pickup_point" class="form-control select2" {{ Session::get('ticket') ? 'disabled' : '' }}>
                                            <option value="">@lang('Select One')</option>
                                            @foreach($stoppages as $item)
                                            <option value="{{ $item->id }}" @if (Session::get('pickup')==$item->id || $item->id == $trip->start_from)
                                                selected
                                                @endif>
                                                {{ __($item->name) }}
                                            </option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside offset-top-20">
                                    <label for="dropping_point" class="form-label-outside text-dark">@lang('Dropping Point')</label>
                                   @foreach($stoppages as $item)
                                        @if (Session::get('destination')==$item->id || $item->id == $trip->end_to)
                                            <input type="text" id="dropping_point_name" autocomplete="off" class="form-control " value="{{$item->name}}"  readonly style="background:linear-gradient(90deg, #3256a4 0%, #18377a 100%);color:#fff">
                                        @endif
                                    @endforeach
                                    <div style="display: none">
                                        <select name="dropping_point" id="dropping_point" class="form-control " {{ Session::get('ticket') ? 'disabled' : '' }} >
                                            <option value="">@lang('Select One')</option>
                                            @foreach($stoppages as $item)
                                            <option value="{{ $item->id }}" @if (Session::get('destination')==$item->id || $item->id == $trip->end_to)
                                                selected
                                                @endif>
                                                {{ __($item->name) }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside offset-top-20">
                                    <label for="passenger_no" class="form-label-outside text-dark">@lang('No. of Passenger')</label>
                                    <select name="passenger_no" id="passenger_no" class="form-control" {{ Session::get('ticket') ? 'disabled' : '' }}>
                                        @foreach([1,2,3,4,5,6,7] as $passenger_no)
                                        <option value="{{ $passenger_no }}" @if (Session::get('passenger_no')==$passenger_no)
                                            selected
                                            @endif>
                                            {{ __($passenger_no) }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="cell-sm-6 offset-top-20">
                                <label class="form-label-outside text-dark">@lang('Select Gender')</label>
                                <div class=" ">
                                    <div class="form-group form-group-label-outside custom--radio">
                                        <input id="male" type="radio" name="gender" value="1">
                                        <label class="form-label-outside text-dark" for="male">@lang('Male')</label>
                                    </div>
                                    <div class="form-group form-group-label-outside custom--radio">
                                        <input id="female" type="radio" name="gender" value="2">
                                        <label class="form-label-outside text-dark" for="female">@lang('Female')</label>
                                    </div>
                                    <div class="form-group form-group-label-outside custom--radio">
                                        <input id="other" type="radio" name="gender" value="3">
                                        <label class="form-label-outside text-dark" for="other">@lang('Other')</label>
                                    </div>
                                </div>
                            </div>
                            <div class="cell-sm-6 offset-top-20">
                                
                                <div class="booked-seat-details my-3 d-none">
                                    <label>@lang('Selected Seats')</label>
                                    <div class="list-group seat-details-animate">
                                        <span class="list-group-item d-flex bg--base text-white justify-content-between">@lang('Seat Details')<span>@lang('Price')</span></span>
                                        <div class="selected-seat-details">
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                       
                    </div>
                    
                    <input type="text" name="seats" hidden>

                        <input type="text" name="seats" hidden> 
                        <div class="cell-sm-12 offset-top-20 " style="margin-bottom:30px;display:flex;justify-content:center;">
                            <button type="submit" class="btn btn-primary">@lang('Pay Now')</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="cell-lg-4 cell-md-4 offset-md-top-40" style="margin-top:160px">
                <h6 class="title">@lang('Click on Seat to select or deselect')</h6>
				{{--
                @if ($trip->day_off)
                <span class="fs--14px">
                    @lang('Off Days') :
                    @foreach ($trip->day_off as $item)
                    <span class="badge badge--success">
                        {{ __(showDayOff($item)) }}
                        @if (!$loop->last)
                        ,
                        @endif
                    </span>
                    @endforeach
                </span>
                @endif
				--}}
                @foreach ($trip->fleetType->deck_seats as $seat)
                <div class="seat-plan-inner">
                    <div class="single" style="max-width: 50%">
					  <span class="front" style="color:black">Front</span>
					  <span class="rear" style="color:black">Rear</span>
					  {{--
					  <span class="lower" style="right: 20px">Door</span>
					  <span class="driver" style="left: 20px">
						<img src="{{ env('APP_URL') }}/assets/templates/basic/images/icon/wheel.svg" alt="icon">
					  </span>
					  --}}
					  <div class="seat-wrapper seat-wrapper-first">
						<div class="left-side">
						  <div>
							<img src="{{ env('APP_URL') }}/assets/templates/basic/images/icon/wheel.svg" alt="icon" style="width:40px">
						  </div>
						  <div>
							<span class='seat' style='opacity: 0;pointer-events: none;'>  <span></span>
							</span>
						  </div>
						  <div>
							<span class='seat'  data-seat='1-A1' > 
                                <span class="seatlegend">A1</span>
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                              
                                    <g>
                                    <g>
                                    <path class="st0" d="M477.8,262.4v166.8c0,26.9-21.8,48.7-48.7,48.7h-3.6c-6.6,0-12.8-1.3-18.5-3.7c-0.1,0-0.1,0-0.1,0
                                        c-5.7,2.3-11.9,3.6-18.4,3.6H124.2c-6.7,0-13-1.3-18.8-3.7c-5.8,2.4-12.3,3.8-19,3.8h-3.6c-26.9,0-48.6-21.8-48.6-48.7V262.4
                                        c0-24.4,17.9-44.6,41.3-48.1V82.8c0-26.9,21.8-48.7,48.7-48.7h264.4c26.9,0,48.7,21.8,48.7,48.7v131.5
                                        C460.3,218.3,477.8,238.3,477.8,262.4z"/>
                                    </g>
                                    <g>
                                    <g>
                                        <path d="M388.6,478.7H124.2c-5.6,0-11.1-0.9-16.3-2.6l-9-3l8.4-4.5c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7
                                            h-3.6c-2.6,0-4.7,0.2-6.6,0.6l-4.9,1V82.8c0-29.1,23.7-52.8,52.8-52.8h264.4c29.1,0,52.8,23.7,52.8,52.8v136.4l-4.8-0.9
                                            c-2.5-0.4-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7l8.9,4.6l-9.6,2.9
                                            C399,477.9,393.7,478.7,388.6,478.7z M119.4,470.3c1.6,0.2,3.1,0.2,4.7,0.2h264.4c1.3,0,2.6-0.1,4-0.2
                                            c-12.4-9.9-19.7-24.9-19.7-41.2V262.4c0-29.1,23.6-52.8,52.7-52.8h3.6c1.4,0,2.8,0.1,4.1,0.2V82.8c0-24.6-20-44.7-44.7-44.7
                                            H124.2c-24.6,0-44.7,20-44.7,44.7v126.9c1.1-0.1,2.2-0.1,3.3-0.1h3.6c29.1,0,52.8,23.7,52.8,52.8v166.8
                                            C139.2,445.4,131.8,460.4,119.4,470.3z"/>
                                    </g>
                                    <g>
                                        <path d="M388.6,481.9H124.2c-7.1,0-14-1.4-20.3-4.1l-8.7-3.7l8.6-3.8c1.2-0.5,2.3-1.1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5v-93.6
                                            h249.9v93.6c0,16.8,9.2,32,24,39.7c1.1,0.6,2.1,1,3.2,1.5l8.8,3.9l-8.6,3.7C402,480.6,395.3,481.9,388.6,481.9z M115.8,473
                                            c2.7,0.5,5.5,0.8,8.3,0.8h264.4c2.6,0,5.1-0.2,7.7-0.7c-14.6-9.7-23.4-26.1-23.4-43.9v-85.5H139.2v85.5
                                            C139.2,446.9,130.4,463.2,115.8,473z"/>
                                    </g>
                                    <g>
                                        <path d="M86.4,481.9h-3.6c-29.1,0-52.7-23.7-52.7-52.8V262.4c0-26.3,18.8-48.2,44.7-52.1c2.2-0.5,4.9-0.7,8-0.7h3.6
                                            c29.1,0,52.8,23.7,52.8,52.8v166.8c0,19.6-10.8,37.5-28.1,46.7c-1.3,0.7-2.6,1.3-4,1.9C100.4,480.6,93.5,481.9,86.4,481.9z
                                                M82.8,217.7c-2.6,0-4.7,0.2-6.6,0.6c-22.1,3.3-38,21.8-38,44.1v166.8c0,24.6,20,44.7,44.6,44.7h3.6c6,0,11.9-1.2,17.4-3.5
                                            c1.1-0.5,2.3-1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7H82.8z"/>
                                    </g>
                                    <g>
                                        <path d="M429.1,481.9h-3.6c-6.7,0-13.2-1.2-19.3-3.7l-0.9-0.3c-1.6-0.7-2.9-1.3-4.1-1.9c-17.5-9.1-28.4-27.1-28.4-46.9V262.4
                                            c0-29.1,23.6-52.8,52.7-52.8h3.6c3.1,0,6,0.3,8.9,0.8c25.4,4.3,43.9,26.2,43.9,52v166.8C481.9,458.3,458.2,481.9,429.1,481.9z
                                                M408,470.3l0.5,0.2c5.4,2.2,11.1,3.3,17,3.3h3.6c24.6,0,44.7-20,44.7-44.7V262.4c0-21.8-15.6-40.3-37.1-44
                                            c-2.5-0.5-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7C406,469.4,407,469.8,408,470.3z"/>
                                    </g>
                                    </g>
                                    </g>
                                </svg>



                            </span>
						  </div>
						</div>
					  </div>
					   <div class="seat-wrapper">
						<div class="left-side">
						  <div>
							<span class='seat'  data-seat='1-B2'> 
                                <span class="seatlegend">B2</span>
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                              
                                    <g>
                                    <g>
                                    <path class="st0" d="M477.8,262.4v166.8c0,26.9-21.8,48.7-48.7,48.7h-3.6c-6.6,0-12.8-1.3-18.5-3.7c-0.1,0-0.1,0-0.1,0
                                        c-5.7,2.3-11.9,3.6-18.4,3.6H124.2c-6.7,0-13-1.3-18.8-3.7c-5.8,2.4-12.3,3.8-19,3.8h-3.6c-26.9,0-48.6-21.8-48.6-48.7V262.4
                                        c0-24.4,17.9-44.6,41.3-48.1V82.8c0-26.9,21.8-48.7,48.7-48.7h264.4c26.9,0,48.7,21.8,48.7,48.7v131.5
                                        C460.3,218.3,477.8,238.3,477.8,262.4z"/>
                                    </g>
                                    <g>
                                    <g>
                                        <path d="M388.6,478.7H124.2c-5.6,0-11.1-0.9-16.3-2.6l-9-3l8.4-4.5c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7
                                            h-3.6c-2.6,0-4.7,0.2-6.6,0.6l-4.9,1V82.8c0-29.1,23.7-52.8,52.8-52.8h264.4c29.1,0,52.8,23.7,52.8,52.8v136.4l-4.8-0.9
                                            c-2.5-0.4-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7l8.9,4.6l-9.6,2.9
                                            C399,477.9,393.7,478.7,388.6,478.7z M119.4,470.3c1.6,0.2,3.1,0.2,4.7,0.2h264.4c1.3,0,2.6-0.1,4-0.2
                                            c-12.4-9.9-19.7-24.9-19.7-41.2V262.4c0-29.1,23.6-52.8,52.7-52.8h3.6c1.4,0,2.8,0.1,4.1,0.2V82.8c0-24.6-20-44.7-44.7-44.7
                                            H124.2c-24.6,0-44.7,20-44.7,44.7v126.9c1.1-0.1,2.2-0.1,3.3-0.1h3.6c29.1,0,52.8,23.7,52.8,52.8v166.8
                                            C139.2,445.4,131.8,460.4,119.4,470.3z"/>
                                    </g>
                                    <g>
                                        <path d="M388.6,481.9H124.2c-7.1,0-14-1.4-20.3-4.1l-8.7-3.7l8.6-3.8c1.2-0.5,2.3-1.1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5v-93.6
                                            h249.9v93.6c0,16.8,9.2,32,24,39.7c1.1,0.6,2.1,1,3.2,1.5l8.8,3.9l-8.6,3.7C402,480.6,395.3,481.9,388.6,481.9z M115.8,473
                                            c2.7,0.5,5.5,0.8,8.3,0.8h264.4c2.6,0,5.1-0.2,7.7-0.7c-14.6-9.7-23.4-26.1-23.4-43.9v-85.5H139.2v85.5
                                            C139.2,446.9,130.4,463.2,115.8,473z"/>
                                    </g>
                                    <g>
                                        <path d="M86.4,481.9h-3.6c-29.1,0-52.7-23.7-52.7-52.8V262.4c0-26.3,18.8-48.2,44.7-52.1c2.2-0.5,4.9-0.7,8-0.7h3.6
                                            c29.1,0,52.8,23.7,52.8,52.8v166.8c0,19.6-10.8,37.5-28.1,46.7c-1.3,0.7-2.6,1.3-4,1.9C100.4,480.6,93.5,481.9,86.4,481.9z
                                                M82.8,217.7c-2.6,0-4.7,0.2-6.6,0.6c-22.1,3.3-38,21.8-38,44.1v166.8c0,24.6,20,44.7,44.6,44.7h3.6c6,0,11.9-1.2,17.4-3.5
                                            c1.1-0.5,2.3-1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7H82.8z"/>
                                    </g>
                                    <g>
                                        <path d="M429.1,481.9h-3.6c-6.7,0-13.2-1.2-19.3-3.7l-0.9-0.3c-1.6-0.7-2.9-1.3-4.1-1.9c-17.5-9.1-28.4-27.1-28.4-46.9V262.4
                                            c0-29.1,23.6-52.8,52.7-52.8h3.6c3.1,0,6,0.3,8.9,0.8c25.4,4.3,43.9,26.2,43.9,52v166.8C481.9,458.3,458.2,481.9,429.1,481.9z
                                                M408,470.3l0.5,0.2c5.4,2.2,11.1,3.3,17,3.3h3.6c24.6,0,44.7-20,44.7-44.7V262.4c0-21.8-15.6-40.3-37.1-44
                                            c-2.5-0.5-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7C406,469.4,407,469.8,408,470.3z"/>
                                    </g>
                                    </g>
                                    </g>
                                </svg>
							</span>
						  </div>
						  <div>
							<span class='seat' data-seat='1-B3'> 
                                <span class="seatlegend">B3</span>
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                              
                                    <g>
                                    <g>
                                    <path class="st0" d="M477.8,262.4v166.8c0,26.9-21.8,48.7-48.7,48.7h-3.6c-6.6,0-12.8-1.3-18.5-3.7c-0.1,0-0.1,0-0.1,0
                                        c-5.7,2.3-11.9,3.6-18.4,3.6H124.2c-6.7,0-13-1.3-18.8-3.7c-5.8,2.4-12.3,3.8-19,3.8h-3.6c-26.9,0-48.6-21.8-48.6-48.7V262.4
                                        c0-24.4,17.9-44.6,41.3-48.1V82.8c0-26.9,21.8-48.7,48.7-48.7h264.4c26.9,0,48.7,21.8,48.7,48.7v131.5
                                        C460.3,218.3,477.8,238.3,477.8,262.4z"/>
                                    </g>
                                    <g>
                                    <g>
                                        <path d="M388.6,478.7H124.2c-5.6,0-11.1-0.9-16.3-2.6l-9-3l8.4-4.5c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7
                                            h-3.6c-2.6,0-4.7,0.2-6.6,0.6l-4.9,1V82.8c0-29.1,23.7-52.8,52.8-52.8h264.4c29.1,0,52.8,23.7,52.8,52.8v136.4l-4.8-0.9
                                            c-2.5-0.4-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7l8.9,4.6l-9.6,2.9
                                            C399,477.9,393.7,478.7,388.6,478.7z M119.4,470.3c1.6,0.2,3.1,0.2,4.7,0.2h264.4c1.3,0,2.6-0.1,4-0.2
                                            c-12.4-9.9-19.7-24.9-19.7-41.2V262.4c0-29.1,23.6-52.8,52.7-52.8h3.6c1.4,0,2.8,0.1,4.1,0.2V82.8c0-24.6-20-44.7-44.7-44.7
                                            H124.2c-24.6,0-44.7,20-44.7,44.7v126.9c1.1-0.1,2.2-0.1,3.3-0.1h3.6c29.1,0,52.8,23.7,52.8,52.8v166.8
                                            C139.2,445.4,131.8,460.4,119.4,470.3z"/>
                                    </g>
                                    <g>
                                        <path d="M388.6,481.9H124.2c-7.1,0-14-1.4-20.3-4.1l-8.7-3.7l8.6-3.8c1.2-0.5,2.3-1.1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5v-93.6
                                            h249.9v93.6c0,16.8,9.2,32,24,39.7c1.1,0.6,2.1,1,3.2,1.5l8.8,3.9l-8.6,3.7C402,480.6,395.3,481.9,388.6,481.9z M115.8,473
                                            c2.7,0.5,5.5,0.8,8.3,0.8h264.4c2.6,0,5.1-0.2,7.7-0.7c-14.6-9.7-23.4-26.1-23.4-43.9v-85.5H139.2v85.5
                                            C139.2,446.9,130.4,463.2,115.8,473z"/>
                                    </g>
                                    <g>
                                        <path d="M86.4,481.9h-3.6c-29.1,0-52.7-23.7-52.7-52.8V262.4c0-26.3,18.8-48.2,44.7-52.1c2.2-0.5,4.9-0.7,8-0.7h3.6
                                            c29.1,0,52.8,23.7,52.8,52.8v166.8c0,19.6-10.8,37.5-28.1,46.7c-1.3,0.7-2.6,1.3-4,1.9C100.4,480.6,93.5,481.9,86.4,481.9z
                                                M82.8,217.7c-2.6,0-4.7,0.2-6.6,0.6c-22.1,3.3-38,21.8-38,44.1v166.8c0,24.6,20,44.7,44.6,44.7h3.6c6,0,11.9-1.2,17.4-3.5
                                            c1.1-0.5,2.3-1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7H82.8z"/>
                                    </g>
                                    <g>
                                        <path d="M429.1,481.9h-3.6c-6.7,0-13.2-1.2-19.3-3.7l-0.9-0.3c-1.6-0.7-2.9-1.3-4.1-1.9c-17.5-9.1-28.4-27.1-28.4-46.9V262.4
                                            c0-29.1,23.6-52.8,52.7-52.8h3.6c3.1,0,6,0.3,8.9,0.8c25.4,4.3,43.9,26.2,43.9,52v166.8C481.9,458.3,458.2,481.9,429.1,481.9z
                                                M408,470.3l0.5,0.2c5.4,2.2,11.1,3.3,17,3.3h3.6c24.6,0,44.7-20,44.7-44.7V262.4c0-21.8-15.6-40.3-37.1-44
                                            c-2.5-0.5-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7C406,469.4,407,469.8,408,470.3z"/>
                                    </g>
                                    </g>
                                    </g>
                                </svg>
							</span>
						  </div>
						  <div>
							<span class='seat' data-seat='1-B4'> 
                                <span class="seatlegend">B4</span>
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                              
                                    <g>
                                    <g>
                                    <path class="st0" d="M477.8,262.4v166.8c0,26.9-21.8,48.7-48.7,48.7h-3.6c-6.6,0-12.8-1.3-18.5-3.7c-0.1,0-0.1,0-0.1,0
                                        c-5.7,2.3-11.9,3.6-18.4,3.6H124.2c-6.7,0-13-1.3-18.8-3.7c-5.8,2.4-12.3,3.8-19,3.8h-3.6c-26.9,0-48.6-21.8-48.6-48.7V262.4
                                        c0-24.4,17.9-44.6,41.3-48.1V82.8c0-26.9,21.8-48.7,48.7-48.7h264.4c26.9,0,48.7,21.8,48.7,48.7v131.5
                                        C460.3,218.3,477.8,238.3,477.8,262.4z"/>
                                    </g>
                                    <g>
                                    <g>
                                        <path d="M388.6,478.7H124.2c-5.6,0-11.1-0.9-16.3-2.6l-9-3l8.4-4.5c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7
                                            h-3.6c-2.6,0-4.7,0.2-6.6,0.6l-4.9,1V82.8c0-29.1,23.7-52.8,52.8-52.8h264.4c29.1,0,52.8,23.7,52.8,52.8v136.4l-4.8-0.9
                                            c-2.5-0.4-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7l8.9,4.6l-9.6,2.9
                                            C399,477.9,393.7,478.7,388.6,478.7z M119.4,470.3c1.6,0.2,3.1,0.2,4.7,0.2h264.4c1.3,0,2.6-0.1,4-0.2
                                            c-12.4-9.9-19.7-24.9-19.7-41.2V262.4c0-29.1,23.6-52.8,52.7-52.8h3.6c1.4,0,2.8,0.1,4.1,0.2V82.8c0-24.6-20-44.7-44.7-44.7
                                            H124.2c-24.6,0-44.7,20-44.7,44.7v126.9c1.1-0.1,2.2-0.1,3.3-0.1h3.6c29.1,0,52.8,23.7,52.8,52.8v166.8
                                            C139.2,445.4,131.8,460.4,119.4,470.3z"/>
                                    </g>
                                    <g>
                                        <path d="M388.6,481.9H124.2c-7.1,0-14-1.4-20.3-4.1l-8.7-3.7l8.6-3.8c1.2-0.5,2.3-1.1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5v-93.6
                                            h249.9v93.6c0,16.8,9.2,32,24,39.7c1.1,0.6,2.1,1,3.2,1.5l8.8,3.9l-8.6,3.7C402,480.6,395.3,481.9,388.6,481.9z M115.8,473
                                            c2.7,0.5,5.5,0.8,8.3,0.8h264.4c2.6,0,5.1-0.2,7.7-0.7c-14.6-9.7-23.4-26.1-23.4-43.9v-85.5H139.2v85.5
                                            C139.2,446.9,130.4,463.2,115.8,473z"/>
                                    </g>
                                    <g>
                                        <path d="M86.4,481.9h-3.6c-29.1,0-52.7-23.7-52.7-52.8V262.4c0-26.3,18.8-48.2,44.7-52.1c2.2-0.5,4.9-0.7,8-0.7h3.6
                                            c29.1,0,52.8,23.7,52.8,52.8v166.8c0,19.6-10.8,37.5-28.1,46.7c-1.3,0.7-2.6,1.3-4,1.9C100.4,480.6,93.5,481.9,86.4,481.9z
                                                M82.8,217.7c-2.6,0-4.7,0.2-6.6,0.6c-22.1,3.3-38,21.8-38,44.1v166.8c0,24.6,20,44.7,44.6,44.7h3.6c6,0,11.9-1.2,17.4-3.5
                                            c1.1-0.5,2.3-1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7H82.8z"/>
                                    </g>
                                    <g>
                                        <path d="M429.1,481.9h-3.6c-6.7,0-13.2-1.2-19.3-3.7l-0.9-0.3c-1.6-0.7-2.9-1.3-4.1-1.9c-17.5-9.1-28.4-27.1-28.4-46.9V262.4
                                            c0-29.1,23.6-52.8,52.7-52.8h3.6c3.1,0,6,0.3,8.9,0.8c25.4,4.3,43.9,26.2,43.9,52v166.8C481.9,458.3,458.2,481.9,429.1,481.9z
                                                M408,470.3l0.5,0.2c5.4,2.2,11.1,3.3,17,3.3h3.6c24.6,0,44.7-20,44.7-44.7V262.4c0-21.8-15.6-40.3-37.1-44
                                            c-2.5-0.5-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7C406,469.4,407,469.8,408,470.3z"/>
                                    </g>
                                    </g>
                                    </g>
                                </svg>
							</span>
						  </div>
						</div>
					  </div>
					  <div class="seat-wrapper">
						<div class="left-side">
						  <div>
							<span class='seat' data-seat='1-C5'> 
                                <span class="seatlegend">C5</span>
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                              
                                    <g>
                                    <g>
                                    <path class="st0" d="M477.8,262.4v166.8c0,26.9-21.8,48.7-48.7,48.7h-3.6c-6.6,0-12.8-1.3-18.5-3.7c-0.1,0-0.1,0-0.1,0
                                        c-5.7,2.3-11.9,3.6-18.4,3.6H124.2c-6.7,0-13-1.3-18.8-3.7c-5.8,2.4-12.3,3.8-19,3.8h-3.6c-26.9,0-48.6-21.8-48.6-48.7V262.4
                                        c0-24.4,17.9-44.6,41.3-48.1V82.8c0-26.9,21.8-48.7,48.7-48.7h264.4c26.9,0,48.7,21.8,48.7,48.7v131.5
                                        C460.3,218.3,477.8,238.3,477.8,262.4z"/>
                                    </g>
                                    <g>
                                    <g>
                                        <path d="M388.6,478.7H124.2c-5.6,0-11.1-0.9-16.3-2.6l-9-3l8.4-4.5c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7
                                            h-3.6c-2.6,0-4.7,0.2-6.6,0.6l-4.9,1V82.8c0-29.1,23.7-52.8,52.8-52.8h264.4c29.1,0,52.8,23.7,52.8,52.8v136.4l-4.8-0.9
                                            c-2.5-0.4-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7l8.9,4.6l-9.6,2.9
                                            C399,477.9,393.7,478.7,388.6,478.7z M119.4,470.3c1.6,0.2,3.1,0.2,4.7,0.2h264.4c1.3,0,2.6-0.1,4-0.2
                                            c-12.4-9.9-19.7-24.9-19.7-41.2V262.4c0-29.1,23.6-52.8,52.7-52.8h3.6c1.4,0,2.8,0.1,4.1,0.2V82.8c0-24.6-20-44.7-44.7-44.7
                                            H124.2c-24.6,0-44.7,20-44.7,44.7v126.9c1.1-0.1,2.2-0.1,3.3-0.1h3.6c29.1,0,52.8,23.7,52.8,52.8v166.8
                                            C139.2,445.4,131.8,460.4,119.4,470.3z"/>
                                    </g>
                                    <g>
                                        <path d="M388.6,481.9H124.2c-7.1,0-14-1.4-20.3-4.1l-8.7-3.7l8.6-3.8c1.2-0.5,2.3-1.1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5v-93.6
                                            h249.9v93.6c0,16.8,9.2,32,24,39.7c1.1,0.6,2.1,1,3.2,1.5l8.8,3.9l-8.6,3.7C402,480.6,395.3,481.9,388.6,481.9z M115.8,473
                                            c2.7,0.5,5.5,0.8,8.3,0.8h264.4c2.6,0,5.1-0.2,7.7-0.7c-14.6-9.7-23.4-26.1-23.4-43.9v-85.5H139.2v85.5
                                            C139.2,446.9,130.4,463.2,115.8,473z"/>
                                    </g>
                                    <g>
                                        <path d="M86.4,481.9h-3.6c-29.1,0-52.7-23.7-52.7-52.8V262.4c0-26.3,18.8-48.2,44.7-52.1c2.2-0.5,4.9-0.7,8-0.7h3.6
                                            c29.1,0,52.8,23.7,52.8,52.8v166.8c0,19.6-10.8,37.5-28.1,46.7c-1.3,0.7-2.6,1.3-4,1.9C100.4,480.6,93.5,481.9,86.4,481.9z
                                                M82.8,217.7c-2.6,0-4.7,0.2-6.6,0.6c-22.1,3.3-38,21.8-38,44.1v166.8c0,24.6,20,44.7,44.6,44.7h3.6c6,0,11.9-1.2,17.4-3.5
                                            c1.1-0.5,2.3-1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7H82.8z"/>
                                    </g>
                                    <g>
                                        <path d="M429.1,481.9h-3.6c-6.7,0-13.2-1.2-19.3-3.7l-0.9-0.3c-1.6-0.7-2.9-1.3-4.1-1.9c-17.5-9.1-28.4-27.1-28.4-46.9V262.4
                                            c0-29.1,23.6-52.8,52.7-52.8h3.6c3.1,0,6,0.3,8.9,0.8c25.4,4.3,43.9,26.2,43.9,52v166.8C481.9,458.3,458.2,481.9,429.1,481.9z
                                                M408,470.3l0.5,0.2c5.4,2.2,11.1,3.3,17,3.3h3.6c24.6,0,44.7-20,44.7-44.7V262.4c0-21.8-15.6-40.3-37.1-44
                                            c-2.5-0.5-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7C406,469.4,407,469.8,408,470.3z"/>
                                    </g>
                                    </g>
                                    </g>
                                </svg>
							</span>
						  </div>
						  <div>
							<span class='seat' data-seat='1-C6'> 
                                <span class="seatlegend">C6</span>
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                              
                                    <g>
                                    <g>
                                    <path class="st0" d="M477.8,262.4v166.8c0,26.9-21.8,48.7-48.7,48.7h-3.6c-6.6,0-12.8-1.3-18.5-3.7c-0.1,0-0.1,0-0.1,0
                                        c-5.7,2.3-11.9,3.6-18.4,3.6H124.2c-6.7,0-13-1.3-18.8-3.7c-5.8,2.4-12.3,3.8-19,3.8h-3.6c-26.9,0-48.6-21.8-48.6-48.7V262.4
                                        c0-24.4,17.9-44.6,41.3-48.1V82.8c0-26.9,21.8-48.7,48.7-48.7h264.4c26.9,0,48.7,21.8,48.7,48.7v131.5
                                        C460.3,218.3,477.8,238.3,477.8,262.4z"/>
                                    </g>
                                    <g>
                                    <g>
                                        <path d="M388.6,478.7H124.2c-5.6,0-11.1-0.9-16.3-2.6l-9-3l8.4-4.5c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7
                                            h-3.6c-2.6,0-4.7,0.2-6.6,0.6l-4.9,1V82.8c0-29.1,23.7-52.8,52.8-52.8h264.4c29.1,0,52.8,23.7,52.8,52.8v136.4l-4.8-0.9
                                            c-2.5-0.4-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7l8.9,4.6l-9.6,2.9
                                            C399,477.9,393.7,478.7,388.6,478.7z M119.4,470.3c1.6,0.2,3.1,0.2,4.7,0.2h264.4c1.3,0,2.6-0.1,4-0.2
                                            c-12.4-9.9-19.7-24.9-19.7-41.2V262.4c0-29.1,23.6-52.8,52.7-52.8h3.6c1.4,0,2.8,0.1,4.1,0.2V82.8c0-24.6-20-44.7-44.7-44.7
                                            H124.2c-24.6,0-44.7,20-44.7,44.7v126.9c1.1-0.1,2.2-0.1,3.3-0.1h3.6c29.1,0,52.8,23.7,52.8,52.8v166.8
                                            C139.2,445.4,131.8,460.4,119.4,470.3z"/>
                                    </g>
                                    <g>
                                        <path d="M388.6,481.9H124.2c-7.1,0-14-1.4-20.3-4.1l-8.7-3.7l8.6-3.8c1.2-0.5,2.3-1.1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5v-93.6
                                            h249.9v93.6c0,16.8,9.2,32,24,39.7c1.1,0.6,2.1,1,3.2,1.5l8.8,3.9l-8.6,3.7C402,480.6,395.3,481.9,388.6,481.9z M115.8,473
                                            c2.7,0.5,5.5,0.8,8.3,0.8h264.4c2.6,0,5.1-0.2,7.7-0.7c-14.6-9.7-23.4-26.1-23.4-43.9v-85.5H139.2v85.5
                                            C139.2,446.9,130.4,463.2,115.8,473z"/>
                                    </g>
                                    <g>
                                        <path d="M86.4,481.9h-3.6c-29.1,0-52.7-23.7-52.7-52.8V262.4c0-26.3,18.8-48.2,44.7-52.1c2.2-0.5,4.9-0.7,8-0.7h3.6
                                            c29.1,0,52.8,23.7,52.8,52.8v166.8c0,19.6-10.8,37.5-28.1,46.7c-1.3,0.7-2.6,1.3-4,1.9C100.4,480.6,93.5,481.9,86.4,481.9z
                                                M82.8,217.7c-2.6,0-4.7,0.2-6.6,0.6c-22.1,3.3-38,21.8-38,44.1v166.8c0,24.6,20,44.7,44.6,44.7h3.6c6,0,11.9-1.2,17.4-3.5
                                            c1.1-0.5,2.3-1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7H82.8z"/>
                                    </g>
                                    <g>
                                        <path d="M429.1,481.9h-3.6c-6.7,0-13.2-1.2-19.3-3.7l-0.9-0.3c-1.6-0.7-2.9-1.3-4.1-1.9c-17.5-9.1-28.4-27.1-28.4-46.9V262.4
                                            c0-29.1,23.6-52.8,52.7-52.8h3.6c3.1,0,6,0.3,8.9,0.8c25.4,4.3,43.9,26.2,43.9,52v166.8C481.9,458.3,458.2,481.9,429.1,481.9z
                                                M408,470.3l0.5,0.2c5.4,2.2,11.1,3.3,17,3.3h3.6c24.6,0,44.7-20,44.7-44.7V262.4c0-21.8-15.6-40.3-37.1-44
                                            c-2.5-0.5-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7C406,469.4,407,469.8,408,470.3z"/>
                                    </g>
                                    </g>
                                    </g>
                                </svg>
							</span>
						  </div>
						  <div>
							<span class='seat' data-seat='1-C7'> 
                                <span class="seatlegend">C7</span>
                                <?xml version="1.0" encoding="utf-8"?>
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                              
                                    <g>
                                    <g>
                                    <path class="st0" d="M477.8,262.4v166.8c0,26.9-21.8,48.7-48.7,48.7h-3.6c-6.6,0-12.8-1.3-18.5-3.7c-0.1,0-0.1,0-0.1,0
                                        c-5.7,2.3-11.9,3.6-18.4,3.6H124.2c-6.7,0-13-1.3-18.8-3.7c-5.8,2.4-12.3,3.8-19,3.8h-3.6c-26.9,0-48.6-21.8-48.6-48.7V262.4
                                        c0-24.4,17.9-44.6,41.3-48.1V82.8c0-26.9,21.8-48.7,48.7-48.7h264.4c26.9,0,48.7,21.8,48.7,48.7v131.5
                                        C460.3,218.3,477.8,238.3,477.8,262.4z"/>
                                    </g>
                                    <g>
                                    <g>
                                        <path d="M388.6,478.7H124.2c-5.6,0-11.1-0.9-16.3-2.6l-9-3l8.4-4.5c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7
                                            h-3.6c-2.6,0-4.7,0.2-6.6,0.6l-4.9,1V82.8c0-29.1,23.7-52.8,52.8-52.8h264.4c29.1,0,52.8,23.7,52.8,52.8v136.4l-4.8-0.9
                                            c-2.5-0.4-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7l8.9,4.6l-9.6,2.9
                                            C399,477.9,393.7,478.7,388.6,478.7z M119.4,470.3c1.6,0.2,3.1,0.2,4.7,0.2h264.4c1.3,0,2.6-0.1,4-0.2
                                            c-12.4-9.9-19.7-24.9-19.7-41.2V262.4c0-29.1,23.6-52.8,52.7-52.8h3.6c1.4,0,2.8,0.1,4.1,0.2V82.8c0-24.6-20-44.7-44.7-44.7
                                            H124.2c-24.6,0-44.7,20-44.7,44.7v126.9c1.1-0.1,2.2-0.1,3.3-0.1h3.6c29.1,0,52.8,23.7,52.8,52.8v166.8
                                            C139.2,445.4,131.8,460.4,119.4,470.3z"/>
                                    </g>
                                    <g>
                                        <path d="M388.6,481.9H124.2c-7.1,0-14-1.4-20.3-4.1l-8.7-3.7l8.6-3.8c1.2-0.5,2.3-1.1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5v-93.6
                                            h249.9v93.6c0,16.8,9.2,32,24,39.7c1.1,0.6,2.1,1,3.2,1.5l8.8,3.9l-8.6,3.7C402,480.6,395.3,481.9,388.6,481.9z M115.8,473
                                            c2.7,0.5,5.5,0.8,8.3,0.8h264.4c2.6,0,5.1-0.2,7.7-0.7c-14.6-9.7-23.4-26.1-23.4-43.9v-85.5H139.2v85.5
                                            C139.2,446.9,130.4,463.2,115.8,473z"/>
                                    </g>
                                    <g>
                                        <path d="M86.4,481.9h-3.6c-29.1,0-52.7-23.7-52.7-52.8V262.4c0-26.3,18.8-48.2,44.7-52.1c2.2-0.5,4.9-0.7,8-0.7h3.6
                                            c29.1,0,52.8,23.7,52.8,52.8v166.8c0,19.6-10.8,37.5-28.1,46.7c-1.3,0.7-2.6,1.3-4,1.9C100.4,480.6,93.5,481.9,86.4,481.9z
                                                M82.8,217.7c-2.6,0-4.7,0.2-6.6,0.6c-22.1,3.3-38,21.8-38,44.1v166.8c0,24.6,20,44.7,44.6,44.7h3.6c6,0,11.9-1.2,17.4-3.5
                                            c1.1-0.5,2.3-1,3.4-1.6c14.7-7.8,23.8-23,23.8-39.5V262.4c0-24.6-20-44.7-44.7-44.7H82.8z"/>
                                    </g>
                                    <g>
                                        <path d="M429.1,481.9h-3.6c-6.7,0-13.2-1.2-19.3-3.7l-0.9-0.3c-1.6-0.7-2.9-1.3-4.1-1.9c-17.5-9.1-28.4-27.1-28.4-46.9V262.4
                                            c0-29.1,23.6-52.8,52.7-52.8h3.6c3.1,0,6,0.3,8.9,0.8c25.4,4.3,43.9,26.2,43.9,52v166.8C481.9,458.3,458.2,481.9,429.1,481.9z
                                                M408,470.3l0.5,0.2c5.4,2.2,11.1,3.3,17,3.3h3.6c24.6,0,44.7-20,44.7-44.7V262.4c0-21.8-15.6-40.3-37.1-44
                                            c-2.5-0.5-4.9-0.7-7.5-0.7h-3.6c-24.6,0-44.6,20-44.6,44.7v166.8c0,16.8,9.2,32,24,39.7C406,469.4,407,469.8,408,470.3z"/>
                                    </g>
                                    </g>
                                    </g>
                                </svg>
							</span>
						  </div>
						</div>
					  </div>
                </div>
                @endforeach
                <div class="seat-for-reserved">
                    <div class="seat-condition available-seat">
                        <span class="seat"><span></span></span>
                        <p>@lang('Available Seats')</p>
                    </div>
                    <div class="seat-condition selected-by-you">
                        <span class="seat"><span></span></span>
                        <p>@lang('Selected by You')</p>
                    </div>
                    <div class="seat-condition selected-by-gents">
                        <div class="seat"><span></span></div>
                        <p>@lang('Booked by Gents')</p>
                    </div>
                    <div class="seat-condition selected-by-ladies">
                        <div class="seat"><span></span></div>
                        <p>@lang('Booked by Ladies')</p>
                    </div>
                    <div class="seat-condition selected-by-others">
                        <div class="seat"><span></span></div>
                        <p>@lang('Booked by Others')</p>
                    </div>
                </div>
                <div>
                    <p>
                        <strong>Note :</strong> 7kg free accompanied hand bag/ load. 
                        Other loads chargeable at the point of loading with the manager or his representative.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- confirmation modal --}}
<div class="modal fade" id="bookConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 100000">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header model-header-flex">
                <h5 class="modal-title model-title-flex-1"> @lang('Confirm Booking')</h5>
                <button type="button" class="w-auto btn--close" data-bs-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <strong class="text-dark">@lang('Are you sure to book these seats?')</strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger w-auto btn--sm px-3" data-dismiss="modal">
                    @lang('Close')
                </button>
                <button type="submit" class="btn btn--success btn--sm w-auto" id="btnBookConfirm">@lang("Confirm")
                </button>
            </div>
        </div>
    </div>
</div>

{{-- alert modal --}}
<div class="modal fade" id="alertModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:100000">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header model-header-flex">
                <h5 class="modal-title model-title-flex-1"> @lang('Alert Message')</h5>
                <button type="button" class="w-auto btn--close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <strong>
                    <p class="error-message text-danger"></p>
                </strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger w-auto btn--sm px-3" data-bs-dismiss="modal">
                    @lang('Continue')
                </button>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script>
    (function($) {
        "use strict";

        var date_of_journey = '{{ Session::get('
        date_of_journey ') }}';
        var pickup = '{{ Session::get('
        pickup ') }}';
        var destination = '{{ Session::get('
        destination ') }}';

        if (date_of_journey && pickup && destination) {
            showBookedSeat();
        }
        $(".seat-wrapper .seat").on("click", function () {
            if (!$(this).parent().hasClass("disabled")) $(this).toggleClass("selected");
        });
        // Seat Expand
        $(".select-seat-btn").on("click", function () {
            $(this)
                .closest(".ticket-item")
                .children(".seat-plan-wrapper")
                .toggleClass("selected");
        });
        //reset all seats
        function reset() {
            $('.seat-wrapper .seat').removeClass('selected');
            $('.seat-wrapper .seat').parent().removeClass('seat-condition selected-by-ladies selected-by-gents selected-by-others disabled');
            $('.selected-seat-details').html('');
        }

        //click on seat
		var no_of_seat = 1;
        $('.seat-wrapper .seat').on('click', function() {
            var pickupPoint = $('select[name="pickup_point"]').val();
            var droppingPoing = $('select[name="dropping_point"]').val();
			// alert(no_of_seat);
        //     console.log({
        //         'no_of_seat':no_of_seat,
        //         'pasenger_no':parseInt($('#passenger_no').val())
        // });
			
			if (no_of_seat > parseInt($('#passenger_no').val())) {
				no_of_seat = 1;
                // $(this).removeClass('selected');
               
				$('.seat-wrapper .seat').removeClass('selected');
                notify('error', "@lang('Invalid number of seat')")
            }
			else{
				if (pickupPoint && droppingPoing) {
					selectSeat();
				}
				else {
					$(this).removeClass('selected');
					
					notify('error', "@lang('Please insure pickup point and dropping point  before select any seat')")

				}
			}
        });

        //select and booked seat
        function selectSeat() {
           
			no_of_seat++;
            let selectedSeats = $('.seat.selected');
            let seatDetails = ``;
            let price = $('input[name=price]').val();
            let subtotal = 0;
            let currency = '{{ __($general->cur_text) }}';
            let seats = '';
			
			// no_of_seat = selectedSeats.length;
			
            if (selectedSeats.length > 0) {
				// no_of_seat++;
                $('.booked-seat-details').removeClass('d-none');
                $.each(selectedSeats, function(i, value) {
                    seats += $(value).data('seat') + ',';
                    seatDetails += `<span class="list-group-item d-flex justify-content-between">${$(value).data('seat')} <span>${price} ${currency}</span></span>`;
					@if( Session::get('date_of_return'))
                    seatDetails += `<span class="list-group-item d-flex justify-content-between">Return <span>${price} ${currency}</span></span>`;
					@endif
                    subtotal = subtotal + parseFloat(price);
					@if( Session::get('date_of_return'))
						subtotal = subtotal * 2;
					@endif
                });

                $('input[name=seats]').val(seats);
                $('.selected-seat-details').html(seatDetails);
                $('.selected-seat-details').append(`<span class="list-group-item d-flex justify-content-between">@lang('Sub total')<span>${subtotal} ${currency}</span></span>`);
            } else {
                $('.selected-seat-details').html('');
                $('.booked-seat-details').addClass('d-none');
            }
        }

        //on change date, pickup point and destination point show available seats
        $(document).on('change', 'select[name="pickup_point"], select[name="dropping_point"], input[name="date_of_journey"]', function(e) {
            showBookedSeat();
        });
		
		$( document ).ready(function() {
			showBookedSeat();
		});

        //booked seat
        function showBookedSeat() {
            reset();
            var date = $('input[name="date_of_journey"]').val();
            var sourceId = $('select[name="pickup_point"]').find("option:selected").val();
            var destinationId = $('select[name="dropping_point"]').find("option:selected").val();

            if (sourceId == destinationId && destinationId != '') {
                notify('error',"@lang('Source Point and Destination Point Must Not Be Same')");
                $('select[name="dropping_point"]').val('').select2();
                return false;
            } else if (sourceId != destinationId) {

                var routeId = '{{ $trip->route->id }}';
                var fleetTypeId = '{{ $trip->fleetType->id }}';

                if (sourceId && destinationId) {
                    getprice(routeId, fleetTypeId, sourceId, destinationId, date)
                }
            }
        }

        // check price, booked seat etc
        function getprice(routeId, fleetTypeId, sourceId, destinationId, date) {
            var data = {
                "trip_id": '{{ $trip->id }}',
                "vehicle_route_id": routeId,
                "fleet_type_id": fleetTypeId,
                "source_id": sourceId,
                "destination_id": destinationId,
                "date": date,
            }
            $.ajax({
                type: "get",
                url: "{{ route('ticket.get-price') }}",
                data: data,
                success: function(response) {

                    if (response.error) {
                        var modal = $('#alertModal');
                        modal.find('.error-message').text(response.error);
                        modal.modal('show');
                        $('select[name="pickup_point"]').val('');
                        $('select[name="dropping_point"]').val('');
                    } else {
                        var stoppages = response.stoppages;

                        var reqSource = response.reqSource;
                        var reqDestination = response.reqDestination;

                        reqSource = stoppages.indexOf(reqSource.toString());
                        reqDestination = stoppages.indexOf(reqDestination.toString());

                        if (response.reverse == true) {
                            $.each(response.bookedSeats, function(i, v) {
                                var bookedSource = v.pickup_point; //Booked
                                var bookedDestination = v.dropping_point; //Booked

                                bookedSource = stoppages.indexOf(bookedSource.toString());
                                bookedDestination = stoppages.indexOf(bookedDestination.toString());

                                if (reqDestination >= bookedSource || reqSource <= bookedDestination) {
                                    $.each(v.seats, function(index, val) {
                                        if(v.gender == 1){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().removeClass('seat-condition selected-by-gents disabled');
                                        }
                                         if(v.gender == 2){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().removeClass('seat-condition selected-by-ladies disabled');
                                        }
                                        if(v.gender == 3){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().removeClass('seat-condition selected-by-others disabled');
                                        }
                                    });
                                } else {
                                    $.each(v.seats, function(index, val) {
                                        if(v.gender == 1){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().addClass('seat-condition selected-by-gents disabled');
                                        }
                                        if(v.gender == 2){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().addClass('seat-condition selected-by-ladies disabled');
                                        }
                                        if(v.gender == 3){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().addClass('seat-condition selected-by-others disabled');
                                        }
                                    });
                                }
                            });
                        } else {
                            $.each(response.bookedSeats, function(i, v) {
                                console.log(i, v);
                                var bookedSource = v.pickup_point; //Booked
                                var bookedDestination = v.dropping_point; //Booked

                                bookedSource = stoppages.indexOf(bookedSource.toString());
                                bookedDestination = stoppages.indexOf(bookedDestination.toString());


                                if (reqDestination <= bookedSource || reqSource >= bookedDestination) {
                                    $.each(v.seats, function(index, val) {
                                        if(v.gender == 1){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().removeClass('seat-condition selected-by-gents disabled');
                                        }
                                         if(v.gender == 2){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().removeClass('seat-condition selected-by-ladies disabled');
                                        }
                                        if(v.gender == 3){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().removeClass('seat-condition selected-by-others disabled');
                                        }
                                    });
                                } else {
                                    $.each(v.seats, function(index, val) {
                                        if(v.gender == 1){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().addClass('seat-condition selected-by-gents disabled');
                                        }
                                        if(v.gender == 2){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().addClass('seat-condition selected-by-ladies disabled');
                                        }
                                        if(v.gender == 3){
                                            $(`.seat-wrapper .seat[data-seat="${val}"]`).parent().addClass('seat-condition selected-by-others disabled');
                                        }
                                    });
                                }
                            });
                        }

                        if (response.price.error) {
                            var modal = $('#alertModal');
                            modal.find('.error-message').text(response.price.error);
                            modal.modal('show');
                        } else {
                            $('input[name=price]').val(response.price);
                        }
                    }
                }
            });
        }

        //booking form submit
        $('#bookingForm').on('submit', function(e) {
            e.preventDefault();
            let selectedSeats = $('.seat.selected');
            if (selectedSeats.length > 0) {
                var modal = $('#bookConfirm');
                modal.modal('show');
            } else {
                notify('error', 'Select at least one seat.');
            }
        });

        //confirmation modal
        $(document).on('click', '#btnBookConfirm', function(e) {
            var modal = $('#bookConfirm');
            modal.modal('hide');
            document.getElementById("bookingForm").submit();
        });

    })(jQuery);
</script>
@endpush
