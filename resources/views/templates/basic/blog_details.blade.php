@extends($activeTemplate.'layouts.newfrontend')
@section('content')
<section class="section-90 section-md-111 text-left">
	<div class="shell">
	  <div class="range range-xs-center range-lg-right">
		<div class="cell-sm-10 cell-md-8">
		  <h2 class="text-bold">{{ __(@$blog->data_values->title) }}</h2>
		  <div class="offset-top-15">
			<ul class="list list-inline list-inline-dots list-inline-8 list-inline-0 text-gray">
			  <li class="offset-top-0"><span style="position: relative; top: -1px;" class="icon icon-xxs mdi mdi-calendar text-gray text-middle"></span><span class="inset-left-10">{{ diffInDays($blog->created_at) }} days ago</span></li>
			  {{-- <li><span>by <a href="team-member-profile.html" class="text-orange-peel">Michael Ford</a></span></li> --}}
			</ul>
		  </div>
		  <div class="offset-top-25 offset-md-top-40">
			<img src="{{ getImage('assets/images/frontend/blog/'.@$blog->data_values->image) }}" alt="{{ __(@$blog->data_values->title) }}">

			</div>
		  <div class="offset-top-30">
			<p class="text-gray-darker">
				@php
						
							
							echo stripStyle(@$blog->data_values->description);
						@endphp
			</p>
		  </div>
		  {{-- <div class="offset-top-20">
			<p class="text-gray-darker">However, like many customers, we’ve experienced inconsistency with the onboard Wi-Fi on our trips: sometimes it works perfectly fine, but on other occasions there’s lag. We’ve also had to deal with buses where the advertised Wi-Fi didn’t work at all, which forced us to spend time admiring the scenery.</p>
		  </div>
		  <div class="offset-top-20"><img src="images/blog/post-08-770x420.jpg" width="770" height="420" alt="" class="img-responsive center-block"></div>
		  <div class="offset-top-30">
			<p class="text-gray-darker">You might not always have an excellent Wi-Fi experience on your bus trips in the US but at least the odds are in your favour that the power outlets can charge up your electronic devices with nearly 75% of our users telling us that the power outlets were properly working.</p>
		  </div>
		  <div class="offset-top-10">
			<p class="text-gray-darker">We surveyed thousands of our customers and those who has recently traveled with major bus operators in the United States. They answered a two-question survey that allowed us to understand their perception of Wi-Fi quality provided on the bus trip and as well as the availability of functional power sockets.</p>
		  </div> --}}
		  <div class="offset-top-30">
			<div class="pull-sm-left">
			  <div class="tags group group-sm"><a href="#" class="btn-xs btn-tag btn btn-ripe-lemon">Travel</a>
				{{-- <a href="#" class="btn-xs btn-tag btn btn-ripe-lemon">USA</a> --}}
			</div>
			</div>
			<div class="pull-sm-right inset-sm-right-6">
			  <div class="reveal-inline-block">
				<p class="text-dark">Share:</p>
			  </div>
			  <ul class="list-inline list-inline-2 reveal-inline-block offset-top-20 offset-sm-top-0 inset-left-15">
				<li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-facebook"></a></li>
				<li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-twitter"></a></li>
				<li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-google-plus"></a></li>
				<li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-instagram"></a></li>
				<li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-rss"></a></li>
			  </ul>
			</div>
			<div class="clearfix"></div>
		  </div>
		  
		 
		 
		 
		 
		</div>
		<div class="cell-sm-10 cell-md-4 offset-top-60 offset-md-top-0">
		  <div class="inset-md-left-30">
			<!-- Aside-->
			<aside class="text-left">
			  <!-- Search in Blog-->
			  <div>
				<h5 class="text-bold">Latest Posts</h5>
			  </div>
			  <div class="offset-top-6">
				<div class="text-subline"></div>
			  </div>
			  @foreach($latestPost as $latest)
			  <div class="offset-top-20 text-left">
				<div class="reveal-inline-block">
				  <!-- Unit-->
				  <div class="unit unit-xs unit-xs-horizontal unit-spacing-19">
					<div class="unit-left"><img src="{{ getImage('assets/images/frontend/blog/thumb_'.$latest->data_values->image) }}" width="100" height="100" alt="" class="img-responsive"></div>
					<div class="unit-body offset-top-10 offset-xs-top-0 offset-sm-top-4">
					  <h6 class="text-bold"><a href="{{ route('blog.details', [$latest->id, slug($latest->data_values->title)]) }}" class="text-primary">{{ __(@$latest->data_values->title) }}</a></h6>
					  <div class="offset-top-10">
						<ul class="list list-inline list-inline-8 list-inline-0 text-gray">
						  <li><span class="icon icon-xxs mdi mdi-calendar text-gray text-middle"></span><span class="inset-left-10 text-middle">{{ diffInDays($latest->created_at) }} days ago</span></li>
						  {{-- <li class="offset-top-0"><span>by <a href="team-member-profile.html" class="text-orange-peel">Michael Ford</a></span></li> --}}
						</ul>
					  </div>
					  <div class="offset-top-20">
						<div class="tags group group-sm"><a href="#" class="btn-xs btn-tag btn btn-ripe-lemon">Travel</a></div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			  @endforeach
			  <!-- Archive-->
			  {{-- <div class="offset-top-30 offset-md-top-60">
				<h5 class="text-bold">Archive</h5>
			  </div>
			  <div class="offset-top-6">
				<div class="text-subline"></div>
			  </div>
			  <div class="offset-top-15 offset-md-top-20">
				<div class="range range-xs-center">
				  <div class="cell-xs-6">
					<div class="inset-xs-left-8">
					  <!-- List Marked-->
					  <ul class="list list-marked list-marked-icon text-dark">
						<li><a href="#" class="text-primary">Jan 2016</a></li>
						<li><a href="#" class="text-primary">Mar 2016</a></li>
						<li><a href="#" class="text-primary">May 2016</a></li>
						<li><a href="#" class="text-primary">Jul 2016</a></li>
					  </ul>
					</div>
				  </div>
				  <div class="cell-xs-6 offset-top-10 offset-xs-top-0 inset-xs-left-8">
					<!-- List Marked-->
					<ul class="list list-marked list-marked-icon text-dark">
					  <li><a href="#" class="text-primary">Feb 2016</a></li>
					  <li><a href="#" class="text-primary">Apr 2016</a></li>
					  <li><a href="#" class="text-primary">Jun 2016</a></li>
					</ul>
				  </div>
				</div>
			  </div> --}}
			  <!-- About-->
			  {{-- <div class="offset-top-30 offset-md-top-60">
				<h5 class="text-bold">About</h5>
			  </div>
			  <div class="offset-top-6">
				<div class="text-subline"></div>
			  </div>
			  <div class="offset-top-15 offset-md-top-20">
				<p>BusExpress is the leading go-to website for booking inter-city bus ticket online. Our booking system allows travelers to search and book bus tickets for over a hundred bus companies throughout the United States, Mexico, Canada and Europe.</p>
			  </div>
			  <div class="offset-top-15"><a href="our-history.html" class="text-bold text-chathams-blue">Read More</a></div> --}}
			  <!-- Gallery-->
			  {{-- <div class="offset-top-30 offset-md-top-60">
				<h5 class="text-bold">Gallery</h5>
			  </div>
			  <div class="offset-top-6">
				<div class="text-subline"></div>
			  </div>
			  <div class="offset-top-15 offset-md-top-20">
				<div data-photo-swipe-gallery="gallery" class="range range-xs-center section-gallery-lg-column-offset">
				  <div class="cell-xs-4 cell-md-6 inset-lg-left-13 inset-lg-right-13">
					<div class="inset-left-30 inset-right-30 inset-xs-left-0 inset-xs-right-0">
										<!-- Thumbnail Rayen--><a class="thumbnail-rayen" data-photo-swipe-item="" data-size="1170x780" href="images/portfolio/gallery-09-1170x780_original.jpg"><span class="figure"><img width="160" height="160" src="images/portfolio/gallery-09-160x160.jpg" alt=""><span class="figcaption"><span class="icon icon-xs fa-search-plus"></span></span></span></a>
					</div>
				  </div>
				  <div class="cell-xs-4 cell-md-6 inset-lg-left-13 inset-lg-right-13 offset-top-20 offset-xs-top-0">
					<div class="inset-left-30 inset-right-30 inset-xs-left-0 inset-xs-right-0">
										<!-- Thumbnail Rayen--><a class="thumbnail-rayen" data-photo-swipe-item="" data-size="850x850" href="images/portfolio/gallery-02-850x850_original.jpg"><span class="figure"><img width="160" height="160" src="images/portfolio/gallery-02-160x160.jpg" alt=""><span class="figcaption"><span class="icon icon-xs fa-search-plus"></span></span></span></a>
					</div>
				  </div>
				  <div class="cell-xs-4 cell-md-6 inset-lg-left-13 inset-lg-right-13 offset-top-20 offset-xs-top-0 offset-md-top-20">
					<div class="inset-left-30 inset-right-30 inset-xs-left-0 inset-xs-right-0">
										<!-- Thumbnail Rayen--><a class="thumbnail-rayen" data-photo-swipe-item="" data-size="1170x777" href="images/portfolio/gallery-04-1170x777_original.jpg"><span class="figure"><img width="160" height="160" src="images/portfolio/gallery-04-160x160.jpg" alt=""><span class="figcaption"><span class="icon icon-xs fa-search-plus"></span></span></span></a>
					</div>
				  </div>
				  <div class="cell-xs-4 cell-md-6 inset-lg-left-13 inset-lg-right-13 offset-top-20">
					<div class="inset-left-30 inset-right-30 inset-xs-left-0 inset-xs-right-0">
										<!-- Thumbnail Rayen--><a class="thumbnail-rayen" data-photo-swipe-item="" data-size="1170x807" href="images/portfolio/gallery-08-1170x807_original.jpg"><span class="figure"><img width="160" height="160" src="images/portfolio/gallery-08-160x160.jpg" alt=""><span class="figcaption"><span class="icon icon-xs fa-search-plus"></span></span></span></a>
					</div>
				  </div>
				  <div class="cell-xs-4 cell-md-6 inset-lg-left-13 inset-lg-right-13 offset-top-20">
					<div class="inset-left-30 inset-right-30 inset-xs-left-0 inset-xs-right-0">
										<!-- Thumbnail Rayen--><a class="thumbnail-rayen" data-photo-swipe-item="" data-size="1170x779" href="images/portfolio/gallery-10-1170x779_original.jpg"><span class="figure"><img width="160" height="160" src="images/portfolio/gallery-10-160x160.jpg" alt=""><span class="figcaption"><span class="icon icon-xs fa-search-plus"></span></span></span></a>
					</div>
				  </div>
				  <div class="cell-xs-4 cell-md-6 inset-lg-left-13 inset-lg-right-13 offset-top-20">
					<div class="inset-left-30 inset-right-30 inset-xs-left-0 inset-xs-right-0">
										<!-- Thumbnail Rayen--><a class="thumbnail-rayen" data-photo-swipe-item="" data-size="1170x780" href="images/portfolio/gallery-11-1170x780_original.jpg"><span class="figure"><img width="160" height="160" src="images/portfolio/gallery-11-160x160.jpg" alt=""><span class="figcaption"><span class="icon icon-xs fa-search-plus"></span></span></span></a>
					</div>
				  </div>
				</div>
			  </div> --}}
			  <!-- Categories-->
			  {{-- <div class="offset-top-30 offset-md-top-60">
				<h5 class="text-bold">Categories</h5>
			  </div> --}}
			  {{-- <div class="offset-top-6">
				<div class="text-subline"></div>
			  </div> --}}
			  {{-- <div class="offset-top-15 offset-md-top-20">
				<div class="inset-xs-left-8">
				  <!-- List Marked-->
				  <ul class="list list-marked list-marked-icon text-dark">
					<li><a href="#" class="text-primary">Travel</a></li>
					<li><a href="#" class="text-primary">Buses</a></li>
					<li><a href="#" class="text-primary">USA</a></li>
					<li><a href="#" class="text-primary">Passengers</a></li>
				  </ul>
				</div>
			  </div> --}}
			  {{-- <div class="offset-top-30 offset-md-top-60">
				<!-- Facebook standart widget-->
				<div>
				  <div class="fb-root fb-widget">
					<div class="fb-page-responsive">
					  <div data-href="https://www.facebook.com/TemplateMonster" data-tabs="timeline" data-height="500" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" class="fb-page">
						<div class="fb-xfbml-parse-ignore">
						  <blockquote cite="https://www.facebook.com/TemplateMonster"><a href="https://www.facebook.com/TemplateMonster">TemplateMonster</a></blockquote>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </div> --}}
			  {{-- <div class="offset-top-30 offset-md-top-60"><a href="#"><img src="images/pages/banner-01-340x340.jpg" width="340" height="340" alt="" class="img-responsive center-block"></a></div> --}}
			</aside>
		  </div>
		</div>
	  </div>
	</div>
  </section>



@endsection


@push('fbComment')
	@php echo loadFbComment() @endphp
@endpush
