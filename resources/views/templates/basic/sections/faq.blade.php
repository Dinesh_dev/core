@php
$faqContent = getContent('faq.content', true);
$faqElements = getContent('faq.element', false, null, true);
@endphp

 <!-- FAQ-->
 <section class="section-90 section-md-111 text-left">
    <div class="shell">
      <h2 class="text-bold text-teft text-sm-left">FAQs</h2>
      <hr class="divider hr-sm-left-0 bg-chathams-blue">
      <p>UNIQUE Mass Transport Ltd an interstates transport company and is also a courier and logistics services company headquartered in Lagos, Nigeria. Incorporated in 2022, it is a subsidiary of the Unique Auto place Ltd.  an intelligence and Unique management company based in Nigeria. The company has service in different States. Abuja Portharcourt, Aba, Enugu, Umuahia etc.</p>

      <div class="range range-xs-center range-sm-left offset-top-30 offset-md-top-60">
        <div class="cell-sm-10 cell-lg-8">
          <!-- Classic Accordion-->
          <div data-type="accordion" class="responsive-tabs responsive-tabs-classic">
            <ul data-group="accordion-arrow-left" class="resp-tabs-list accordion-arrow-left">
              <li>Who is the owner of Unique Mass ?</li>
              <li>Unique Mass Transport &  Logistics</li>
              <li>Services?</li>
              <li>Headquarters	Lagos, Nigeria.</li>
              <li>Key people</li>

            </ul>
            <div data-group="accordion-arrow-left" class="resp-tabs-container accordion-arrow-left">
              <div>
                <p>
                    Chief Ndubuisi David Acho. 
                </p>
              </div>
              <div>
                <p>Type - 	Private</p>
              </div>
              <div>
                <p>
                    Interstates transport,. Charted service,	E-commerce logistics
                </p>
              </div>
              <div>
                <p>
                    No. 17. Balogun Street Anifowoshe Ikeja Lagos.
                </p>
              </div>
              <div>
                <p>
                    Chief Ndubuisi David Acho.  (CEO).<h6>Managers:</h6>
                    Abuja - Mr. George. +2348139342007<br>
                    
                    Lagos - Mr. Okechukwu. +2348023199735. <br>
                    
                    South East (Portharcourt) - 
                    Emeka. +2348065339110<br>
                    Chioma - (Account) - +2348121219774<br>
                    
                    Mr. Austin (Admin) - +2348033980593<br>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
 



{{--
<section class="faq-section padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-8">
                <div class="section-header text-center">
                    <h2 class="title">{{ __(@$faqContent->data_values->heading) }}</h2>
                    <p>{{ __(@$faqContent->data_values->sub_heading) }}</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="faq-wrapper">
                    @foreach ($faqElements as $item)
                    @if($loop->odd)
                    <div class="faq-item">
                        <div class="faq-title">
                            <span class="icon"></span>
                            <h5 class="title">{{ __(@$item->data_values->question) }}</h5>
                        </div>
                        <div class="faq-content">
                            <p>@php
                                echo @$item->data_values->answer
                                @endphp</p>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            <div class="col-lg-6">
                <div class="faq-wrapper">
                    @foreach ($faqElements as $item)
                    @if($loop->even)
                    <div class="faq-item">
                        <div class="faq-title">
                            <span class="icon"></span>
                            <h5 class="title">{{ __(@$item->data_values->question) }}</h5>
                        </div>
                        <div class="faq-content">
                            <p>@php
                                echo @$item->data_values->answer
                                @endphp</p>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
--}}
