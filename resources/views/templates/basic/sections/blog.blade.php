@php
    $blogContent = getContent('blog.content', true);
    $blogElements = getContent('blog.element', false, 4);
@endphp

<!-- Blog Section Starts Here -->
<section style="background:aliceblue" data-aos="fade-in" data-aos-duration="2000" class="tile blog-section padding-top-sec padding-bottom-sec">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-10">
                <div class="section-header text-center">
                    <h2 class="title title-underline">{{ __(@$blogContent->data_values->heading) }}</h2>
                    <p>{{ __(@$blogContent->data_values->sub_heading) }}</p>
                </div>
            </div>
        </div>
        <div class="row  g-4">
            @foreach($blogElements as $item)
            <div class="col-md-6 col-sm-10">
                <div class="post-item" style="margin-top:30px;position:relative">
                    <div class="post-thumb">
                        <img src="{{ getImage('assets/images/frontend/blog/thumb_'. $item->data_values->image) }}" alt="blog">
                    </div>
                    <div class="post-content post-content-two blogele">
                        <div style="text-align:left">
                            <span class="blogtag">Travel</span>
                        </div>
                        <div>
                            <h4 class="title" style="text-align:left;color:white !important;"><a href="{{ route('blog.details', [$item->id, slug($item->data_values->title)]) }}">{{ __(@$item->data_values->title) }}</a></h4>

                            <ul class="post-meta" style="padding:0px;">
                                <li>
                                    <span class="date"><i class="fa fa-calendar"></i>{{ showDateTime($item->created_at, 'd M Y') }}</span>
                                </li>
                            </ul>
                        </div>
                        
                        <!-- <p style="text-align:left">{{ __(shortDescription(strip_tags($item->data_values->description))) }}</p> -->
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @if(sizeof($blogElements) > 4)
        <div style="display:flex;justify-content:center;margin-top:60px">
          <a href="{{route('blog')}}" class="btn-new btn-default-new" style="border:1px solid #dddd">View All News</a>
        </div>
        @endif
    </div>
</section>
<!-- Blog Section Ends Here -->
