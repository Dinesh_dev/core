@php
    $blogContent = getContent('blog.content', true);
    $blogElements = getContent('blog.element', false, 6);
@endphp

<section class="section-90 section-md-111 bg-zircon">
    <div class="shell">
      <h2 class="text-bold">Popular Bus Tickets</h2>
      <hr class="divider bg-chathams-blue">
      <p>Have a look at our popular reason. why you should choose you bus. Just choose a Bus and get a ticket for your great journey.</p>

      <div class="range offset-top-60">
        @foreach($top_trips as $item)
        <div class="cell-xs-10 cell-sm-6 cell-md-4 offset-top-30 ">
          <!-- Post Ticket--><a href="{{ route('ticket.seats', [$item->id, slug($item->title)]) }}" class="post-ticket">
            <div class="post-ticket-header">
              <img src="{{ env('APP_URL')}}/assets/vehicles/images/display/{{ $item->image }}" alt="" class="img-responsive" width="370" height="280">
              <div class="post-ticket-price-svg-wrap">
                <div class="post-ticket-price-svg"><svg viewBox="0 0 140 36"><path d="m 0.01190457,18.011904 0,-17.99999966 70.00000343,0 69.999992,0 0,5.50000006 c 0,4.157048 -0.35675,5.4999996 -1.46104,5.4999996 -2.34992,0 -5.53896,4.138795 -5.53896,7.188561 0,3.025049 3.05738,6.811439 5.5,6.811439 1.15152,0 1.5,1.277778 1.5,5.5 l 0,5.5 -69.999992,0 -70.00000343,0 0,-18 z"></path></svg>
                  <div class="post-ticket-price text-bold text-shark"><span>{{ showAmount($item->price) }}</span><span>&nbsp;{{ __($general->cur_sym) }}</span><span></span></div>
                </div>
              </div>
            </div>
            <div class="post-ticket-body text-left">
              <!-- List Inline-->
              <div>
                <ul class="group-xs list list-inline list-inline-icon list-inline-icon-type-1 list-inline-icon-primary">
                  <li class="h5 text-bold text-primary">{{$item->start_from}}</li>
                  <li class="h5 text-bold text-primary">{{$item->end_to}}</li>
                </ul>
              </div>
              <div class="offset-top-10" style="display: flex;justify-content:space-between;align-items:center;">
                <div class="post-meta text-gray">{{$item->time}}</div>
                <button class="btn btn-default">Book Ticket</button>      
              </div>
            </div>
          </a>
        </div>
        @endforeach
      </div>
      @if(sizeof($top_trips) > 6)
      <div class="offset-top-60"><a href="{{route('destination')}}" class="btn btn-default">View All Destinations</a></div>
     @endif
    </div>
  </section>

<!-- Blog Section Starts Here -->
{{-- <section style="background-color:aliceblue" data-aos="fade-in" data-aos-duration="2000"  class="tile blog-section padding-top-sec padding-bottom-sec">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-10">
                <div class="section-header text-center">
                    <h2 class="title title-underline">Popular Bus Tickets</h2>
                    <p>Have a look at our popular reason. why you should choose you bus. Just choose a Bus and get a ticket for your great journey.</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center g-4">
            
            @foreach($top_trips as $item)
            <div class="col-lg-4 col-md-6 col-sm-10"  style="margin-top:20px;">
                <div class="post-item">
                    <div class="post-thumb">
                        <img src="{{ env('APP_URL')}}/assets/vehicles/images/display/{{ $item->assignedVehicle->vehicle->image }}" alt="blog">
                    </div>
                    <div class="post-content" style="background-color:#fff;position:relative !important">
                        <ul class="post-meta" style="padding:0px !important">
                            <li>
                                <span class="date"><i class="las la-calendar-check"></i>{{ showDateTime($item->created_at, 'd M Y') }}</span>
                            </li>
                        </ul>
                        <h4 class="title"><a href="{{ route('search') }}?pickup={{ $item->start_from }}&destination={{ $item->end_to }}">{{ $item->title }}</a></h4>
                        <p>Vehicle Name : {{ $item->assignedVehicle->vehicle->nick_name }}</p>
							          <p>Total Bookings : {{ $item->booked_tickets_count }}</p>
						<br>
						<div class="text-center">
							<a href="{{ route('search') }}?pickup={{ $item->start_from }}&destination={{ $item->end_to }}" class="cmn--btn  bt-blue" style="display:inline-block;">@lang('Buy Tickets')</a>
						</div>
                    </div>
                </div>
            </div>
            @endforeach
           
        </div>
    </div>
</section> --}}

<section data-aos="fade-in" class="tile" style='position:relative;background: url({{ getImage("assets/images/frontend/breadcrumb/61f14e10b48871643204112.jpg", "1920x1288") }}) center;height:400px' >
     <div style="display:flex;flex-direction:column;justify-content:center;align-items:center;height:100%">
             <h2 class="text-bold text-white">Chatter Service: Bus Rental with Driver</h2>
             <a href="{{ route('ticket') }}" class="btn btn-primary" style="margin-top:25px">Book Now</a>
     </div>   

</section>
<!-- Blog Section Ends Here -->
