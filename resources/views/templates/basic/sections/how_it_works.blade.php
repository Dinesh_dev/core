@php
    $hwContent = getContent('how_it_works.content', true);
    $hwClements = getContent('how_it_works.element', false,null, true);
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/templates/basic/css/flexgridsystem.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/basic/css/divider.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/basic/css/icon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/templates/basic/css/mdiicons.css')}}">
    <style>
        .box-icon{
            display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
        }

        .offset-top-30 {
  margin-top: 30px; }
  .offset-top-10 {
  margin-top: 10px; }

  .box-icon .icon:hover:after, .box-icon .icon:focus:after {
    top: -5px;
    right: -5px;
    bottom: -5px;
    left: -5px;
    border-color: #18377a;
}

.box-icon .icon:after {
    transition: .3s all ease;
}
.icon-outlined:after {
    position: absolute;
    top: -10px;
    right: -10px;
    bottom: -10px;
    left: -10px;
    content: '';
    border: 1px solid #c2c2c2;
    will-change: transform;
}
.icon-outlined, .icon-outlined:after {
    border-radius: 50%;
}
    </style>

@endpush
<section data-aos="fade-in" data-aos-duration="2000" class="tile section-90 section-md-111" style="padding-bottom:30px !important">
          <div class="shell">
            <h2 class="text-bold " style="text-align:center">{{ __(@$hwContent->data_values->heading) }}</h2>
           
            <div class="range range-xs-center offset-top-69">
                        
                        <div class="col-md-4" >
                           <div class="box-icon">
                               <span class="icon icon-xlg icon-circle icon-outlined icon-chathams-primary-filled ">
                               <svg fill="#fff" xmlns="http://www.w3.org/2000/svg" height="48" width="48"><path d="M10 37.8v2.7q0 .65-.425 1.075Q9.15 42 8.5 42h-1q-.65 0-1.075-.425Q6 41.15 6 40.5V24.3l4.25-12.8q.15-.7.775-1.1.625-.4 1.375-.4h6.35V6.25h10.6V10h6.25q.75 0 1.375.4t.775 1.1L42 24.3v16.2q0 .65-.425 1.075Q41.15 42 40.5 42h-1.05q-.65 0-1.075-.425-.425-.425-.425-1.075v-2.7Zm.15-16.5h27.7L35.1 13H12.9ZM9 24.3v10.5Zm5.3 8q1.15 0 1.925-.8.775-.8.775-1.9 0-1.15-.775-1.975-.775-.825-1.925-.825t-1.975.825q-.825.825-.825 1.975 0 1.15.825 1.925.825.775 1.975.775Zm19.45 0q1.15 0 1.975-.8.825-.8.825-1.9 0-1.15-.825-1.975-.825-.825-1.975-.825-1.15 0-1.925.825-.775.825-.775 1.975 0 1.15.8 1.925.8.775 1.9.775ZM9 34.8h30V24.3H9Z"/></svg>                                   
                               </span>
                              
                               <div class="offset-top-30">
                                   <h5 class="text-bold ">Choose your Bus</h5>
                               </div>
                               <div class="offset-top-10" style="text-align:center">
                                   <p class="text-gray">
                                    Search for preferred destination and route and select 
                                   </p>
                               </div>
                           </div>
                        </div>
                        <div class="col-md-4" >
                           <div class="box-icon">
                            <span class="icon icon-xlg icon-circle icon-outlined icon-chathams-primary-filled ">
                            <svg fill="#fff" xmlns="http://www.w3.org/2000/svg" height="48" width="48"><path d="M15 11q-1.45 0-2.45-1-1-1-1-2.45 0-1.45 1-2.5T15 4q1.45 0 2.5 1.05t1.05 2.5Q18.55 9 17.5 10q-1.05 1-2.5 1Zm12.85 28H13.8q-1.3 0-2.225-.85T10.4 36L5.75 12.75H8.9L13.25 36h14.6Zm12 5-5.9-10.25h-15.7q-1.3 0-2.325-.75-1.025-.75-1.275-2L12.3 18.6q-.45-2.3 1.15-4.075 1.6-1.775 4-1.775 1.75 0 3 1.125t1.6 2.825l2.3 12h8.3q1 0 1.75.6t1.25 1.45L42.5 42.5Z"/></svg>
                            </span>
                              
                               <div class="offset-top-30">
                                   <h5 class="text-bold ">Choose your Ticket</h5>
                               </div>
                               <div class="offset-top-10" style="text-align:center">
                                   <p class="text-gray">
                                   Select preferred ticket, time, seat and ticket <br> fare     
                                    </p>
                               </div>
                           </div>
                        </div>
                        <div class="col-md-4" >
                           <div class="box-icon">
                            <span class="icon icon-xlg icon-circle icon-outlined icon-chathams-primary-filled ">
                            <svg fill="#fff" xmlns="http://www.w3.org/2000/svg" height="48" width="48"><path d="M27 27q-2.5 0-4.25-1.75T21 21q0-2.5 1.75-4.25T27 15q2.5 0 4.25 1.75T33 21q0 2.5-1.75 4.25T27 27Zm-16 7q-1.25 0-2.125-.875T8 31V11q0-1.25.875-2.125T11 8h32q1.25 0 2.125.875T46 11v20q0 1.25-.875 2.125T43 34Zm5-3h22q0-2.1 1.45-3.55Q40.9 26 43 26V16q-2.1 0-3.55-1.45Q38 13.1 38 11H16q0 2.1-1.45 3.55Q13.1 16 11 16v10q2.1 0 3.55 1.45Q16 28.9 16 31Zm24 9H5q-1.25 0-2.125-.875T2 37V14h3v23h35Zm-29-9V11v20Z"/></svg>
                            </span>
                              
                               <div class="offset-top-30">
                                   <h5 class="text-bold ">Pay your Bills</h5>
                               </div>
                               <div class="offset-top-10" style="text-align:center">
                                   <p class="text-gray">
                                   Choose payment options/type, make payments and get Confirmation
                                   </p>
                               </div>
                           </div>
                        </div>
                  <!-- @foreach ($hwClements as $item)

                      <div class="col-md-4" >
                           
                                <div class="box-icon">
                                    <span class="icon icon-xlg icon-circle icon-outlined icon-chathams-primary-filled mdi mdi-wifi">
                                        @if($loop->index == '0')
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="#fff" height="48" width="48"><path d="M11.6 30.15 8.45 27q3.5-3.5 7.325-5.25T24 20q4.4 0 8.225 1.75Q36.05 23.5 39.55 27l-3.15 3.15q-3.05-3.05-6.15-4.35-3.1-1.3-6.25-1.3t-6.25 1.3q-3.1 1.3-6.15 4.35ZM3.15 21.7 0 18.55q4.65-4.75 10.825-7.65Q17 8 24 8q7 0 13.175 2.9Q43.35 13.8 48 18.55l-3.15 3.15q-4.4-4.2-9.625-6.7T24 12.5q-6 0-11.225 2.5T3.15 21.7ZM24 42.55l7.4-7.45q-1.45-1.45-3.325-2.275Q26.2 32 24 32t-4.075.825Q18.05 33.65 16.6 35.1Z"/></svg>
                                        @endif    
                                        
                                    </span>
                                   
                                    <div class="offset-top-30">
                                        <h5 class="text-bold ">{{ __(@$item->data_values->heading) }}</h5>
                                    </div>
                                    <div class="offset-top-10" style="text-align:center">
                                        <p class="text-gray">{{ __(@$item->data_values->sub_heading) }}</p>
                                    </div>
                                </div>
                           
                            
                                
                            
                      </div>
                      @endforeach -->
                      
            </div>
          </div>
        </section>

<!-- Working Process Section Starts Here -->
<!-- <section class="working-process padding-top padding-bottom">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-10">
                <div class="section-header text-center">
                    <h2 class="title title-underline">{{ __(@$hwContent->data_values->heading) }}</h2>
                    <p>{{ __(@$hwContent->data_values->sub_heading) }}</p>
                </div>
            </div>
        </div>
        <div class="row g-4 gy-md-5 justify-content-center">
            @foreach ($hwClements as $item)
                <div class="col-lg-4 col-md-6 col-sm-10">
                    <div class="working-process-item">
                        <div class="thumb-wrapper">
                            <div class="thumb">
                                @php
                                    echo @$item->data_values->icon
                                @endphp
                            </div>
                        </div>
                        <div class="content">
                            <h4 class="title">{{ __(@$item->data_values->heading) }}</h4>
                            <p>{{ __(@$item->data_values->sub_heading) }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section> -->
<!-- Working Process Section Ends Here -->
