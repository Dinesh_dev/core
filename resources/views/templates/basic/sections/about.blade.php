@php
    $aboutContent = getContent('about.content', true);
@endphp

<section class="section-90 section-md-111 text-left">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-sm-8 cell-md-6">
          <div class="inset-lg-right-20">
            <h2 class="text-bold text-left text-md-left">A Few Words About Us</h2>
            <hr class="divider hr-md-left-0 bg-chathams-blue">
            <div class="offset-top-30 offset-md-top-60">
                <p>                        UNIQUE Mass Transport Ltd an interstates transport company and is also a courier and logistics services company headquartered in Lagos, Nigeria. Incorporated in 2022, it is a subsidiary of the Unique Auto place Ltd.  an intelligence and Unique management company based in Nigeria. The company has service in different States. Abuja Portharcourt, Aba, Enugu, Umuahia etc.
                </p>
                <div class="offset-top-10">
                    <p>
                        Unique Mass Transport Ltd is one of the fastest growing  transport and logistics company in Nigeria. It provides d inter-state express delivery services without delay through the use of our online booking platform on our website.<br>
 
 Our rapid growth has largely been propelled  by an excellent team of thorough-bred professionals. UNIQUE prides you  with the most satisfying dreams you ever looked for. 
 
 Contact us today and you will be happy you did.<br>
 Our  executive Sienna car carrying 7 passengers with factory fitted air-conditioning. 
 First bus leaving various route between 06:00am - 07:00am 
 and second bus between 08:00am - 09:00am
                     </p>
                </div>
            </div>
          </div>
        </div>
        <div class="cell-sm-8 cell-md-6 offset-top-40 offset-md-top-0">
            <img src="{{ getImage('assets/images/frontend/about/'. $aboutContent->data_values->image) }}" alt="{{ __(@$aboutContent->data_values->heading) }}" style="width:100%;height:400px">

          <!-- RD Video-->
          {{-- <div data-rd-video-path="video/video-for-player-1" data-rd-video-title="" data-rd-video-muted="true" data-rd-video-preview="video/video-for-player-1.jpg" data-rd-video-preload="false" class="rd-video-player">
            <div class="rd-video-wrap embed-responsive-16by9">
              <!-- Play\Pause button--><a href="#" class="rd-video-play-pause rd-video-main-play mdi mdi-play-circle-outline"></a>
              <div class="rd-video-preloader"></div>
              <video preload="metadata"></video>
              <div class="rd-video-preview"></div>
              <div class="rd-video-top-controls">
                <!-- Title--><span class="rd-video-title"></span><a href="#" class="rd-video-fullscreen mdi mdi-fullscreen rd-video-icon"></a>
              </div>
              <div class="rd-video-controls">
                <div class="rd-video-controls-buttons">
                  <!-- Play\Pause button--><a href="#" class="rd-video-play-pause mdi mdi-play"></a>
                  <!-- Progress bar-->
                </div>
                <div class="rd-video-progress-bar"></div>
                <div class="rd-video-time"><span class="rd-video-current-time"></span> <span class="rd-video-time-divider">:</span>  <span class="rd-video-duration"></span></div>
                <div class="rd-video-volume-wrap">
                  <!-- Volume button--><a href="#" class="rd-video-volume mdi mdi-volume-high rd-video-icon"></a>
                  <div class="rd-video-volume-bar-wrap">
                    <!-- Volume bar-->
                    <div class="rd-video-volume-bar"></div>
                  </div>
                </div>
              </div>
            </div>
          </div> --}}
        </div>
      </div>
    </div>
  </section>



{{--
<section class="about-section padding-top padding-bottom">
    <div class="container">
        <div class="row mb-4 mb-md-5 gy-4">
            <div class="col-lg-7 col-xl-6">
                <div class="about-content">
                    <div class="section-header">
                        <h2 class="title">{{ __(@$aboutContent->data_values->heading) }}</h2>
                    </div>
                    <p>
                        @php
                            echo @$aboutContent->data_values->short_description
                        @endphp
                    </p>
                </div>
            </div>
            <div class="col-lg-5 col-xl-6">
                <div class="about-thumb">
                    <img src="{{ getImage('assets/images/frontend/about/'. $aboutContent->data_values->image) }}" alt="{{ __(@$aboutContent->data_values->heading) }}">
                </div>
            </div>
        </div>
        <div class="about-details">
            <div class="item">
                <h4 class="title">{{ __(@$aboutContent->data_values->title) }}</h4>
                <p>
                    @php
                        echo @$aboutContent->data_values->description
                    @endphp
                </p>
            </div>
        </div>
    </div>
</section>
--}}
