@extends($activeTemplate.'layouts.newfrontend')

@section('content')
<section class="section-90 section-md-111 text-left">
    <div class="shell">
      <div class="range range-xs-center">
        <div class="cell-xs-10 cell-md-8">
          <div class="inset-lg-right-40">
            <h2 class="text-bold  text-md-left">Get in Touch</h2>
            <hr class="divider hr-sm-left-0 bg-chathams-blue">
            <div class="offset-top-35 offset-md-top-65">
              <p>You can contact us any way that is convenient for you. We are available 24/7 via fax or email. You can also use a quick contact form below or visit our office personally.</p>
            </div>
            <div class="offset-top-6">
              <p>We would be happy to answer your questions.</p>
            </div>
            <div class="offset-top-20 offset-md-top-30">
              <!-- RD Mailform-->
              <form data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php" class="rd-mailform text-left">
                @csrf
                <div class="range range-xs-center">
                  <div class="cell-sm-6">
                    <div class="form-group form-group-label-outside">
                      <label for="contacts-first-name" class="form-label form-label-outside text-dark">First name</label>
                      <input id="contacts-first-name" type="text" name="first-name" data-constraints="@Required" class="form-control">
                    </div>
                    <div class="form-group form-group-label-outside offset-top-20">
                      <label for="contacts-email" class="form-label form-label-outside text-dark">E-mail</label>
                      <input id="contacts-email" type="email" name="email" data-constraints="@Email @Required" class="form-control">
                    </div>
                  </div>
                  <div class="cell-sm-6 offset-top-20 offset-sm-top-0">
                    <div class="form-group form-group-label-outside">
                      <label for="contacts-last-name" class="form-label form-label-outside text-dark">Last name</label>
                      <input id="contacts-last-name" type="text" name="last-name" data-constraints="@Required" class="form-control">
                    </div>
                    <div class="form-group form-group-label-outside offset-top-20">
                      <label for="contacts-phone" class="form-label form-label-outside text-dark">Phone</label>
                      <input id="contacts-phone" type="text" name="last-name" data-constraints="@IsNumeric @Required" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="form-group form-group-label-outside offset-top-20">
                  <label for="contacts-message" class="form-label form-label-outside text-dark">Message</label>
                  <textarea id="contacts-message" name="message" data-constraints="@Required" style="max-height: 150px;" class="form-control"></textarea>
                </div>
                <div class="offset-top-18 offset-sm-top-30 text-center text-sm-left">
                  <button type="submit" class="btn btn-ripe-lemon">Send Message</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="cell-xs-10 cell-md-4 offset-top-60 offset-md-top-0">
          <!-- Sidebar-->
          <aside class="text-left">
            <div class="inset-md-left-30">
              <!-- Socials-->
              <div>
                <h5 class="text-bold">Socials</h5>
              </div>
              <div class="offset-top-6">
                <div class="text-subline"></div>
              </div>
              <div class="offset-top-20">
                <ul class="list-inline list-inline-2">
                  <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-facebook"></a></li>
                  <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-twitter"></a></li>
                  <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-google-plus"></a></li>
                  <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-instagram"></a></li>
                  <li><a href="#" class="icon icon-xxs icon-silver-filled icon-circle fa fa-rss"></a></li>
                </ul>
              </div>
              <!-- Phones-->
              <div class="offset-top-30 offset-md-top-60">
                <div>
                  <h5 class="text-bold">Phones</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline"></div>
                </div>
                <div class="offset-top-20">
                  <div class="unit unit-middle unit-horizontal unit-spacing-xs">
                    <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-phone"></span></div>
                    <div class="unit-body text-gray-darker">
                      <p>
                        <a href="callto:#" class="reveal-block reveal-xs-inline-block text-gray-darker">+234 803 398 0593</a>
                        <span class="veil reveal-xs-inline-block">, &nbsp;</span>
                        <a href="callto:#" class="reveal-block reveal-xs-inline-block text-gray-darker">+234 802 319 9735</a>
                        <span class="veil reveal-xs-inline-block">, &nbsp;</span>
                        <a href="callto:#" class="reveal-block reveal-xs-inline-block text-gray-darker">+234 803 771 7320</a></p>
                        
                    </div>
                  </div>
                </div>
              </div>
              <!-- E-mail-->
              <div class="offset-top-30 offset-md-top-60">
                <div>
                  <h5 class="text-bold">E-mail</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline"></div>
                </div>
                <div class="offset-top-20">
                  <div class="unit unit-middle unit-horizontal unit-spacing-xs">
                    <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-email-outline"></span></div>
                    <div class="unit-body">
                      <p><a href="mailto:{{ @$content->data_values->email }}" class="text-primary">uniquemasstransport@gmail.com</a></p>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Address-->
              <div class="offset-top-30 offset-md-top-60">
                <div>
                  <h5 class="text-bold">Head Office</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline"></div>
                </div>
                <div class="offset-top-20">
                  <div class="unit unit-horizontal unit-spacing-xs">
                    <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-map-marker"></span></div>
                    <div class="unit-body">
                      <p>
                        <a href="#" class="text-gray-darker">17 Balogun Street Anifowoshe Ikeja Lagos. 
                            <br class="veil reveal-sm-inline-block"> 
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="offset-top-30 offset-md-top-60">
                <div>
                  <h5 class="text-bold">Other Office</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline"></div>
                </div>
                {{-- <div class="offset-top-20">
                  <div class="unit unit-horizontal unit-spacing-xs">
                    <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-map-marker"></span></div>
                    <div class="unit-body">
                      <p>
                        <a href="#" class="text-gray-darker">Abuja Office - Opp. Privé Brunch; A.E. Ekukinam street. 
                            Utako, Abuja.
                        <br class="veil reveal-sm-inline-block"> 
                        </a>
                      </p>
                    </div>
                  </div>
                </div> --}}

                {{-- <div class="offset-top-20">
                    <div class="unit unit-horizontal unit-spacing-xs">
                      <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-map-marker"></span></div>
                      <div class="unit-body">
                        <p>
                          <a href="#" class="text-gray-darker">
                            Portharcourt Office - No. 1 East West Road Rumuokoro flyover, Portharcourt.
                          <br class="veil reveal-sm-inline-block"> 
                          </a>
                        </p>
                      </div>
                    </div>
                </div> --}}

                {{-- <div class="offset-top-20">
                    <div class="unit unit-horizontal unit-spacing-xs">
                      <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-map-marker"></span></div>
                      <div class="unit-body">
                        <p>
                          <a href="#" class="text-gray-darker">
                            @lang('Lagos Office') - No. 51 Old Ojo Road, Maza Maza, Lagos State
                          <br class="veil reveal-sm-inline-block"> 
                          </a>
                        </p>
                      </div>
                    </div>
                </div> --}}
                <div class="offset-top-20">
                  <div class="unit unit-horizontal unit-spacing-xs">
                    <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-map-marker"></span></div>
                    <div class="unit-body">
                      <p>
                        <a href="#" class="text-gray-darker">
                          No. 172 Old  Abeokuta Road. Iyanaipaja  bustop. Lagos
                        <br class="veil reveal-sm-inline-block"> 
                        </a>
                      </p>
                    </div>
                  </div>
              </div>

                <div class="offset-top-20">
                    <div class="unit unit-horizontal unit-spacing-xs">
                      <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-map-marker"></span></div>
                      <div class="unit-body">
                        <p>
                          <a href="#" class="text-gray-darker">
                            Aba Office - No. 1 Milverton Road, Aba Abia State.
                          <br class="veil reveal-sm-inline-block"> 
                          </a>
                        </p>
                      </div>
                    </div>
                </div>


              </div>
              <!-- Opening Hours-->
              <div class="offset-top-30 offset-md-top-60">
                <div>
                  <h5 class="text-bold">Opening Hours</h5>
                </div>
                <div class="offset-top-6">
                  <div class="text-subline"></div>
                </div>
                <div class="offset-top-20">
                  <div class="unit unit-horizontal unit-spacing-xs">
                    <div class="unit-left"><span class="icon icon-xxs icon-primary icon-primary-filled icon-circle mdi mdi-calendar-clock"></span></div>
                    <div class="unit-body">
                      <p class="text-gray-darker">Mon-Fri: 8:00am–6:00pm</p>
                      <p class="text-gray-darker">Sat: 10:00am–4:00pm</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </aside>
        </div>
      </div>
    </div>
  </section>
  <section>
    <!-- RD Google Map-->
    {{-- <div class="rd-google-map">
      <div id="rd-google-map" data-zoom="15" data-x="-73.9874068" data-y="40.643180" data-styles='[{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}]' class="rd-google-map__model"></div>
      <ul class="rd-google-map__locations">
        <li data-x="-73.9874068" data-y="40.643180">
          <p>9870 St Vincent Place, Glasgow, DC 45 Fr 45</p>
        </li>
      </ul>
    </div> --}}
    {{-- <iframe class="map" style="border:0;" src="https://maps.google.com/maps?q=6.599140,3.336270&hl=es;z=14&amp;output=embed"></iframe> --}}
    <div class="map-wrapper" style="margin-top:20px;">
        <iframe class="map" style="border:0;width:100%;height:450px;" src="https://maps.google.com/maps?q=6.599140,3.336270&hl=es;z=14&amp;output=embed"></iframe>
    </div>
  </section>


@endsection
