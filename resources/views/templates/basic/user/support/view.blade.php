@extends($activeTemplate.'layouts.newfrontend')

@section('content')
<div class="container">
    <div class="section-90 section-md-111 text-left" >
        <div class="col-md-12">
            <div class="card cmn--card" style="margin-bottom: 60px;">
                <div class="card-header d-flex flex-wrap justify-content-between align-items-center" style="gap:10px">
                    <h5 class="card-title m-0 text-white" style="margin-bottom: 0px">
                        @if($my_ticket->status == 0)
                        <span class="badge badge--success py-2 px-3">@lang('Open')</span>
                        @elseif($my_ticket->status == 1)
                        <span class="badge badge--primary py-2 px-3">@lang('Answered')</span>
                        @elseif($my_ticket->status == 2)
                        <span class="badge badge--warning py-2 px-3">@lang('Replied')</span>
                        @elseif($my_ticket->status == 3)
                        <span class="badge badge--dark py-2 px-3">@lang('Closed')</span>
                        @endif
                        [@lang('Ticket')#{{ $my_ticket->ticket }}] {{ $my_ticket->subject }}
                    </h5>
                    <button class="" type="button" title="@lang('Close Ticket')" data-bs-toggle="modal" data-bs-target="#DelModal"><i class="fa fa-lg fa-times-circle"></i>
                    </button>
                </div>
                <div class="card-body" style="padding:35px;">
                    <div class="accordion" id="accordionExample">
                        <div id="collapseThree" class="collapse show" aria-labelledby="headingThree" data-parent="#accordionExample">
                            @if($my_ticket->status != 4)
                            <form method="post" action="{{ route('ticket.reply', $my_ticket->id) }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="replayTicket" value="1">
                                <div class="row">
                                    <div class="col-md-12 offset-top-20">
                                        <div class="form-group">
                                            <textarea name="message" class="form-control form--control shadow-none" id="inputMessage" placeholder="@lang('Your Reply')" rows="4" cols="10"></textarea>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                <div class="col-sm-12 offset-top-20">
                                    <div class="d-flex justify-content-between">
                                        <div class="col-md-8">
                                            <div class="">
                                                <label for="inputAttachments" class="form-label form-label-outside">@lang('Attachments')</label>
    
                                                <div class="d-flex justify-content-between">
                                                    <input type="file" name="attachments[]" id="inputAttachments" class="form--control form-control ps-2 py-1">
                                                    <a href="javascript:void(0)" class="btn btn--base btn-round ms-sm-4 ms-1 addFile px-3 radius-5">
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            
                                                <div id="fileUploadsContainer"></div>
                                                <p class="mt-1 ticket-attachments-message text-muted fs-small">
                                                    @lang('Allowed File Extensions'): .@lang('jpg'), .@lang('jpeg'), .@lang('png'), .@lang('pdf'), .@lang('doc'), .@lang('docx')
                                                </p>
                                            </div>
                                        </div>
                                        <div>
                                            <button type="submit" class="btn btn--base mt-md-4 mt-2 h-40">
                                                <i class="fa fa-reply"></i> @lang('Reply')
                                            </button>
                                        </div>
                                    </div>
                                    
                                </div>
                                </div>
                            </form>
                            @endif
                        </div>
                    </div>
                   
                   <div class="row">
                    <div class="col-md-12">
                        <div style="padding:10px;">
                            <div class="card" style="border:1px solid #ddd">
                                <div class="card-body " style="padding: 20px 60px;">
                                    @foreach($messages as $message)
                                    @if($message->admin_id == 0)
                                    <div  class="row border border-primary border-radius-3 my-sm-3 my-2 py-3 mx-0 mx-sm-2 offset-top-20" style="background-color: #dbe9ff;padding:15px;">
                                        <div class="col-md-3 border--right text-right replyname">
                                            <h5 class="my-3">{{ $message->ticket->name }}</h5>
                                        </div>
                                        <div class="col-md-9 ps-2">
                                            <p class="text-muted fw-bold">
                                                @lang('Posted on') {{ $message->created_at->format('l, dS F Y @ H:i') }}</p>
                                            <p>{{$message->message}}</p>
                                            @if($message->attachments()->count() > 0)
                                            <div class="mt-2">
                                                @foreach($message->attachments as $k=> $image)
                                                <a href="{{route('ticket.download',encrypt($image->id))}}" class="mr-3"><i class="fa fa-file"></i> @lang('Attachment') {{++$k}} </a>
                                                @endforeach
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    @else
                                    <div  class="row border border-warning border-radius-3 my-sm-3 my-2 py-3 mx-0 mx-sm-2 offset-top-20" style="background-color: #ffd96729;padding:15px;">
                                        <div class="col-md-3 border--right text-right replyname">
                                            <h5 class="my-1">{{ $message->admin->name }}</h5>
                                            <p class="lead text-muted">@lang('Staff')</p>
                                        </div>
                                        <div class="col-md-9">
                                            <p class="text-muted fw-bold">
                                                @lang('Posted on') {{ $message->created_at->format('l, dS F Y @ H:i') }}</p>
                                            <p>{{$message->message}}</p>
                                            @if($message->attachments()->count() > 0)
                                            <div class="mt-2">
                                                @foreach($message->attachments as $k=> $image)
                                                <a href="{{route('ticket.download',encrypt($image->id))}}" class="mr-3"><i class="fa fa-file"></i> @lang('Attachment') {{++$k}} </a>
                                                @endforeach
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                      
                    </div>
                   </div>
                    
                   
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="DelModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 100000">
    <div class="modal-dialog">
        <div class="modal-content">
            <form method="post" action="{{ route('ticket.reply', $my_ticket->id) }}">
                @csrf
                <input type="hidden" name="replayTicket" value="2">
                <div class="modal-header modal-header-flex">
                    <h5 class="modal-title modal-title-full"> @lang('Confirmation')!</h5>
                    <button type="button" class="w-auto btn--close" data-dismiss="modal"><i class="fa fa-times"></i></button>
                </div>
                <div class="modal-body">
                    <strong class="text-dark">@lang('Are you sure you want to close this support ticket')?</strong>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn  btn--danger w-auto btn--sm px-3" data-dismiss="modal"> @lang('Close')</button>
                    <button type="submit" class="btn btn--success btn--sm w-auto"> @lang("Confirm") </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    (function($) {
        "use strict";
        $('.delete-message').on('click', function(e) {
            $('.message_id').val($(this).data('id'));
        });
        $('.addFile').on('click', function() {
            $("#fileUploadsContainer").append(
                `<div class="extra-file-attach d-flex justify-content-between offset-top-20">
                            <input type="file" name="attachments[]" id="inputAttachments" class="form-control form--control ps-2 py-1" required>
                            <a href="javascript:void(0)" class="btn btn--danger btn-round remove-btn px-3">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                    `
            )
        });
        $(document).on('click', '.remove-btn', function() {
            $(this).closest('.extra-file-attach').remove();
        });
    })(jQuery);
</script>
@endpush
