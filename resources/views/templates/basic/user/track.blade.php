@extends($activeTemplate.'layouts.newfrontend')
@push('style')
<style>

    .tracker{
        padding:30px 20px;
        display:flex;
        justify-content:space-between;
        align-items:flex-end;
        gap:10px;
    }

    .tracker div:nth-of-type(1){
        width:80%;
    }

</style>
@endpush
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-8 col-md-8">
            <form method="post" action="{{route('user.tackpackage')}}">
                @csrf
                {{-- <div class="tracker">
                    <div class="form--group">
                        <label for="subject" style="font-weight:900;color:black;">Tracking No <span>*</span></label>
                        <input id="trackingno" name="trackingno" type="text" class="form--control" placeholder="Enter Tracking Number" value="" required="">
                    </div>
                    <div>
                        <button class="contact-button" type="submit" style="height:40px">Search</button>
                    </div>
                </div> --}}
                <div class="tracker">
                    <div class="group-item element-fullwidth offset-top-6 offset-md-top-0 offset-lg-top-6">
                        <div class="form-group form-group-label-outside text-left">
                          <label for="tracking-schedule-number" class="form-label form-label-outside text-dark" style="color:black">Tracking No <span>*</span></label>
                          <input id="tracking-schedule-number" type="text" name="trackingno" style="color:#393a3c;" class="form-control shadow-drop-md bg-white">
                        </div>
                    </div>
                    <div class="group-item reveal-sm-inline-block text-center text-sm-left">
                        <button type="submit" style="max-width: 140px; min-width: 140px; min-height: 50px;" class="shadow-drop-md btn btn-ripe-lemon">search</button>
                      </div>
            </form>
            
        </div>
    </div>
</div>

<!-- Result this  -->
<div class="container">
    <div class="row justify-content-center">
        <table data-responsive="true" class="table table-custom table-traking table-fixed table-hover-rows stacktable large-only">
                
            <tbody>
                <tr>
                    <th>Tracking No</th>
                    <th>Sender Name</th>
                    <th>Sender Phone</th>
                    <th>Sender Address</th>
                    <th> Type</th>
                    <th> Weight</th>
                    <th>Status</th>
                </tr>
                @isset($data)
                    <tr>
                        <td>
                            <span class="badge badge--success font-weight-bold"> {{$data->transport_id}} </span>
                        </td>
                        <td>{{$data->sendername}}</td>
                        <td>{{$data->senderphoneno}}</td>
                        <td>
                        <p class="packageaddress">{{$data->senderaddress}}</p>  
                        </td>
                        <td>        
                            @if($data->packagetype == '1')
                            <span class="badge badge--success font-weight-bold"> Fragile </span>
                            @endif
                            @if($data->packagetype == '2')
                            <span class="badge badge--success font-weight-bold"> Non Fragile </span>
                            @endif
                        </td>
                        <td>{{$data->packageweight}}</td>
                        <td>
                                        @if($data->status == '1')
                                        <span class="badge badge--success font-weight-bold" >In Transit </span>
                                        @endif
                                        @if($data->status == '2')
                                        <span class="badge badge--success font-weight-bold"> Processing </span>
                                        @endif
                                        @if($data->status == '3')
                                        <span class="badge badge--success font-weight-bold"> Delivered </span>
                                        @endif
                                        @if($data->status == '4')
                                        <span class="badge badge--success font-weight-bold"> Arrived </span>
                                        @endif
                        </td>
                    </tr>
                @endisset
               
                @empty($data)
                    <tr>
                        <td colspan="6">Please enter tracking no to track your package</td>
                    </tr>
                    
                @endempty
              </tbody>
        </table>
    </div>
</div>

@endsection