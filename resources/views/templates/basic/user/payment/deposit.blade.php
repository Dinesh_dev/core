@extends($activeTemplate.'layouts.newfrontend')
@section('content')
<div class="container padding-top padding-bottom">
    <div class="range range-xs-center">
	{{--
        @foreach($gatewayCurrency as $data)
        <div class="col-lg-4 col-md-6 col-xl-3">
            <div class="card cmn--card card-deposit">
                <div class="card-header text-center">
                    <h5 class="title">{{__($data->name)}}</h5>
                </div>
                <div class="card-body card-body-deposit">
                    <img src="{{$data->methodImage()}}" class="card-img-top" alt="{{__($data->name)}}" class="w-100">
                </div>
                <div class="card-footer pt-0 pb-4">
                    <a href="javascript:void(0)" data-id="{{$data->id}}" data-name="{{$data->name}}" data-currency="{{$data->currency}}" data-method_code="{{$data->method_code}}" data-base_symbol="{{$data->baseSymbol()}}" data-percent_charge="{{showAmount($data->percent_charge)}}" data-booked_ticket="{{ $bookedTicket }}" class=" btn btn--base w-100 deposit" data-bs-toggle="modal" data-bs-target="#depositModal">
                        @lang('Pay Now')
                    </a>
                </div>
            </div>
        </div>
        @endforeach
	--}}
		<div class="cell-lg-4 cell-md-6 cell-xl-3">
            <div class="card cmn--card card-deposit offset-md-top-60 offset-md-bottom-60" style="margin-bottom:60px">
				<div class="card-header text-center">
                    <h5 class="title" style="color:#fff">Payment</h5>
                </div>
                <div class="card-body">
				
					<span class="text-center">
					<h5 style="font-weight: 600;margin-top:50px">Payment is under process please wait</h5>
					<div style="width:100%;height:100px;display:flex;justify-content:center">
						<svg version="1.1" id="L4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
							<circle fill="blue" stroke="none" cx="6" cy="50" r="6">
								<animate
								attributeName="opacity"
								dur="1s"
								values="0;1;0"
								repeatCount="indefinite"
								begin="0.1"/>    
							</circle>
							<circle fill="blue" stroke="none" cx="26" cy="50" r="6">
								<animate
								attributeName="opacity"
								dur="1s"
								values="0;1;0"
								repeatCount="indefinite" 
								begin="0.2"/>       
							</circle>
							<circle fill="blue" stroke="none" cx="46" cy="50" r="6">
								<animate
								attributeName="opacity"
								dur="1s"
								values="0;1;0"
								repeatCount="indefinite" 
								begin="0.3"/>     
							</circle>
							<circle fill="blue" stroke="none" cx="66" cy="50" r="6">
								<animate
								attributeName="opacity"
								dur="1s"
								values="0;1;0"
								repeatCount="indefinite" 
								begin="0.3"/>     
							</circle>
					    </svg>
					</div>
					
						  
					{{-- <p>Unique Autoplace (Nig) Ltd.</p> --}}
					
					</span>
                </div>
                <div class="card-footer pt-0 pb-4">
					
					<form action="{{ route('pay') }}" method="post" id="ticketpayment">
						@csrf
						<input type="hidden" name="amount" value="{{ $bookedTicket->sub_total }}">
						<input type="hidden" name="reference" value="{{ $bookedTicket->pnr_number }}">
						@if (Auth::guest())
						<input type="hidden" name="email" value="{{ $bookedTicket->customer_email }}">
						@else
						<input type="hidden" name="email" value="{{ $bookedTicket->user->email }}">
						@endif
						<input type="hidden" name="currency" value="NGN">
						<input type="hidden" name="orderID" value="{{ $bookedTicket->id }}">
						{{-- <div class="prevent-double-click">
							<button id="pay_now"  type="submit" class="btn btn--base w-100 deposit">@lang('Pay Now')</button>
						</div> --}}
					</form>
					
					{{-- <a href="{{ route('user.ticket.print', $bookedTicket->id) }}" data-toggle="modal" data-target="#depositModal" class="btn btn-default" target="_blank">@lang('Get Ticket')</a> --}}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="depositModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title method-name" id="depositModalLabel"></h5>
                <a href="javascript:void(0)" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </a>
            </div>
			<div class="modal-body">
				<div class="form-group">
					<input type="hidden" name="currency" class="edit-currency">
					<input type="hidden" name="method_code" class="edit-method-code">
				</div>
				<span>
					<h1>2006820547</h1><br>
					Unique Autoplace (Nig) Ltd.<br>
					First Bank<br>
				</span>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn--danger w-auto btn--sm" data-bs-dismiss="modal">@lang('Close')</button>
			</div>
        </div>
    </div>
</div>
@endsection

@push('script')
	<script>
		$(document).ready(function(){
			setTimeout(() => {
				$('#ticketpayment').submit();
			}, "1000");


			



		});
	</script>
@endpush
