@extends($activeTemplate.'layouts.newfrontend')
@section('content')
<div class="section-90 section-md-111 text-left">
    <div class="container">
        <div class="profile__edit__wrapper">
            <div class="profile__edit__form">
                <form class="register prevent-double-click" action="" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="col-xl-10">
                            <div class="range range-xs-center">
                                {{-- <h5 class="title">{{ __($pageTitle) }}</h5> --}}
                                <div class="cell-sm-6">
                                <div class="form-group form-group-label-outside">
                                    <label for="InputFirstname" class="form-label form-label-outside text-dark">@lang('First Name')</label>
                                    <input type="text" class="form-control" id="InputFirstname" name="firstname" placeholder="@lang('First Name')" value="{{$user->firstname}}" minlength="3">
                                </div>
                                </div>
                                <div class="cell-sm-6">
                                    <div class="form-group form-group-label-outside">
                                        <label for="lastname" class="form-label form-label-outside text-dark">@lang('Last Name')</label>
                                        <input type="text" class="form-control" id="lastname" name="lastname" placeholder="@lang('Last Name')" value="{{$user->lastname}}" required>
                                    </div>
                                </div>
                                <div class="cell-sm-6 offset-top-20">
                                    <div class="form-group form-group-label-outside">
                                        <label for="email" class="form-label form-label-outside text-dark">@lang('E-mail Address')</label>
                                        <input class="form-control" id="email" value="{{$user->email}}" disabled>
                                    </div>
                                </div>
                                <div class="cell-sm-6 offset-top-20">
                                    <div class="form-group form-group-label-outside">
                                        <label for="phone" class="form-label form-label-outside text-dark">@lang('Mobile Number')</label>
                                        <input class="form-control" id="phone" value="{{$user->mobile}}" disabled>
                                    </div>
                                </div>
                                <div class="cell-sm-6 offset-top-20">
                                    <div class="form-group form-group-label-outside">
                                        <label for="address" class="form-label form-label-outside text-dark">@lang('Address')</label>
                                        <input type="text" class="form-control" id="address" name="address" placeholder="@lang('Address')" value="{{@$user->address->address}}" required="">
                                    </div>
                                </div>
                                <div class="cell-sm-6 offset-top-20">
                                    <div class="form-group form-group-label-outside">
                                        <label for="state" class="form-label form-label-outside text-dark">@lang('State')</label>
                                        <input type="text" class="form-control" id="state" name="state" placeholder="@lang('state')" value="{{@$user->address->state}}" required="">
                                    </div>
                                </div>
                                {{-- <div class="cell-sm-6 offset-top-20">
                                    <div class="form-group form-group-label-outside">
                                        <label for="zip" class="form-label form-label-outside text-dark">@lang('Zip Code')</label>
                                        <input type="text" class="form-control" id="zip" name="zip" placeholder="@lang('Zip Code')" value="{{@$user->address->zip}}" required="">
                                    </div>
                                </div>
                                <div class="cell-sm-6 offset-top-20">
                                    <div class="form-group form-group-label-outside">
                                        <label for="city" class="form-label form-label-outside text-dark">@lang('City')</label>
                                        <input type="text" class="form-control" id="city" name="city" placeholder="@lang('City')" value="{{@$user->address->city}}" required="">
                                    </div>
                                </div> --}}
                                <div class="cell-sm-6 offset-top-20">
                                    <div class="form-group form-group-label-outside">
                                        <label for="zip" class="form-label form-label-outside text-dark">@lang('Zip Code')</label>
                                        <input type="text" class="form-control" id="zip" name="zip" placeholder="@lang('Zip Code')" value="{{@$user->address->zip}}" required="">
                                    </div>
                                </div>
                                <div class="cell-sm-6 offset-top-20">
                                    <div class="form-group form-group-label-outside">
                                        <label class="form-label form-label-outside text-dark">@lang('Country')</label>
                                        <input class="form-control" value="{{@$user->address->country}}" disabled>
                                    </div>
                                </div>
                                <div class="offset-top-20">
                                    <button type="submit" class="btn btn-ripe-lemon">@lang('Update Profile')</button>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
