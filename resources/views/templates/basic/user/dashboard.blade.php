@extends($activeTemplate.'layouts.newfrontend')
@section('content')
<!-- booking history Starts Here -->
<section class="section-90 section-md-111 text-left">
<a href="#" id="openReviewModal" class="d-none" data-bs-toggle="modal" data-bs-target="#reviewModal"></a>
    <div class="container">
        <div class="dashboard-wrapper">
            <div class="row pb-60 gy-4 justify-content-center">
                <div class="col-lg-4 col-md-6 col-sm-10">
                    <div class="dashboard-widget">
                        <div class="dashboard-widget__content">
                            <p>@lang('Total Booked Ticket')</p>
                            <h3 class="title">{{ __($widget['booked']) }}</h3>
                        </div>
                        <div class="dashboard-widget__icon">
                            <i class="fa fa-ticket"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-10">
                    <div class="dashboard-widget">
                        <div class="dashboard-widget__content">
                            <p>@lang('Total Rejected Ticket')</p>
                            <h3 class="title">{{ __($widget['rejected']) }}</h3>
                        </div>
                        <div class="dashboard-widget__icon">
                            <i class="fa fa-ticket "></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-10">
                    <div class="dashboard-widget">
                        <div class="dashboard-widget__content">
                            <p>@lang('Total Pending Ticket')</p>
                            <h3 class="title">{{ __($widget['pending']) }}</h3>
                        </div>
                        <div class="dashboard-widget__icon">
                            <i class="fa fa-ticket"></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="range range-xs-center range-md-left offset-top-30 offset-md-top-49">
                <div class="">
                  <!-- Classic Responsive Table-->
                  <table data-responsive="true" class="table table-custom table-fixed striped-table">
                    <tr>
                        <th>@lang('PNR Number')</th>
                        <th>@lang('AC / Non-Ac')</th>
                        <th>@lang('Starting Point')</th>
                        <th>@lang('Dropping Point')</th>
                        <th>@lang('Departure Date')</th>
                        <th>@lang('Return Date')</th>
                        <th>@lang('Pickup Time')</th>
                        <th>@lang('Booked Seats')</th>
                        <th>@lang('Status')</th>
                        <th>@lang('Fare')</th>
                        <th>@lang('Action')</th>
                    </tr>
                   
                    @forelse($bookedTickets as $item)
                       
                        <tr>
                            <td class="ticket-no" data-label="@lang('PNR Number')">{{ __($item->pnr_number) }}</td>
                            <td class="" data-label="@lang('AC / Non-Ac')">{{$item->trip->fleetType->has_ac ? 'AC' : 'Non-Ac' }}</td>
                            <td class="pickup" data-label="Starting Point">{{ __($item->pickup->name) }}</td>
                            <td class="drop" data-label="Dropping Point">{{ __($item->drop->name) }}</td>
                            <td class="date" data-label="Journey Date">
                               
                                {{ __(showDateTime($item->date_of_journey , 'd M, Y')) }}
                               
                                
                            </td>
                            <td class="date" data-label="Return Date">
                                
                                {{ __($item->date_of_return ? showDateTime($item->date_of_return , 'd M, Y') : 'N/A' ) }}
                              
                            </td>
                            <td class="time" data-label="Pickup Time">
                                
                                {{ __(showDateTime($item->trip->schedule->start_from, 'H:i a')) }}
                               
                            </td>
                            <td class="seats" data-label="Booked Seats">
                                {{ __(implode(",",$item->seats)) }}
                            </td>
                            <td data-label="@lang('Status')">
                                @if($item->status == 1)
                                <span class="badge badge--success"> @lang('Booked')</span>
                                @elseif($item->status == 2)
                                <span class="badge badge--warning"> @lang('Pending')</span>
                                @elseif($item->status == 3)
                                <span class="badge badge--secondary"> @lang('Canceled')</span>
                                @else
                                <span class="badge badge--danger"> @lang('Rejected')</span>
                                @endif
                            </td>
                            <td class="fare" data-label="Fare">{{ __(showAmount($item->sub_total)) }} {{ __($general->cur_text) }}</td>
                            <td class="action" data-label="Action">
                                <div class="action-button-wrapper">
                                    @if ($item->date_of_journey >= \Carbon\Carbon::today()->format('Y-m-d') && $item->status == 1)
                                    <a href="{{ route('user.ticket.print', $item->id) }}" target="_blank" class="print"><i class="fa fa-print"></i></a>
                                    &nbsp;
                                    <a href="javascript::void(0)" class="checkinfo" data-info="{{ $item }}" data-toggle="modal" data-target="#rescheduleModal_{{ $item->id }}"><i class="fa fa-calendar"></i></a>
                                    &nbsp;
                                    <a href="javascript::void(0)" class="checkinfo " data-info="{{ $item }}" data-toggle="modal" data-target="#cancelModal_{{ $item->id }}"><i class="fa fa-times-circle"></i></a>
                                    @elseif (
                                        $item->date_of_return
                                        &&
                                        ($item->date_of_journey <= \Carbon\Carbon::today()->format('Y-m-d') && $item->date_of_return >= \Carbon\Carbon::today()->format('Y-m-d') && $item->status == 1)
                                    )
                                    <a href="{{ route('user.ticket.print', $item->id) }}" target="_blank" class="print"><i class="fa fa-print"></i></a>
                                    &nbsp;
                                    <a href="javascript::void(0)" class="checkinfo" data-info="{{ $item }}" data-toggle="modal" data-target="#rescheduleModal_{{ $item->id }}"><i class="fa fa-calendar"></i></a>
                                    &nbsp;
                                    <a href="javascript::void(0)" class="checkinfo bg-danger" data-info="{{ $item }}" data-toggle="modal" data-target="#cancelModal_{{ $item->id }}"><i class="fa fa-times-circle"></i></a>
                                    @else
                                        <a href="javascript::void(0)" class="checkinfo" data-info="{{ $item }}" data-toggle="modal" data-target="#infoModal"><i class="fa fa-info-circle"></i></a>
                                        @if($item->status == 3)
                                        &nbsp;
                                        <a href="{{ route('ticket.reuse', [$item->id]) }}" class="checkinfo bg-warning"><i class="fa fa-recycle"></i></a>
                                        @endif
                                    @endif
                                </div>
                            </td>
                        </tr>
                       
                    @empty
                    <tr>
                        <td colspan="11">{{ $emptyMessage }}</td>
                    </tr>
                    @endforelse
                  
                  </table>
                </div>
              </div>

            
            @if ($bookedTickets->hasPages())
            {{ paginateLinks($bookedTickets) }}
            @endif
        </div>
    </div>
</section>
<!-- booking history end Here -->

<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> @lang('Ticket Booking History')</h5>
                <button type="button" class="w-auto btn--close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger w-auto btn--sm px-3" data-dismiss="modal"></i>
                    @lang('Close')
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:100000">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-flex">
                <h5 class="modal-title modal-title-full"> @lang('Please leave a review.')</h5>
                <button type="button" class="w-auto btn--close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
				<form action="{{ route('review') }}" method="POST" class="row gy-2">
					@csrf
					<div class="col-12">
						<div class="form-group">
							<label for="content" class="form-label">@lang('Enter review')</label>
							<textarea class="form-control" rows="5" id="content" name="content"></textarea>
						</div>
					</div>
					<div class="col-12">
						<button type="submit" class="book-bus-btn">@lang('Submit')</button>
					</div>
				</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger w-auto btn--sm px-3" data-dismiss="modal"></i>
                    @lang('Close')
                </button>
            </div>
        </div>
    </div>
</div>

@forelse ($bookedTickets as $item)
<div class="modal fade" id="rescheduleModal_{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:100000">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-flex">
                <h5 class="modal-title modal-title-full"> @lang('Ticket Booking : Reschedule')</h5>
                <button type="button" class="w-auto btn--close" data-bs-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
				<form action="{{ route('ticket.reschedule', $item->id) }}" method="POST" class="row gy-2">
					@csrf
					<input type="text" name="price" hidden>
					@if($item->date_of_journey >= \Carbon\Carbon::today()->format('Y-m-d'))
					<div class="col-sm-12">
						<div class="form-group form-group-label-outside" style="text-align: left">
							<label for="date_of_journey" class="form-label form-label-outside">@lang('Departure Date')</label>
							<input type="text" id="date_of_journey" autocomplete="off" class="form-control datepicker" value="{{ $item->date_of_journey ? Carbon\Carbon::parse($item->date_of_journey)->format('m/d/Y') : date('m/d/Y') }}" name="date_of_journey">
						</div>
					</div>
					@endif
					@if( $item->date_of_return )
					<div class="col-sm-12">
						<div class="form-group form-group-label-outside" style="text-align: left">
							<label for="date_of_return" class="form-label form-label-outside">@lang('Return Date')</label>
							<input type="text" id="date_of_return" class="form--control datepicker" value="{{ $item->date_of_return ? Carbon\Carbon::parse($item->date_of_return)->format('m/d/Y') : date('m/d/Y') }}" name="date_of_return">
						</div>
					</div>
					@endif
					<input type="text" name="seats" hidden>
					<div class="col-sm-12 offset-top-20">
						<button type="submit" class="btn btn-primary">@lang('Confirm')</button>
					</div>
				</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger w-auto btn--sm px-3" data-dismiss="modal"></i>
                    @lang('Close')
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="cancelModal_{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index:100000">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header modal-header-flex">
                <h5 class="modal-title modal-title-full"> @lang('Ticket Booking : Cancel')</h5>
                <button type="button" class="w-auto btn--close" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body text-center">
				<p> Are you sure you want to cancel booking? </p> 
				<p>PNR Number : {{ $item->pnr_number }} ? </p>
				<form action="{{ route('ticket.cancel', $item->id) }}" method="POST" class="row gy-2">
					@csrf
					<input type="text" name="date_of_journey" value="{{ $item->date_of_journey }}" hidden>
					<div class="col-4">
					</div>
					<div class="text-center" style="width:100%">
						<button type="submit" class="btn btn-primary" style="width:100%">@lang('Confirm')</button>
					</div>
					<div class="col-4">
					</div>
				</form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn--danger w-auto btn--sm px-3" data-dismiss="modal"></i>
                    @lang('Close')
                </button>
            </div>
        </div>
    </div>
</div>
@empty
@endforelse
	
@endsection
@push('style')
<style>
    .modal-body p:not(:last-child){
        border-bottom: 1px dashed #ebebeb;
        padding:5px 0;
    }
</style>
@endpush

@push('script')
<script>
    "use strict"

    $('.checkinfo').on('click', function() {
        var info = $(this).data('info');
        var modal = $('#infoModal');
        var html = '';
        html += `
                    <p class="d-flex flex-wrap justify-content-between pt-0"><strong>@lang('Journey Date')</strong>  <span>${info.date_of_journey}</span></p>
                    <p class="d-flex flex-wrap justify-content-between"><strong>@lang('PNR Number')</strong>  <span>${info.pnr_number}</span></p>
                    <p class="d-flex flex-wrap justify-content-between"><strong>@lang('Route')</strong>  <span>${info.trip.start_from.name} @lang('to') ${info.trip.end_to.name}</span></p>
                    <p class="d-flex flex-wrap justify-content-between"><strong>@lang('Fare')</strong>  <span>${parseInt(info.sub_total).toFixed(2)} {{ __($general->cur_text) }}</span></p>
                    <p class="d-flex flex-wrap justify-content-between"><strong>@lang('Status')</strong>  <span>${info.status == 1 ? '<span class="badge badge--success">@lang('Successful')</span>' : info.status == 2 ? '<span class="badge badge--warning">@lang('Pending')</span>' : info.status == 3 ? '<span class="badge badge--secondary">@lang('Canceled')</span>' : '<span class="badge badge--danger">@lang('Rejected')</span>'}</span></p>
                `;
        modal.find('.modal-body').html(html);
    });
	
</script>

<script type="text/javascript">
window.onload = function(){
	@if($show_review_modal)
	document.getElementById('openReviewModal').click();
	@endif
}
</script>
@endpush