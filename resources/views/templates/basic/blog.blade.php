@extends($activeTemplate.'layouts.newfrontend')
@section('content')
<!-- Blog Section Starts Here -->
<section class="blog-section section-90 section-md-111">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="row  g-4">
                    @foreach ($blogs as $item)
                    <div class="col-lg-6 col-md-6 col-sm-10">
                    <div class="post-item" style="margin-top:30px;position:relative">
                    <div class="post-thumb">
                        <img src="{{ getImage('assets/images/frontend/blog/thumb_'. $item->data_values->image) }}" alt="blog">
                    </div>
                    <div class="post-content post-content-two blogele">
                        <div style="text-align:left">
                            <span class="blogtag">Travel</span>
                        </div>
                        <div>
                            <h4 class="title" style="text-align:left;color:white !important;"><a href="{{ route('blog.details', [$item->id, slug($item->data_values->title)]) }}">{{ __(@$item->data_values->title) }}</a></h4>

                            <ul class="post-meta" style="padding:0px;">
                                <li>
                                    <span class="date"><i class="fa fa-calendar"></i>{{ showDateTime($item->created_at, 'd M Y') }}</span>
                                </li>
                            </ul>
                        </div>
                        
                        <!-- <p style="text-align:left">{{ __(shortDescription(strip_tags($item->data_values->description))) }}</p> -->
                    </div>
                </div>
                        <!-- <div class="post-item">
                            <div class="post-thumb">
                                <img src="{{ getImage('assets/images/frontend/blog/thumb_'. $item->data_values->image) }}" alt="{{ __(@$item->data_values->title) }}">
                            </div>
                            <div class="post-content">
                            <ul class="post-meta">
                                    <li>
                                        <span class="date"><i class="las la-calendar-check"></i>{{ showDateTime($item->created_at, 'd M Y') }}</span>
                                    </li>
                                </ul>
                                <h4 class="title"><a href="{{ route('blog.details', [$item->id, slug($item->data_values->title)]) }}">{{ __(@$item->data_values->title) }}</a></h4>
                                <p>{{ __(shortDescription(strip_tags($item->data_values->description))) }}</p>

                            </div>
                        </div> -->
                    </div>
                    @endforeach
                </div>
                  <div class="text-center text-lg-left offset-top-60" style="margin-bottom:60px;display:flex">
                      @if ($blogs->hasPages())
                      {{ paginateLinksNew($blogs) }}
                      @endif
                    </div>  
            </div>
        </div>
    </div>
</section>

<!-- Blog Section Ends Here -->
@if($sections->secs != null)
    @foreach(json_decode($sections->secs) as $sec)
        @include($activeTemplate.'sections.'.$sec)
    @endforeach
@endif
@endsection
