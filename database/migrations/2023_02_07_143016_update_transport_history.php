<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTransportHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('transporthistory', function (Blueprint $table) {
            //
            $table->dropColumn('packageweight');
            $table->dropColumn('status');
        });
        Schema::table('transporthistory', function (Blueprint $table) {
            //
          
            $table->string('packageweight')->nullable();
            $table->string('status')->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('transporthistory', function (Blueprint $table) {
            //
            $table->string('packageweight')->nullable();
            $table->string('status')->default(2);
        });
    }
}
