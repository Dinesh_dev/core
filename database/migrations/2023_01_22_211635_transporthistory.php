<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transporthistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('transporthistory',function(Blueprint $table){
            $table->id();
            $table->string('transport_id');
            $table->string('sendername');
            $table->longtext('senderaddress');
            $table->string('senderphoneno');
            $table->string('packagetype')->default(1);
            $table->string('packageweight');
            $table->string('amountcharged');
            $table->string('receivername');
            $table->string('receiveraddress');
            $table->string('receiverphoneno');
            $table->string('trackingno');
            $table->tinyInteger('status')->default(1);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('transporthistory');
    }
}
